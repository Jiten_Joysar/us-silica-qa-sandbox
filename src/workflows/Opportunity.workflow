<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify Technical Support when an opp reaches 25%25 probability</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>weller@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Sys_Admin_Templates/Technical_Support_Needs_to_Examine_Opp</template>
    </alerts>
    <alerts>
        <fullName>Notify Technical Support when an opp reaches 25%25 probability - 7 day reminder</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>weller@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Sys_Admin_Templates/Technical_Support_Needs_to_Examine_Opp_7_day_notice</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set Amount from Total Revenue Oppty</fullName>
        <field>Amount</field>
        <formula>Total_Revenue_Opportunity__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Amount to Total Revenue Opportunity</fullName>
        <actions>
            <name>Set Amount from Total Revenue Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>US Silica O&amp;G Sales Opportunity</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notfy Tech Support rep when added to opp</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Tech_Support_Needed__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Support_Rep_Assigned__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notfy Technical Support of 25%25 probability opportunities</fullName>
        <actions>
            <name>Notify Technical Support when an opp reaches 25%25 probability</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>0.25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Support_Needed__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sum Up O%26G Products to Amount</fullName>
        <active>true</active>
        <formula>OR( ISNEW(), ISCHANGED(X100_Mesh_Tonnage__c), ISCHANGED(X20_40_Tonnage__c), ISCHANGED(X200_Mesh_Tonnage__c), ISCHANGED(X30_50_Tonnage__c), ISCHANGED(X40_70_Tonnage__c), ISCHANGED(RCS_Tonnage__c), ISCHANGED(Other_Tonnage__c ),  ISCHANGED(X100_Mesh_Avg_Price__c ), ISCHANGED(X20_40_Avg_Price__c), ISCHANGED(X200_Mesh_Avg_Price__c), ISCHANGED(X30_50_Avg_Price__c), ISCHANGED(X40_70_Avg_Price__c), ISCHANGED(RCS_Avg_Price__c), ISCHANGED(Other_Avg_Price__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
