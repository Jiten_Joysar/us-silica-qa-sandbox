<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approval Level Roll Up</fullName>
        <field>Approval_Level_Roll_Up__c</field>
        <formula>Approval_Level_for_Quote__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>If Branch Plant is Columbia%2C add %245 for</fullName>
        <field>Pallet_Charge__c</field>
        <formula>5</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approval Level Roll Up</fullName>
        <actions>
            <name>Approval Level Roll Up</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>QuoteLineItem.Approval_Level__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>columbia test</fullName>
        <actions>
            <name>If Branch Plant is Columbia%2C add %245 for</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>QuoteLineItem.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>COLUMBIA DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
