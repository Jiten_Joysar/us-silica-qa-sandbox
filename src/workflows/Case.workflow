<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>3 Days Past Due</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contacts__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/X3_days_past_due</template>
    </alerts>
    <alerts>
        <fullName>5 Days Past Due</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Contacts__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/X5_days_past_due</template>
    </alerts>
    <alerts>
        <fullName>7 Days Past Due</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Contacts__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/X7_days_past_due</template>
    </alerts>
    <alerts>
        <fullName>Approval Notification of Freight Rate Request Case</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Freight_Rate_Request_Case_Approved_notification</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Artesia</fullName>
        <ccEmails>supplychain@ussilica.com</ccEmails>
        <ccEmails>kevinr@ironhorsepermianbasin.com</ccEmails>
        <ccEmails>khoryr@ironhorsepermianbasin.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Avis</fullName>
        <ccEmails>sallison@arrowmaterialservices.com</ccEmails>
        <ccEmails>jdrummond@arrowmaterialservices.com</ccEmails>
        <ccEmails>mward@arrowmaterialservices.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Barnhart</fullName>
        <ccEmails>Kelli.grove@tssands.com</ccEmails>
        <ccEmails>Jamie.enderby@tssands.com</ccEmails>
        <ccEmails>antoniovargas@resourcetransport.com</ccEmails>
        <ccEmails>delma.urias@tssands.com</ccEmails>
        <ccEmails>marty.lethrud@resourcetransport.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Barnhart1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>chuck.kesterson@txspecialtysands.com</ccEmails>
        <ccEmails>bobby.harrison@tssands.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Bossier Arrow</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Brush Wildcat</fullName>
        <ccEmails>bburns@wildcatminerals.com</ccEmails>
        <ccEmails>brush@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <ccEmails>rporch@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Brush Wildcat 2</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Bryan Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Butler Transflo</fullName>
        <ccEmails>brianharvey@savageservices.com</ccEmails>
        <ccEmails>transflo-butler@csx.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Carlsbad Titan</fullName>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>tlt-bgt@titanco.com</ccEmails>
        <ccEmails>cole_mullins@titanco.com</ccEmails>
        <ccEmails>whit_patterson@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Carlsbad Titan1</fullName>
        <ccEmails>selina_quintana@titanco.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>Torrance_jefferson@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Casper C%26C</fullName>
        <ccEmails>uss.casper@cctransload.com</ccEmails>
        <ccEmails>dwjolley@cctransload.com</ccEmails>
        <ccEmails>lance.muir@cctransload.com</ccEmails>
        <ccEmails>sean@cctransload.com</ccEmails>
        <ccEmails>sue@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Casper C%26C 2</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Columbia</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>murrayh@ussilica.com</ccEmails>
        <ccEmails>jonesd@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Douglas</fullName>
        <ccEmails>tiffany.norwood@twineagle.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>brian.hart@twineagle.com</ccEmails>
        <ccEmails>Douglas.User@twineagle.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Douglas1</fullName>
        <ccEmails>martin.edwards@twineagle.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Dubberly</fullName>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - East Fairview Northstar</fullName>
        <ccEmails>kbarksdale@northstarmidstream.com</ccEmails>
        <ccEmails>jcathey@northstarmidstream.com</ccEmails>
        <ccEmails>kstephens@northstarmidstream.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>efvlogistics@northstarmidstream.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - East Fairview Northstar1</fullName>
        <ccEmails>EFV-Logistics@northstarmidstream.com</ccEmails>
        <ccEmails>EFV-SandCustomerService@northstarmidstream.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - East Fairview Wildcat</fullName>
        <ccEmails>mhassard@wildcatminerals.com</ccEmails>
        <ccEmails>bakken@wildcatminerals.com</ccEmails>
        <ccEmails>mmiller@wildcatminerals.com</ccEmails>
        <ccEmails>jaltenhofen@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - East Fairview Wildcat1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - East Liverpool SH Bell</fullName>
        <ccEmails>dthayer@shbellco.com</ccEmails>
        <ccEmails>abell@shbellco.com</ccEmails>
        <ccEmails>gsmith@shbellco.com</ccEmails>
        <ccEmails>orders.eliv@shbellco.com</ccEmails>
        <ccEmails>mwoodburn@shbellco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - East Liverpool SH Bell1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Enid Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Fairmont Transflo</fullName>
        <ccEmails>tdavis@arrowmaterialservices.com</ccEmails>
        <ccEmails>mscena@transflo.net</ccEmails>
        <ccEmails>travis_davis@csx.com</ccEmails>
        <ccEmails>transflo_fairmont@csx.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Fairmont Transflo1</fullName>
        <ccEmails>jdrummond@arrowmaterialservices.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Ft Stockton Titan</fullName>
        <ccEmails>jlarsen@titanlansing.com</ccEmails>
        <ccEmails>tlt-sjt@titanco.com</ccEmails>
        <ccEmails>cole_mullins@titanco.com</ccEmails>
        <ccEmails>whit_patterson@titanco.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Ft Worth Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <ccEmails>jwall@maalt.com</ccEmails>
        <ccEmails>jcastillo@maalt.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Gonzales Wildcat</fullName>
        <ccEmails>gonzo@wildcatminerals.com</ccEmails>
        <ccEmails>msanmiguel@wildcatminerals.com</ccEmails>
        <ccEmails>nking@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Gonzales Wildcat1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Hannibal MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <ccEmails>gschell@e-mrie.com</ccEmails>
        <ccEmails>blind@e-mrie.com</ccEmails>
        <ccEmails>jlind@e-mrie.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Hughes Spring</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>droberts@ciglogistics.com</ccEmails>
        <ccEmails>logistics@etpstransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Hurtsboro</fullName>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Kelim Omnitrax</fullName>
        <ccEmails>lmargo@omnitrax.com</ccEmails>
        <ccEmails>rtkach@broe.com</ccEmails>
        <ccEmails>scobb@omnitrax.com</ccEmails>
        <ccEmails>rfactorlogistics@gmail.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Kosse</fullName>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Laredo Wildcat</fullName>
        <ccEmails>eagleford@wildcatminerals.com</ccEmails>
        <ccEmails>edadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Laredo Wildcat1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>Jholguin@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Levelland Titan</fullName>
        <ccEmails>tlt-lvd@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>whit_patterson@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Levelland Titan1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Loving Rangeland</fullName>
        <ccEmails>bbeaty@rgldenergy.com</ccEmails>
        <ccEmails>jyoung@rgldenergy.com</ccEmails>
        <ccEmails>agranger@rgldenergy.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Lubbock Titan</fullName>
        <ccEmails>tlt-lbb@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Lubbock Titan1</fullName>
        <ccEmails>reuben_guevera@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Mapleton</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>razzano@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>dell@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Mauricetown</fullName>
        <ccEmails>booz@ussilica.com</ccEmails>
        <ccEmails>riggins@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Mckees Rock MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <ccEmails>blind@e-mrie.com</ccEmails>
        <ccEmails>jkamenar@e-mrie.com</ccEmails>
        <ccEmails>jlind@e-mrie.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Mill Creek</fullName>
        <ccEmails>millcreek-orders@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Montpelier</fullName>
        <ccEmails>minteer@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - New Town</fullName>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>jlaster@wildcatminerals.com</ccEmails>
        <ccEmails>newtown@wildcatminerals.com</ccEmails>
        <ccEmails>pkalligher@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - New Town1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>jlaster@wildcatminerals.com</ccEmails>
        <ccEmails>akuehn@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Odessa USS</fullName>
        <ccEmails>transload@forenergyllc.com</ccEmails>
        <ccEmails>stephen@forenergyllc.com</ccEmails>
        <ccEmails>ken@forenergyllc.com</ccEmails>
        <ccEmails>tim@forenergyllc.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Odessa USS1</fullName>
        <ccEmails>elizabeth@forenergyllc.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Odessa Wildcat</fullName>
        <ccEmails>cholguin@wildcatminerals.com</ccEmails>
        <ccEmails>odessa@wildcatminerals.com</ccEmails>
        <ccEmails>btrujillo@wildcatminerals.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Ottawa</fullName>
        <ccEmails>otttransportationdept@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Pacific</fullName>
        <ccEmails>woods@ussilica.com</ccEmails>
        <ccEmails>cook@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Penhold</fullName>
        <ccEmails>operations@custbulk.com</ccEmails>
        <ccEmails>custbulk@xplorenet.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Rochelle CSS</fullName>
        <ccEmails>ebens@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Rockdale</fullName>
        <ccEmails>support@twlog.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Rockwood</fullName>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - San Antonio RLI</fullName>
        <ccEmails>cigsacsr@ciglogistics.com</ccEmails>
        <ccEmails>Theron@ciglogistics.com</ccEmails>
        <ccEmails>Brian@ciglogistics.com</ccEmails>
        <ccEmails>JGarcia@ciglogistics.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - San Antonio RLI1</fullName>
        <ccEmails>rhartnett@ciglogistics.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - San Antonio Titan</fullName>
        <ccEmails>myrick_navejar@titanco.com</ccEmails>
        <ccEmails>selina_quintana@titanco.com</ccEmails>
        <ccEmails>robert_cannon@titanco.com</ccEmails>
        <ccEmails>sanantoniotl@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - San Augustine Wildcat</fullName>
        <ccEmails>dgriffin@wildcatminerals.com</ccEmails>
        <ccEmails>akuehn@wildcatminerals.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Shattuck WB Johnston</fullName>
        <ccEmails>scott.bonine@cgb.com</ccEmails>
        <ccEmails>alan.bilyeu@cgb.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Sparta</fullName>
        <ccEmails>bangart@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Sparta1</fullName>
        <ccEmails>kenworthy@ussilica.com</ccEmails>
        <ccEmails>forde@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <ccEmails>kraklow@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Three Rivers</fullName>
        <ccEmails>email-support@twlog.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Victoria Equalizer</fullName>
        <ccEmails>rbrunson@equalizerinc.com</ccEmails>
        <ccEmails>equalizerinc@hotmail.com</ccEmails>
        <ccEmails>jtate@equalizerinc.com</ccEmails>
        <ccEmails>mnamken@equalizerinc.com</ccEmails>
        <ccEmails>pcombs@equalizerinc.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Victoria Equalizer1</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>cprichard@equalizerinc.com</ccEmails>
        <ccEmails>victoria@equalizerinc.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Weatherford Wildcat</fullName>
        <ccEmails>nleslie@wildcatminerals.com</ccEmails>
        <ccEmails>midcon@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>rporch@wildcatminerals.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Williston Rockwater</fullName>
        <ccEmails>sandplant@rockwaterenergy.com</ccEmails>
        <ccEmails>jschwartz@rockwaterenergy.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Wyalusing</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>adam.dietz@tran-z.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Branch Plant Notification - Wysox Shale Rail</fullName>
        <ccEmails>t_coleman@shalerail.com</ccEmails>
        <ccEmails>j_brink@shalerail.com</ccEmails>
        <ccEmails>admin@shalerail.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Cancelled Past Due Order</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Contacts__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact4__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Related_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Cancelled_Past_Due_Order</template>
    </alerts>
    <alerts>
        <fullName>Credit Application - Create new accounts</fullName>
        <ccEmails>customermaster@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Application_Approved_New_Account_Setup_Needed</template>
    </alerts>
    <alerts>
        <fullName>Credit Application - New accounts activated</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Application_Notification_of_new_accounts</template>
    </alerts>
    <alerts>
        <fullName>Credit Application Missing Company Financials</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Application_Missing_Company_Financials</template>
    </alerts>
    <alerts>
        <fullName>Credit Application Missing Signature</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Application_Missing_Signature</template>
    </alerts>
    <alerts>
        <fullName>Credit Application Missing Tax Exempt Form</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Application_No_Tax_Exempt_Form</template>
    </alerts>
    <alerts>
        <fullName>Credit Rebill Approved Step 1</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Rebill_Case_Approved_notification</template>
    </alerts>
    <alerts>
        <fullName>Credit Rebill Executive Approval Needed</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jenks@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Rebill_Executive_Approval_needed</template>
    </alerts>
    <alerts>
        <fullName>Credit Rebill not closed after 1 week</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Reminder_to_close_credit_rebill_case_1_week</template>
    </alerts>
    <alerts>
        <fullName>Credit Rebill not closed after 3 days</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Reminder_to_close_credit_rebill_case</template>
    </alerts>
    <alerts>
        <fullName>Credit Rebill not closed after 5 days</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Reminder_to_close_credit_rebill_case_5_days</template>
    </alerts>
    <alerts>
        <fullName>Customer Master - Notify Credit Team to activate account</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>Credit Analyst, ISP</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Credit Analyst, Oil &amp; Gas</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Master_Notify_Credit_to_Activate_Account</template>
    </alerts>
    <alerts>
        <fullName>Customer Survey to the contact for closed case</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <template>unfiled$public/Customer_Survey_invite</template>
    </alerts>
    <alerts>
        <fullName>Email Account Owner - New Case</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Account_notification</template>
    </alerts>
    <alerts>
        <fullName>Email Account Owner - New Case - South</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>chandler@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Email Account Owner closed case status</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <template>Case_Workflow_Templates/Case_Closed_for_Account_notification</template>
    </alerts>
    <alerts>
        <fullName>Email notification for Don Weinheimer Approval Received</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_1__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/O_G_Approver_3_Approved_SPR</template>
    </alerts>
    <alerts>
        <fullName>Email notification for Jack Maley Approval Received</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>buehl@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>husain@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thorp@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thumm@ussilica.com1.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>weber@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_1__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/O_G_Approver_2_Approved_SPR</template>
    </alerts>
    <alerts>
        <fullName>Fifth SPR Approval</fullName>
        <protected>false</protected>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_5__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/SPR_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>First SPR Approval</fullName>
        <protected>false</protected>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_1__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/SPR_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Fourth SPR Approval</fullName>
        <protected>false</protected>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_4__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/SPR_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate - Entered into JDE</fullName>
        <ccEmails>banks@ussilica.com</ccEmails>
        <ccEmails>dodds@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Freight_Rate_Freight_Rate_Entered_into_JDE</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate Request - Rail</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>klinck@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Freight_Rate_New_Rate_Request_Form_Received</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate Request - Truck</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennings@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>klinck@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stadnick@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Freight_Rate_New_Rate_Request_Form_Received</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate Request Completed</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Freight_Rate_New_Rate_Request_Form_Completed</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate accepted by customer - not rail</fullName>
        <ccEmails>furst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennings@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>klinck@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stadnick@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/Freight_Rate_New_Rate_Request_Accepted</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate accepted by customer - rail</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>klinck@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/Freight_Rate_New_Rate_Request_Accepted</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Avis</fullName>
        <ccEmails>sallison@arrowmaterialservices.com</ccEmails>
        <ccEmails>jdrummond@arrowmaterialservices.com</ccEmails>
        <ccEmails>mward@arrowmaterialservices.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Bossier Arrow</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Brush Wildcat</fullName>
        <ccEmails>rbronk@wildcatminerals.com</ccEmails>
        <ccEmails>brush@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <ccEmails>dedmonds@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Bryan Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Butler Transflo</fullName>
        <ccEmails>brianharvey@savageservices.com</ccEmails>
        <ccEmails>transflo-butler@csx.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Carlsbad Titan</fullName>
        <ccEmails>matthew_farmer@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>tlt-bgt@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Casper C%26C</fullName>
        <ccEmails>uss.casper@cctransload.com</ccEmails>
        <ccEmails>dwjolley@cctransload.com</ccEmails>
        <ccEmails>lance.muir@cctransload.com</ccEmails>
        <ccEmails>sean@cctransload.com</ccEmails>
        <ccEmails>sue@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Columbia</fullName>
        <ccEmails>murrayh@ussilica.com</ccEmails>
        <ccEmails>jonesd@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Dubberly</fullName>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - East Fairview Wildcat</fullName>
        <ccEmails>mhassard@wildcatminerals.com</ccEmails>
        <ccEmails>bakken@wildcatminerals.com</ccEmails>
        <ccEmails>abraun@wildcatminerals.com</ccEmails>
        <ccEmails>mmiller@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - East Liverpool SH Bell</fullName>
        <ccEmails>dthayer@shbellco.com</ccEmails>
        <ccEmails>abell@shbellco.com</ccEmails>
        <ccEmails>gsmith@shbellco.com</ccEmails>
        <ccEmails>orders.eliv@shbellco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Enid Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Fairmont Transflo</fullName>
        <ccEmails>tdavis@arrowmaterialservices.com</ccEmails>
        <ccEmails>mscena@transflo.net</ccEmails>
        <ccEmails>travis_davis@csx.com</ccEmails>
        <ccEmails>transflo_fairmont@csx.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Ft Stockton Titan</fullName>
        <ccEmails>cesar_ramirez@titanco.com</ccEmails>
        <ccEmails>tlt-sjt@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Ft Worth Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <ccEmails>jwall@maalt.com</ccEmails>
        <ccEmails>jcastillo@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Gonzales Wildcat</fullName>
        <ccEmails>gonzo@wildcatminerals.com</ccEmails>
        <ccEmails>msanmiguel@wildcatminerals.com</ccEmails>
        <ccEmails>nking@wildcatminerals.com</ccEmails>
        <ccEmails>gzadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Hannibal MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Hurtsboro</fullName>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Kelim Omnitrax</fullName>
        <ccEmails>lmargo@omnitrax.com</ccEmails>
        <ccEmails>rtkach@broe.com</ccEmails>
        <ccEmails>scobb@omnitrax.com</ccEmails>
        <ccEmails>rfactorlogistics@gmail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Kosse</fullName>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Laredo Wildcat</fullName>
        <ccEmails>eagleford@wildcatminerals.com</ccEmails>
        <ccEmails>edadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Levelland Titan</fullName>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>tlt-lvd@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Lubbock Titan</fullName>
        <ccEmails>tlt-lbb@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Mapleton</fullName>
        <ccEmails>himes@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Mauricetown</fullName>
        <ccEmails>booz@ussilica.com</ccEmails>
        <ccEmails>riggins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Mckees Rock MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <ccEmails>blind@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Mill Creek</fullName>
        <ccEmails>millcreek-orders@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Montpelier</fullName>
        <ccEmails>minteer@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - New Town</fullName>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>blovins@wildcatminerals.com</ccEmails>
        <ccEmails>menglish@wildcatminerals.com</ccEmails>
        <ccEmails>newtown@wildcatminerals.com</ccEmails>
        <ccEmails>pkalligher@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Odessa UPDS</fullName>
        <ccEmails>uss.odessa@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Odessa Wildcat</fullName>
        <ccEmails>lmccasin@wildcatminerals.com</ccEmails>
        <ccEmails>cholguin@wildcatminerals.com</ccEmails>
        <ccEmails>odessa@wildcatminerals.com</ccEmails>
        <ccEmails>btrujillo@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Ottawa</fullName>
        <ccEmails>otttransportationdept@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Pacific</fullName>
        <ccEmails>woods@ussilica.com</ccEmails>
        <ccEmails>cook@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Rochelle</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Rochelle CSS</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Rockwood</fullName>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - San Antonio RLI</fullName>
        <ccEmails>taymaninventorymanagement@raillink.com</ccEmails>
        <ccEmails>clientsupportteam1.sa@raillink.com</ccEmails>
        <ccEmails>dispatch.sa@raillink.com</ccEmails>
        <ccEmails>clientsupportteam2.sa@raillink.com</ccEmails>
        <ccEmails>terminalmgr.sa@raillink.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - San Antonio Titan</fullName>
        <ccEmails>myrick_navejar@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>robert_cannon@titanco.com</ccEmails>
        <ccEmails>sanantoniotl@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Sparta</fullName>
        <ccEmails>bangart@ussilica.com</ccEmails>
        <ccEmails>jennings@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Victoria Equalizer</fullName>
        <ccEmails>rbrunson@texnet.net</ccEmails>
        <ccEmails>equalizerinc@hotmail.com</ccEmails>
        <ccEmails>jtate@texnet.net</ccEmails>
        <ccEmails>mnamken@texnet.net</ccEmails>
        <ccEmails>pcombs@texnet.net</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Weatherford Wildcat</fullName>
        <ccEmails>craney@wildcatminerals.com</ccEmails>
        <ccEmails>midcon@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Williston Rockwater</fullName>
        <ccEmails>sandplant@rockwaterenergy.com</ccEmails>
        <ccEmails>jschwartz@rockwaterenergy.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 2 Notification - Wysox Shale Rail</fullName>
        <ccEmails>t_coleman@shalerail.com</ccEmails>
        <ccEmails>j_brink@shalerail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_2_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Avis</fullName>
        <ccEmails>sallison@arrowmaterialservices.com</ccEmails>
        <ccEmails>jdrummond@arrowmaterialservices.com</ccEmails>
        <ccEmails>mward@arrowmaterialservices.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Bossier Arrow</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Brush Wildcat</fullName>
        <ccEmails>rbronk@wildcatminerals.com</ccEmails>
        <ccEmails>brush@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <ccEmails>dedmonds@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Bryan Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Butler Transflo</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Carlsbad Titan</fullName>
        <ccEmails>matthew_farmer@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>tlt-bgt@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Casper C%26C</fullName>
        <ccEmails>uss.casper@cctransload.com</ccEmails>
        <ccEmails>dwjolley@cctransload.com</ccEmails>
        <ccEmails>lance.muir@cctransload.com</ccEmails>
        <ccEmails>sean@cctransload.com</ccEmails>
        <ccEmails>sue@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Columbia</fullName>
        <ccEmails>murrayh@ussilica.com</ccEmails>
        <ccEmails>jonesd@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Dubberly</fullName>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - East Fairview Wildcat</fullName>
        <ccEmails>mhassard@wildcatminerals.com</ccEmails>
        <ccEmails>bakken@wildcatminerals.com</ccEmails>
        <ccEmails>abraun@wildcatminerals.com</ccEmails>
        <ccEmails>mmiller@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - East Liverpool SH Bell</fullName>
        <ccEmails>dthayer@shbellco.com</ccEmails>
        <ccEmails>abell@shbellco.com</ccEmails>
        <ccEmails>gsmith@shbellco.com</ccEmails>
        <ccEmails>orders.eliv@shbellco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Enid Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Fairmont Transflo</fullName>
        <ccEmails>tdavis@arrowmaterialservices.com</ccEmails>
        <ccEmails>mscena@transflo.net</ccEmails>
        <ccEmails>travis_davis@csx.com</ccEmails>
        <ccEmails>transflo_fairmont@csx.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Ft Stockton Titan</fullName>
        <ccEmails>cesar_ramirez@titanco.com</ccEmails>
        <ccEmails>tlt-sjt@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Ft Worth Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <ccEmails>jwall@maalt.com</ccEmails>
        <ccEmails>jcastillo@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Gonzales Wildcat</fullName>
        <ccEmails>gonzo@wildcatminerals.com</ccEmails>
        <ccEmails>msanmiguel@wildcatminerals.com</ccEmails>
        <ccEmails>nking@wildcatminerals.com</ccEmails>
        <ccEmails>gzadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Hannibal MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Hurtsboro</fullName>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Kelim Omnitrax</fullName>
        <ccEmails>lmargo@omnitrax.com</ccEmails>
        <ccEmails>rtkach@broe.com</ccEmails>
        <ccEmails>scobb@omnitrax.com</ccEmails>
        <ccEmails>rfactorlogistics@gmail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Kosse</fullName>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Laredo Wildcat</fullName>
        <ccEmails>eagleford@wildcatminerals.com</ccEmails>
        <ccEmails>edadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Levelland Titan</fullName>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>tlt-lvd@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Lubbock Titan</fullName>
        <ccEmails>tlt-lbb@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Mapleton</fullName>
        <ccEmails>himes@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Mauricetown</fullName>
        <ccEmails>booz@ussilica.com</ccEmails>
        <ccEmails>riggins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Mckees Rock MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <ccEmails>blind@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Mill Creek</fullName>
        <ccEmails>millcreek-orders@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Montpelier</fullName>
        <ccEmails>minteer@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - New Town</fullName>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>blovins@wildcatminerals.com</ccEmails>
        <ccEmails>menglish@wildcatminerals.com</ccEmails>
        <ccEmails>newtown@wildcatminerals.com</ccEmails>
        <ccEmails>pkalligher@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Odessa UPDS</fullName>
        <ccEmails>uss.odessa@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Odessa Wildcat</fullName>
        <ccEmails>lmccasin@wildcatminerals.com</ccEmails>
        <ccEmails>cholguin@wildcatminerals.com</ccEmails>
        <ccEmails>odessa@wildcatminerals.com</ccEmails>
        <ccEmails>btrujillo@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Ottawa</fullName>
        <ccEmails>otttransportationdept@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Pacific</fullName>
        <ccEmails>woods@ussilica.com</ccEmails>
        <ccEmails>cook@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Rochelle</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Rochelle CSS</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Rockwood</fullName>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - San Antonio RLI</fullName>
        <ccEmails>taymaninventorymanagement@raillink.com</ccEmails>
        <ccEmails>clientsupportteam1.sa@raillink.com</ccEmails>
        <ccEmails>dispatch.sa@raillink.com</ccEmails>
        <ccEmails>clientsupportteam2.sa@raillink.com</ccEmails>
        <ccEmails>terminalmgr.sa@raillink.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - San Antonio Titan</fullName>
        <ccEmails>myrick_navejar@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>robert_cannon@titanco.com</ccEmails>
        <ccEmails>sanantoniotl@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Sparta</fullName>
        <ccEmails>bangart@ussilica.com</ccEmails>
        <ccEmails>jennings@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Victoria Equalizer</fullName>
        <ccEmails>rbrunson@texnet.net</ccEmails>
        <ccEmails>equalizerinc@hotmail.com</ccEmails>
        <ccEmails>jtate@texnet.net</ccEmails>
        <ccEmails>mnamken@texnet.net</ccEmails>
        <ccEmails>pcombs@texnet.net</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Weatherford Wildcat</fullName>
        <ccEmails>craney@wildcatminerals.com</ccEmails>
        <ccEmails>midcon@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Williston Rockwater</fullName>
        <ccEmails>sandplant@rockwaterenergy.com</ccEmails>
        <ccEmails>jschwartz@rockwaterenergy.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 3 Notification - Wysox Shale Rail</fullName>
        <ccEmails>t_coleman@shalerail.com</ccEmails>
        <ccEmails>j_brink@shalerail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_3_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Avis</fullName>
        <ccEmails>sallison@arrowmaterialservices.com</ccEmails>
        <ccEmails>jdrummond@arrowmaterialservices.com</ccEmails>
        <ccEmails>mward@arrowmaterialservices.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Bossier Arrow</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Brush Wildcat</fullName>
        <ccEmails>rbronk@wildcatminerals.com</ccEmails>
        <ccEmails>brush@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <ccEmails>dedmonds@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Bryan Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Butler Transflo</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Carlsbad Titan</fullName>
        <ccEmails>matthew_farmer@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>tlt-bgt@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Casper C%26C</fullName>
        <ccEmails>uss.casper@cctransload.com</ccEmails>
        <ccEmails>dwjolley@cctransload.com</ccEmails>
        <ccEmails>lance.muir@cctransload.com</ccEmails>
        <ccEmails>sean@cctransload.com</ccEmails>
        <ccEmails>sue@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Columbia</fullName>
        <ccEmails>murrayh@ussilica.com</ccEmails>
        <ccEmails>jonesd@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Dubberly</fullName>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - East Fairview Wildcat</fullName>
        <ccEmails>mhassard@wildcatminerals.com</ccEmails>
        <ccEmails>bakken@wildcatminerals.com</ccEmails>
        <ccEmails>abraun@wildcatminerals.com</ccEmails>
        <ccEmails>mmiller@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - East Liverpool SH Bell</fullName>
        <ccEmails>dthayer@shbellco.com</ccEmails>
        <ccEmails>abell@shbellco.com</ccEmails>
        <ccEmails>gsmith@shbellco.com</ccEmails>
        <ccEmails>orders.eliv@shbellco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Enid Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Fairmont Transflo</fullName>
        <ccEmails>tdavis@arrowmaterialservices.com</ccEmails>
        <ccEmails>mscena@transflo.net</ccEmails>
        <ccEmails>travis_davis@csx.com</ccEmails>
        <ccEmails>transflo_fairmont@csx.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Ft Stockton Titan</fullName>
        <ccEmails>cesar_ramirez@titanco.com</ccEmails>
        <ccEmails>tlt-sjt@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Ft Worth Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <ccEmails>jwall@maalt.com</ccEmails>
        <ccEmails>jcastillo@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Gonzales Wildcat</fullName>
        <ccEmails>gonzo@wildcatminerals.com</ccEmails>
        <ccEmails>msanmiguel@wildcatminerals.com</ccEmails>
        <ccEmails>nking@wildcatminerals.com</ccEmails>
        <ccEmails>gzadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Hannibal MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Hurtsboro</fullName>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Kelim Omnitrax</fullName>
        <ccEmails>lmargo@omnitrax.com</ccEmails>
        <ccEmails>rtkach@broe.com</ccEmails>
        <ccEmails>scobb@omnitrax.com</ccEmails>
        <ccEmails>rfactorlogistics@gmail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Kosse</fullName>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Laredo Wildcat</fullName>
        <ccEmails>eagleford@wildcatminerals.com</ccEmails>
        <ccEmails>edadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Levelland Titan</fullName>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>tlt-lvd@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Lubbock Titan</fullName>
        <ccEmails>tlt-lbb@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Mapleton</fullName>
        <ccEmails>himes@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Mauricetown</fullName>
        <ccEmails>booz@ussilica.com</ccEmails>
        <ccEmails>riggins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Mckees Rock MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <ccEmails>blind@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Mill Creek</fullName>
        <ccEmails>millcreek-orders@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Montpelier</fullName>
        <ccEmails>minteer@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - New Town</fullName>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>blovins@wildcatminerals.com</ccEmails>
        <ccEmails>menglish@wildcatminerals.com</ccEmails>
        <ccEmails>newtown@wildcatminerals.com</ccEmails>
        <ccEmails>pkalligher@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Odessa UPDS</fullName>
        <ccEmails>uss.odessa@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Odessa Wildcat</fullName>
        <ccEmails>lmccasin@wildcatminerals.com</ccEmails>
        <ccEmails>cholguin@wildcatminerals.com</ccEmails>
        <ccEmails>odessa@wildcatminerals.com</ccEmails>
        <ccEmails>btrujillo@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Ottawa</fullName>
        <ccEmails>otttransportationdept@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Pacific</fullName>
        <ccEmails>woods@ussilica.com</ccEmails>
        <ccEmails>cook@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Rochelle</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Rochelle CSS</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Rockwood</fullName>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - San Antonio RLI</fullName>
        <ccEmails>taymaninventorymanagement@raillink.com</ccEmails>
        <ccEmails>clientsupportteam1.sa@raillink.com</ccEmails>
        <ccEmails>dispatch.sa@raillink.com</ccEmails>
        <ccEmails>clientsupportteam2.sa@raillink.com</ccEmails>
        <ccEmails>terminalmgr.sa@raillink.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - San Antonio Titan</fullName>
        <ccEmails>myrick_navejar@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>robert_cannon@titanco.com</ccEmails>
        <ccEmails>sanantoniotl@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Victoria Equalizer</fullName>
        <ccEmails>rbrunson@texnet.net</ccEmails>
        <ccEmails>equalizerinc@hotmail.com</ccEmails>
        <ccEmails>jtate@texnet.net</ccEmails>
        <ccEmails>mnamken@texnet.net</ccEmails>
        <ccEmails>pcombs@texnet.net</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Weatherford Wildcat</fullName>
        <ccEmails>craney@wildcatminerals.com</ccEmails>
        <ccEmails>midcon@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Williston Rockwater</fullName>
        <ccEmails>sandplant@rockwaterenergy.com</ccEmails>
        <ccEmails>jschwartz@rockwaterenergy.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 4 Notification - Wysox Shale Rail</fullName>
        <ccEmails>t_coleman@shalerail.com</ccEmails>
        <ccEmails>j_brink@shalerail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_4_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Avis</fullName>
        <ccEmails>sallison@arrowmaterialservices.com</ccEmails>
        <ccEmails>jdrummond@arrowmaterialservices.com</ccEmails>
        <ccEmails>mward@arrowmaterialservices.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Bossier Arrow</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Brush Wildcat</fullName>
        <ccEmails>rbronk@wildcatminerals.com</ccEmails>
        <ccEmails>brush@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <ccEmails>dedmonds@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Bryan Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Butler Transflo</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Carlsbad Titan</fullName>
        <ccEmails>matthew_farmer@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>tlt-bgt@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Casper C%26C</fullName>
        <ccEmails>uss.casper@cctransload.com</ccEmails>
        <ccEmails>dwjolley@cctransload.com</ccEmails>
        <ccEmails>lance.muir@cctransload.com</ccEmails>
        <ccEmails>sean@cctransload.com</ccEmails>
        <ccEmails>sue@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Columbia</fullName>
        <ccEmails>murrayh@ussilica.com</ccEmails>
        <ccEmails>jonesd@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Dubberly</fullName>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - East Fairview Wildcat</fullName>
        <ccEmails>mhassard@wildcatminerals.com</ccEmails>
        <ccEmails>bakken@wildcatminerals.com</ccEmails>
        <ccEmails>abraun@wildcatminerals.com</ccEmails>
        <ccEmails>mmiller@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - East Liverpool SH Bell</fullName>
        <ccEmails>dthayer@shbellco.com</ccEmails>
        <ccEmails>abell@shbellco.com</ccEmails>
        <ccEmails>gsmith@shbellco.com</ccEmails>
        <ccEmails>orders.eliv@shbellco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Enid Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Fairmont Transflo</fullName>
        <ccEmails>tdavis@arrowmaterialservices.com</ccEmails>
        <ccEmails>mscena@transflo.net</ccEmails>
        <ccEmails>travis_davis@csx.com</ccEmails>
        <ccEmails>transflo_fairmont@csx.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Ft Stockton Titan</fullName>
        <ccEmails>cesar_ramirez@titanco.com</ccEmails>
        <ccEmails>tlt-sjt@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Ft Worth Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <ccEmails>jwall@maalt.com</ccEmails>
        <ccEmails>jcastillo@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Gonzales Wildcat</fullName>
        <ccEmails>gonzo@wildcatminerals.com</ccEmails>
        <ccEmails>msanmiguel@wildcatminerals.com</ccEmails>
        <ccEmails>nking@wildcatminerals.com</ccEmails>
        <ccEmails>gzadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Hannibal MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Hurtsboro</fullName>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Kelim Omnitrax</fullName>
        <ccEmails>lmargo@omnitrax.com</ccEmails>
        <ccEmails>rtkach@broe.com</ccEmails>
        <ccEmails>scobb@omnitrax.com</ccEmails>
        <ccEmails>rfactorlogistics@gmail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Kosse</fullName>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Laredo Wildcat</fullName>
        <ccEmails>eagleford@wildcatminerals.com</ccEmails>
        <ccEmails>edadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Levelland Titan</fullName>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>tlt-lvd@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Lubbock Titan</fullName>
        <ccEmails>tlt-lbb@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Mapleton</fullName>
        <ccEmails>himes@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Mauricetown</fullName>
        <ccEmails>booz@ussilica.com</ccEmails>
        <ccEmails>riggins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Mckees Rock MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <ccEmails>blind@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Mill Creek</fullName>
        <ccEmails>millcreek-orders@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Montpelier</fullName>
        <ccEmails>minteer@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - New Town</fullName>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>blovins@wildcatminerals.com</ccEmails>
        <ccEmails>menglish@wildcatminerals.com</ccEmails>
        <ccEmails>newtown@wildcatminerals.com</ccEmails>
        <ccEmails>pkalligher@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Odessa UPDS</fullName>
        <ccEmails>uss.odessa@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Odessa Wildcat</fullName>
        <ccEmails>lmccasin@wildcatminerals.com</ccEmails>
        <ccEmails>cholguin@wildcatminerals.com</ccEmails>
        <ccEmails>odessa@wildcatminerals.com</ccEmails>
        <ccEmails>btrujillo@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Ottawa</fullName>
        <ccEmails>otttransportationdept@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Pacific</fullName>
        <ccEmails>woods@ussilica.com</ccEmails>
        <ccEmails>cook@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Rochelle</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Rochelle CSS</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Rockwood</fullName>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - San Antonio RLI</fullName>
        <ccEmails>taymaninventorymanagement@raillink.com</ccEmails>
        <ccEmails>clientsupportteam1.sa@raillink.com</ccEmails>
        <ccEmails>dispatch.sa@raillink.com</ccEmails>
        <ccEmails>clientsupportteam2.sa@raillink.com</ccEmails>
        <ccEmails>terminalmgr.sa@raillink.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - San Antonio Titan</fullName>
        <ccEmails>myrick_navejar@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>robert_cannon@titanco.com</ccEmails>
        <ccEmails>sanantoniotl@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Sparta</fullName>
        <ccEmails>bangart@ussilica.com</ccEmails>
        <ccEmails>jennings@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Victoria Equalizer</fullName>
        <ccEmails>rbrunson@texnet.net</ccEmails>
        <ccEmails>equalizerinc@hotmail.com</ccEmails>
        <ccEmails>jtate@texnet.net</ccEmails>
        <ccEmails>mnamken@texnet.net</ccEmails>
        <ccEmails>pcombs@texnet.net</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Weatherford Wildcat</fullName>
        <ccEmails>craney@wildcatminerals.com</ccEmails>
        <ccEmails>midcon@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Williston Rockwater</fullName>
        <ccEmails>sandplant@rockwaterenergy.com</ccEmails>
        <ccEmails>jschwartz@rockwaterenergy.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Grouping 5 Notification - Wysox Shale Rail</fullName>
        <ccEmails>t_coleman@shalerail.com</ccEmails>
        <ccEmails>j_brink@shalerail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_5_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>ISP - Credit team notified to remove credit hold</fullName>
        <ccEmails>usscredit@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_CS_Credit_Hold_Notification</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Berkeley</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>minteer@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>varner@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Cadre</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>cheryl.copeland@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christiepayne@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>randall.larr@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Columbia</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jonesd@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Dubberly</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hutchins@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Florisil</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>minteer@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>varner@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Hurtsboro</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stinson@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Jackson</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>watersm@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Kosse</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>bradley@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Mapleton</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dell@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Mauricetown</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>booz@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>riggins@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Mill Creek</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>millerl@ussilica.com.qa.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>strong@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wright@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Montpelier</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>minteer@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>varner@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Ottawa</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>swords@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Pacific</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wood@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Rochelle</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>swords@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - PO on quote - Rockwood</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>windhurst@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Customer_Service_Notification_when_PO_Attached_to_Quote</template>
    </alerts>
    <alerts>
        <fullName>ISP - Send CoA Request to Ottawa</fullName>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/CoA_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>ISP CoA Request - Ottawa</fullName>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/CoA_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>ISP Customer Care - Email Notification of Order Confirmation</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Customer_Care_Order_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>ISP Customer Care - Email Notification of Order Confirmation w notification</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Customer_Care_Order_Confirmation_w_extra_statement</template>
    </alerts>
    <alerts>
        <fullName>ISP International Sales Order Received Notification</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_International_Order_Received</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Berkeley</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>minteer@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>varner@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Columbia</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jonesd@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Dubberly</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>james@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Florisil</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>minteer@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>varner@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Hurtsboro</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stinson@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Jackson</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>watersm@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Kosse</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bradley@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Mapleton</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>dell@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Mauricetown</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>booz@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>riggins@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Mill Creek</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>millerl@ussilica.com.qa.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>strong@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wright@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Montpelier</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>minteer@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>varner@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Ottawa</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>swords@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yunker@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Pacific</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wood@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Rochelle</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>swords@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Rockwood</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>windhurst@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>ISP Pricing - Customer Service Notified of JDE Setup - Voca</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>cheryl.copeland@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christiepayne@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>randall.larr@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>Initial Order Approval</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Initial_Order_Approval</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Avis</fullName>
        <ccEmails>sallison@arrowmaterialservices.com</ccEmails>
        <ccEmails>jdrummond@arrowmaterialservices.com</ccEmails>
        <ccEmails>mward@arrowmaterialservices.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Berkeley Springs</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Brush Wildcat</fullName>
        <ccEmails>rbronk@wildcatminerals.com</ccEmails>
        <ccEmails>brush@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <ccEmails>dedmonds@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Bryan Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Carbondale</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>jgrabin@lindeco.com</ccEmails>
        <ccEmails>lholgate@lindeco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Carlsbad Titan</fullName>
        <ccEmails>matthew_farmer@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>tlt-bgt@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Casper</fullName>
        <ccEmails>uss.casper@cctransload.com</ccEmails>
        <ccEmails>dwjolley@cctransload.com</ccEmails>
        <ccEmails>lance.muir@cctransload.com</ccEmails>
        <ccEmails>sean@cctransload.com</ccEmails>
        <ccEmails>sue@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Columbia</fullName>
        <ccEmails>murrayh@ussilica.com</ccEmails>
        <ccEmails>jonesd@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Dubberly</fullName>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - East Fairview Wildcat</fullName>
        <ccEmails>mhassard@wildcatminerals.com</ccEmails>
        <ccEmails>bakken@wildcatminerals.com</ccEmails>
        <ccEmails>abraun@wildcatminerals.com</ccEmails>
        <ccEmails>mmiller@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - East Liverpool - Bell</fullName>
        <ccEmails>dthayer@shbellco.com</ccEmails>
        <ccEmails>abell@shbellco.com</ccEmails>
        <ccEmails>gsmith@shbellco.com</ccEmails>
        <ccEmails>orders.eliv@shbellco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Fairmont</fullName>
        <ccEmails>tdavis@arrowmaterialservices.com</ccEmails>
        <ccEmails>mscena@transflo.net</ccEmails>
        <ccEmails>travis_davis@csx.com</ccEmails>
        <ccEmails>transflo_fairmont@csx.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Ft Stockton Titan</fullName>
        <ccEmails>cesar_ramirez@titanco.com</ccEmails>
        <ccEmails>tlt-sjt@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Gonzales Wildcat</fullName>
        <ccEmails>gonzo@wildcatminerals.com</ccEmails>
        <ccEmails>msanmiguel@wildcatminerals.com</ccEmails>
        <ccEmails>nking@wildcatminerals.com</ccEmails>
        <ccEmails>gzadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Hannibal</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Hurtsboro</fullName>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Kelim</fullName>
        <ccEmails>lmargo@omnitrax.com</ccEmails>
        <ccEmails>rtkach@broe.com</ccEmails>
        <ccEmails>scobb@omnitrax.com</ccEmails>
        <ccEmails>rfactorlogistics@gmail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Kosse</fullName>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - LaSalle ADM</fullName>
        <ccEmails>john.yucus@adm.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Laredo Wildcat</fullName>
        <ccEmails>eagleford@wildcatminerals.com</ccEmails>
        <ccEmails>edadmin@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>customerservice@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Levelland Titan</fullName>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>tlt-lvd@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Lubbock Titan</fullName>
        <ccEmails>tlt-lbb@titanco.com</ccEmails>
        <ccEmails>transloading@titanco.com</ccEmails>
        <ccEmails>cody_dukatnik@titanco.com</ccEmails>
        <ccEmails>richard_winegeart@titanco.com</ccEmails>
        <ccEmails>tim_escue@titanco.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - MRIE</fullName>
        <ccEmails>sshazer@e-mrie.com</ccEmails>
        <ccEmails>blind@e-mrie.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Maalt</fullName>
        <ccEmails>logistics@maalt.com</ccEmails>
        <ccEmails>rhaney@maalt.com</ccEmails>
        <ccEmails>jwall@maalt.com</ccEmails>
        <ccEmails>jcastillo@maalt.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Mapleton</fullName>
        <ccEmails>himes@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Mauricetown</fullName>
        <ccEmails>booz@ussilica.com</ccEmails>
        <ccEmails>riggins@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Mill Creek</fullName>
        <ccEmails>millcreek-orders@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Montpelier</fullName>
        <ccEmails>minteer@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - New Town Wildcat</fullName>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <ccEmails>blovins@wildcatminerals.com</ccEmails>
        <ccEmails>menglish@wildcatminerals.com</ccEmails>
        <ccEmails>newtown@wildcatminerals.com</ccEmails>
        <ccEmails>pkalligher@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Odessa Wildcat</fullName>
        <ccEmails>lmccasin@wildcatminerals.com</ccEmails>
        <ccEmails>cholguin@wildcatminerals.com</ccEmails>
        <ccEmails>odessa@wildcatminerals.com</ccEmails>
        <ccEmails>btrujillo@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Ottawa</fullName>
        <ccEmails>otttransportationdept@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Ottawa ADM</fullName>
        <ccEmails>nancy.casey@adm.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Pacific</fullName>
        <ccEmails>woods@ussilica.com</ccEmails>
        <ccEmails>cook@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - RLI</fullName>
        <ccEmails>taymaninventorymanagement@raillink.com</ccEmails>
        <ccEmails>clientsupportteam1.sa@raillink.com</ccEmails>
        <ccEmails>dispatch.sa@raillink.com</ccEmails>
        <ccEmails>clientsupportteam2.sa@raillink.com</ccEmails>
        <ccEmails>terminalmgr.sa@raillink.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Rockwater</fullName>
        <ccEmails>sandplant@rockwaterenergy.com</ccEmails>
        <ccEmails>jschwartz@rockwaterenergy.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Rockwood</fullName>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Rook</fullName>
        <ccEmails>ccompston@arrowmaterialservices.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - San Antonio Titan</fullName>
        <ccEmails>myrick_navejar@titanco.com</ccEmails>
        <ccEmails>rebecca_cavazos@titanco.com</ccEmails>
        <ccEmails>robert_cannon@titanco.com</ccEmails>
        <ccEmails>sanantoniotl@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Sparta</fullName>
        <ccEmails>bangart@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Streator</fullName>
        <ccEmails>bill.casey@prairietrans.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - USS Odessa</fullName>
        <ccEmails>uss.odessa@cctransload.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Victoria Equalizer</fullName>
        <ccEmails>rbrunson@texnet.net</ccEmails>
        <ccEmails>equalizerinc@hotmail.com</ccEmails>
        <ccEmails>jtate@texnet.net</ccEmails>
        <ccEmails>mnamken@texnet.net</ccEmails>
        <ccEmails>pcombs@texnet.net</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Weatherford Wildcat</fullName>
        <ccEmails>craney@wildcatminerals.com</ccEmails>
        <ccEmails>midcon@wildcatminerals.com</ccEmails>
        <ccEmails>tleigh@wildcatminerals.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inquiry Approval Notification - Wysox</fullName>
        <ccEmails>tcoleman@shalerail.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 -  Silica - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Aplite - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Aplite - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Aplite - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Kaolin - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Kaolin - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Silica - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Snowtex - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Snowtex</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 1 - Snowtex - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Snowtex</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 -  Silica - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Aplite - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Aplite - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Aplite - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Kaolin - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Kaolin - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Silica - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Snowtex - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Snowtex</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 2 - Snowtex - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <ccEmails>Halls@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Snowtex</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 -  Silica - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Aplite - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Aplite - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Aplite - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Kaolin - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Kaolin - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Silica - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 3 - Snowtex - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_3_Snowtex</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 -  Silica - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Aplite - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Aplite - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Aplite - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Kaolin - Corporate Lab</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Kaolin - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 4 - Silica - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_4_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Aplite - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Aplite - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Aplite - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Aplite</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Kaolin - Corporate Lab</fullName>
        <ccEmails>clements@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Kaolin - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Kaolin</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>kuykendall@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Sample 5 - Silica - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_5_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Berkeley</fullName>
        <ccEmails>kuykendall@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Columbia</fullName>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Corporate Lab</fullName>
        <ccEmails>clement@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Dubberly</fullName>
        <ccEmails>rice@ussilica.com</ccEmails>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Houston Lab</fullName>
        <ccEmails>treasure@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Hurtsboro</fullName>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Kosse</fullName>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Mapleton</fullName>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Mauricetown</fullName>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Mill Creek</fullName>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Montpelier</fullName>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Ottawa</fullName>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Pacific</fullName>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Rockwood</fullName>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Sparta</fullName>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Utica</fullName>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Silica - Voca</fullName>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Snowtex - Corporate Lab</fullName>
        <ccEmails>clement@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Snowtex</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - Snowtex - Kosse</fullName>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Letter_and_Document_Snowtex</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - sample 1 - Silica - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_1_Silica</template>
    </alerts>
    <alerts>
        <fullName>MSDS Documentation - sample 2 - Silica - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/MSDS_Safety_Data_Sheet_Sample_2_Silica</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Berkeley</fullName>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gusa@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Columbia</fullName>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <ccEmails>byrd@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Corporate Lab</fullName>
        <ccEmails>clement@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Dubberly</fullName>
        <ccEmails>hutchins@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Hurtsboro</fullName>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Jackson</fullName>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Mapleton</fullName>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Mauricetown</fullName>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Mill Creek</fullName>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Ottawa</fullName>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Pacific</fullName>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Rochelle</fullName>
        <ccEmails>swords@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Rockwood</fullName>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Sparta</fullName>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Utica</fullName>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case - Voca</fullName>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 1 - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_1_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 2 - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_2_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 3 - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_3_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 4 - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_4_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Berkeley</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>varner@ussilica.com</ccEmails>
        <ccEmails>Gossert@ussilica.com</ccEmails>
        <ccEmails>KEEFER@USSILICA.COM</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Columbia</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>turnerj@ussilica.com</ccEmails>
        <ccEmails>chase@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Corporate Lab</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>clements@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Dubberly</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rice@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Hurtsboro</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>stinson@ussilica.com</ccEmails>
        <ccEmails>tolbertk@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Jackson</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>watersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Kosse</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>bradley@ussilica.com</ccEmails>
        <ccEmails>wheal@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Mapleton</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>finkle@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Mauricetown</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>rogersm@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Mill Creek</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>strong@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Montpelier</fullName>
        <ccEmails>garrett@ussilica.com</ccEmails>
        <ccEmails>griffith@ussilica.com</ccEmails>
        <ccEmails>steele@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Ottawa</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>swordsa@ussilica.com</ccEmails>
        <ccEmails>duffell@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Pacific</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>langewisch@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Rochelle</fullName>
        <ccEmails>engel@ussilica.com</ccEmails>
        <ccEmails>swords@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Rockwood</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>franzen@ussilica.com</ccEmails>
        <ccEmails>windhurst@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Sparta</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>brueggen@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Utica</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>proctor@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>New Sample Case 5 - Voca</fullName>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>randall.larr@cadreproppants.com</ccEmails>
        <protected>false</protected>
        <template>Case_Workflow_Templates/ISP_Sample_Request_5_Message_to_branch_plant</template>
    </alerts>
    <alerts>
        <fullName>Notify Case Owner of Approved Case</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Rebill_Case_Approved_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Pricing of Approved Case</fullName>
        <ccEmails>husain@ussilica.com</ccEmails>
        <ccEmails>weber@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_Case_Approved_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager - BAKKEN</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager - CANADA</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager - MID-CONTINENT</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>chandler@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager - NORTHEAST</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager - PERMIAN</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>daigle@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager - ROCKIES%2FWEST</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager - SOUTH</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>chandler@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager Closed - BAKKEN</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Closed_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager Closed - CANADA</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Closed_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager Closed - MID-CONTINENT</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>chandler@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Closed_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager Closed - NORTHEAST</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Closed_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager Closed - PERMIAN</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>daigle@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Closed_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager Closed - ROCKIES%2FWEST</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Closed_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Regional Manager Closed - SOUTH</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>chandler@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Closed_Case_for_Region_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Requester of Pricing Error Case Closed</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_Error_Completed</template>
    </alerts>
    <alerts>
        <fullName>Notify Requestor of Closed Case</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/Case_Closed_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Requestor of Rejected Case</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_1__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Rebill_Rejected_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify Requestor of Rejected SPR Case</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_1__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Rebill_Rejected_notification</template>
    </alerts>
    <alerts>
        <fullName>Oil %26 Gas Technical Services Contacted</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>diep@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tanguay@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Technical_Services_Case_assignment_notification</template>
    </alerts>
    <alerts>
        <fullName>Order Increase Approval Email Sent</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Increase_Approved</template>
    </alerts>
    <alerts>
        <fullName>Order approved email</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Order approved email - Grouping 1</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Accepted_Grouping_1</template>
    </alerts>
    <alerts>
        <fullName>Order approved email - Grouping 2</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Accepted_Grouping_2</template>
    </alerts>
    <alerts>
        <fullName>Order approved email - Grouping 3</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Accepted_Grouping_3</template>
    </alerts>
    <alerts>
        <fullName>Order approved email - Grouping 4</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Accepted_Grouping_4</template>
    </alerts>
    <alerts>
        <fullName>Order approved email - Grouping 5</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Accepted_Grouping_5</template>
    </alerts>
    <alerts>
        <fullName>Order declined customer email</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Declined_Order</template>
    </alerts>
    <alerts>
        <fullName>Order declined customer email - grouping 1</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Declined_Grouping_1</template>
    </alerts>
    <alerts>
        <fullName>Order declined customer email - grouping 2</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Declined_Grouping_2</template>
    </alerts>
    <alerts>
        <fullName>Order declined customer email - grouping 3</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Declined_Grouping_3</template>
    </alerts>
    <alerts>
        <fullName>Order declined customer email - grouping 4</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Declined_Grouping_4</template>
    </alerts>
    <alerts>
        <fullName>Order declined customer email - grouping 5</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Declined_Grouping_5</template>
    </alerts>
    <alerts>
        <fullName>Order declined email - Group 1</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Declined_Grouping_1</template>
    </alerts>
    <alerts>
        <fullName>Order initially approved email - Grouping 1</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Initially_Accepted_Grouping_1</template>
    </alerts>
    <alerts>
        <fullName>Order initially approved email - Grouping 2</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Initially_Accepted_Grouping_2</template>
    </alerts>
    <alerts>
        <fullName>Order initially approved email - Grouping 3</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Initially_Accepted_Grouping_3</template>
    </alerts>
    <alerts>
        <fullName>Order initially approved email - Grouping 4</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Initially_Accepted_Grouping_4</template>
    </alerts>
    <alerts>
        <fullName>Order initially approved email - Grouping 5</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Order_Initially_Accepted_Grouping_5</template>
    </alerts>
    <alerts>
        <fullName>Pricing entered into JDE notification</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>buehl@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thorp@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thumm@ussilica.com1.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Pricing_entered_into_JDE_notification</template>
    </alerts>
    <alerts>
        <fullName>RCS Case Notification</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>chandler@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>daigle@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/New_Case_for_Account_notification</template>
    </alerts>
    <alerts>
        <fullName>Sales and Pricing notified of PO attached to case</fullName>
        <ccEmails>pricing@ussilica.com</ccEmails>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <template>Case_Workflow_Templates/Customer_Service_Notification_when_PO_Attached_to_Case</template>
    </alerts>
    <alerts>
        <fullName>Sample Request Notification of Completion</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Sample_Request_Shipped</template>
    </alerts>
    <alerts>
        <fullName>Sample Request Notification of Receipt</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Sample_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>Sample Request Notification of Shipment - Domestic</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Sample_Request_Shipped</template>
    </alerts>
    <alerts>
        <fullName>Sample Request Notification of Shipment - Export</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <ccEmails>gusa@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <field>Email_Address__c</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/Sample_Request_Shipped_export</template>
    </alerts>
    <alerts>
        <fullName>Second SPR Approval</fullName>
        <protected>false</protected>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_2__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/SPR_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Step 1 Credit Rebill Failed</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Credit_Rebill_Rejected_notification</template>
    </alerts>
    <alerts>
        <fullName>Third SPR Approval</fullName>
        <protected>false</protected>
        <recipients>
            <field>Oil_Gas_Pricing_Approver_3__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/SPR_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Transload Notification - La Salle Twin Eagle</fullName>
        <ccEmails>douglas.users@twineagle.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Case_Workflow_Templates/Grouping_Order_1_Approved_Transload</template>
    </alerts>
    <alerts>
        <fullName>Transload Operations notified of new stockout case</fullName>
        <ccEmails>SLCATransloads@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>carusona@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jenks@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Transload_Stockout_Notification</template>
    </alerts>
    <alerts>
        <fullName>Urgent Email Notification</fullName>
        <protected>false</protected>
        <recipients>
            <field>CAM_Email__c</field>
            <type>email</type>
        </recipients>
        <template>Case_Workflow_Templates/SUPPORTCaseUrgent</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved Case Status</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Field to Approved Date</fullName>
        <field>Date_Time_Approved__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Owner to Credit%2FRebill</fullName>
        <field>OwnerId</field>
        <lookupValue>Credit / Rebill</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed Reject Case Status</fullName>
        <field>Status</field>
        <literalValue>Closed Rejected</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In Approval Process Case Status</fullName>
        <field>Status</field>
        <literalValue>In Approval Process</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set Time of First Status Change</fullName>
        <field>First_Status_Change__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status Change from In Approval Process</fullName>
        <description>Status changes from In Approval Process to Approved</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status changes to Pending JDE Setup</fullName>
        <field>Status</field>
        <literalValue>Pending JDE Setup</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Account Owner Email field</fullName>
        <field>Account_Owner_Email__c</field>
        <formula>Account.Owner.Email</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Last Status Change</fullName>
        <field>Last_Status_Change__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set Urgent Flag to False</fullName>
        <field>Is_Urgent__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>3 days past due</fullName>
        <actions>
            <name>3 Days Past Due</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>3 days past due email sent to customer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Past Due Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Past_Due_Notification_Type__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>5 days past due</fullName>
        <actions>
            <name>5 Days Past Due</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>5 days past due email sent to customer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Past Due Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Past_Due_Notification_Type__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>7 days past due</fullName>
        <actions>
            <name>7 Days Past Due</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>7 days past due email sent to customer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Past Due Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Past_Due_Notification_Type__c</field>
            <operation>equals</operation>
            <value>7</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cancelled Past Due Order</fullName>
        <actions>
            <name>Cancelled Past Due Order</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Cancelled Past Due Order</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Past Due Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Past_Due_Notification_Type__c</field>
            <operation>equals</operation>
            <value>Cancellation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Declined Customer Inquiry</fullName>
        <actions>
            <name>Order declined customer email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Declined Email Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Declined</value>
        </criteriaItems>
        <description>To be used to send automated emails when an order inquiry is declined</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit App - Missing signature</fullName>
        <actions>
            <name>Credit Application Missing Signature</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email sent to sales noting missing signature</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Applications</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sales - No signature on credit app</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit App - Notify Sales and Customer Service of New Account</fullName>
        <actions>
            <name>Credit Application - New accounts activated</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sales Rep and Customer Service notified of new account</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Applications</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Account Activated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit App - missing company financials</fullName>
        <actions>
            <name>Credit Application Missing Company Financials</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email sent to sales noting missing company financials</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Applications</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sales - Need company financials</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit App - missing tax exempt form</fullName>
        <actions>
            <name>Credit Application Missing Tax Exempt Form</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email sent to sales noting missing tax exempt form</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Applications</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sales - No tax exempt form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit Application Approved - New Account Setup Needed</fullName>
        <actions>
            <name>Credit Application - Create new accounts</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Master notified to set up account</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit Applications</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Master Setup</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit%2FRebill - Reminder to close</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit / Rebill</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Master - Notify Credit to Activate Account</fullName>
        <actions>
            <name>Customer Master - Notify Credit Team to activate account</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Credit Team notified to activate account</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Master Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_activation_request_to_credit_team__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Decline of Customer Inquiry - Set 1</fullName>
        <actions>
            <name>Order declined email - Group 1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order declined email sent to customer for grouping 1</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Order_Status1__c</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <description>To be used to send automated emails when an order inquiry is declined</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Freight Rate - Freight Rate Entered into JDE</fullName>
        <actions>
            <name>Freight Rate - Entered into JDE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sales notified of case closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.JDE_Setup_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Freight Rate - New Rate Request Accepted - Rail</fullName>
        <actions>
            <name>Freight Rate accepted by customer - rail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Transportation notified of freight rate acceptance</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Business_Secured1__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Mode_of_Transportation1__c</field>
            <operation>equals</operation>
            <value>Rail</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Freight Rate - New Rate Request Accepted - Truck</fullName>
        <actions>
            <name>Freight Rate accepted by customer - not rail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Transportation notified of freight rate acceptance</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Business_Secured1__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Mode_of_Transportation1__c</field>
            <operation>equals</operation>
            <value>Truck,Barge,Ocean Freight,Air Freight</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Freight Rate Request Completed</fullName>
        <actions>
            <name>Freight Rate Request Completed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Requester notified of rate request complete</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Notification_to_Requester__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Freight Rate Request Received - Rail</fullName>
        <actions>
            <name>Freight Rate Request - Rail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Transportation notified of freight rate request received</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Mode_of_Transportation1__c</field>
            <operation>equals</operation>
            <value>Rail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Freight Rate Request Received - Truck</fullName>
        <actions>
            <name>Freight Rate Request - Truck</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Logistics notified of new rate request</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Mode_of_Transportation1__c</field>
            <operation>notEqual</operation>
            <value>Rail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Berkeley</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>contains</operation>
            <value>Berkeley Springs Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Columbia</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Columbia Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Florisil</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Florisil</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Florisil Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Hurtsboro</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Hurtsboro Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Jackson</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Jackson Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Mapleton</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Mapleton Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Mill Creek</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Mill Creek Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Montpelier</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Montpelier Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Ottawa</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Ottawa Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Pacific</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Pacific Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Rochelle</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Rochelle Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Rockwood</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Rockwood Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when JDE Setup Complete - Voca</fullName>
        <actions>
            <name>ISP Pricing - Customer Service Notified of JDE Setup - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Customer Service notified of completed JDE setup</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Voca Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Cadre</fullName>
        <actions>
            <name>ISP - PO on quote - Cadre</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Voca Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Columbia</fullName>
        <actions>
            <name>ISP - PO on quote - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Columbia Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Dubberly</fullName>
        <actions>
            <name>ISP - PO on quote - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Dubberly Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Florisil</fullName>
        <actions>
            <name>ISP - PO on quote - Florisil</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Florisil Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Hurtsboro</fullName>
        <actions>
            <name>ISP - PO on quote - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Hurtsboro Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Jackson</fullName>
        <actions>
            <name>ISP - PO on quote - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Jackson Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Mapleton</fullName>
        <actions>
            <name>ISP - PO on quote - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Mapleton Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Mauricetown</fullName>
        <actions>
            <name>ISP - PO on quote - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Mauricetown Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Mill Creek</fullName>
        <actions>
            <name>ISP - PO on quote - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Mill Creek Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Montpelier</fullName>
        <actions>
            <name>ISP - PO on quote - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Montpelier Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Ottawa</fullName>
        <actions>
            <name>ISP - PO on quote - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Ottawa Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Pacific</fullName>
        <actions>
            <name>ISP - PO on quote - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Pacific Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Customer Service Notification when PO Attached to Quote - Rockwood</fullName>
        <actions>
            <name>ISP - PO on quote - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email notification sent to branch plant to advise of PO on quote</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Quote_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>Rockwood Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_attached_to_quote_record__c</field>
            <operation>equals</operation>
            <value>YES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Email sent to credit with credit hold removal request</fullName>
        <actions>
            <name>ISP - Credit team notified to remove credit hold</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Credit team notified to remove credit hold</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Customer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Credit Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Order Confirmation for ISP Customer Care</fullName>
        <actions>
            <name>ISP Customer Care - Email Notification of Order Confirmation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order Confirmation Email Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Customer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Customer_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Add_additional_statement_to_notification__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Order Confirmation for ISP Customer Care w%2Fstatement</fullName>
        <actions>
            <name>ISP Customer Care - Email Notification of Order Confirmation w notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order Confirmation Email Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Customer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Customer_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Add_additional_statement_to_notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Send CoA to Ottawa Lab</fullName>
        <actions>
            <name>ISP - Send CoA Request to Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CoA Request Sent to Lab</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Certificate of Analysis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>CoA Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.General_Branch_Plant__c</field>
            <operation>equals</operation>
            <value>OTTAWA DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP Certificate of Analysis - Ottawa</fullName>
        <actions>
            <name>ISP CoA Request - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Certificate of Analysis request sent to branch plant</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Certificate of Analysis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Quality</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Initial Approval Sent to Customer</fullName>
        <actions>
            <name>Initial Order Approval</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Initial Approval Email Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Initial Approval of Customer Inquiry - Set 1</fullName>
        <actions>
            <name>Order initially approved email - Grouping 1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 customer initially notified of approval</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Order_Status1__c</field>
            <operation>equals</operation>
            <value>Partially Accepted,Accepted</value>
        </criteriaItems>
        <description>To be used to send automated emails when an order inquiry is approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Artesia</fullName>
        <actions>
            <name>Branch Plant Notification - Artesia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9732</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Avis</fullName>
        <actions>
            <name>Branch Plant Notification - Avis</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9715</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Barnhart</fullName>
        <actions>
            <name>Branch Plant Notification - Barnhart</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Barnhart1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9794</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Berkeley</fullName>
        <actions>
            <name>Branch Plant Notification - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>151</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Bossier</fullName>
        <actions>
            <name>Branch Plant Notification - Bossier Arrow</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_PlantTL__c</field>
            <operation>equals</operation>
            <value>BOSSIER CITY - ARROW MATERIAL SERVICES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Brush</fullName>
        <actions>
            <name>Branch Plant Notification - Brush Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9725</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Bryan Maalt</fullName>
        <actions>
            <name>Branch Plant Notification - Bryan Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9778</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Burlington Rockwater</fullName>
        <actions>
            <name>Inquiry Approval Notification - Rockwater</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BURLINGTON - ROCKWATER ENERGY SOLUTIONS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Butler</fullName>
        <actions>
            <name>Branch Plant Notification - Butler Transflo</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9718</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Carbondale</fullName>
        <actions>
            <name>Inquiry Approval Notification - Carbondale</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9798</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Carlsbad Titan</fullName>
        <actions>
            <name>Branch Plant Notification - Carlsbad Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Carlsbad Titan1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9781</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Casper</fullName>
        <actions>
            <name>Branch Plant Notification - Casper C%26C</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9775</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Columbia1</fullName>
        <actions>
            <name>Branch Plant Notification - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>171</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Douglas Twin Eagle</fullName>
        <actions>
            <name>Branch Plant Notification - Douglas</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Douglas1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9790</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Dubberly1</fullName>
        <actions>
            <name>Branch Plant Notification - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>561</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - East Fairview 1</fullName>
        <actions>
            <name>Branch Plant Notification - East Fairview Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9735</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - East Fairview Northstar</fullName>
        <actions>
            <name>Branch Plant Notification - East Fairview Northstar</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - East Fairview Northstar1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9789</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - East Liverpool Bell 1</fullName>
        <actions>
            <name>Branch Plant Notification - East Liverpool SH Bell</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9706</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Enid Maalt 1</fullName>
        <actions>
            <name>Branch Plant Notification - Enid Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9713</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Ft Stockton Titan 1</fullName>
        <actions>
            <name>Branch Plant Notification - Ft Stockton Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9772</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Ft Worth North Maalt 1</fullName>
        <actions>
            <name>Branch Plant Notification - Ft Worth Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9714</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Ft Worth South Maalt</fullName>
        <actions>
            <name>Branch Plant Notification - Ft Worth Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9787</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Gonzales1</fullName>
        <actions>
            <name>Branch Plant Notification - Gonzales Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9738</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Hannibal1</fullName>
        <actions>
            <name>Branch Plant Notification - Hannibal MRIE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9727</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Hughes Spring</fullName>
        <actions>
            <name>Branch Plant Notification - Hughes Spring</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9793</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Hurtsboro1</fullName>
        <actions>
            <name>Branch Plant Notification - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>581</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Jackson1</fullName>
        <actions>
            <name>Branch Plant Notification - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>71</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Kelim Omnitrax 1</fullName>
        <actions>
            <name>Branch Plant Notification - Kelim Omnitrax</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9768</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Kosse1</fullName>
        <actions>
            <name>Branch Plant Notification - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>541</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - La Salle Twin Eagle</fullName>
        <actions>
            <name>Transload Notification - La Salle Twin Eagle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9791</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Levelland Titan 1</fullName>
        <actions>
            <name>Branch Plant Notification - Levelland Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Levelland Titan1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9703</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Loving Rangeland</fullName>
        <actions>
            <name>Branch Plant Notification - Loving Rangeland</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9740</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Lubbock Titan 1</fullName>
        <actions>
            <name>Branch Plant Notification - Lubbock Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Lubbock Titan1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9716</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Mapleton1</fullName>
        <actions>
            <name>Branch Plant Notification - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>131</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Mauricetown1</fullName>
        <actions>
            <name>Branch Plant Notification - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>261</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Mckees Rock MRIE 1</fullName>
        <actions>
            <name>Branch Plant Notification - Mckees Rock MRIE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9720</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Mill Creek1</fullName>
        <actions>
            <name>Branch Plant Notification - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>81</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Montpelier1</fullName>
        <actions>
            <name>Branch Plant Notification - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>521</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - New Town1</fullName>
        <actions>
            <name>Branch Plant Notification - New Town</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - New Town1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9736</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Odessa USS</fullName>
        <actions>
            <name>Branch Plant Notification - Odessa USS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Odessa USS1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9786</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Odessa Wildcat1</fullName>
        <actions>
            <name>Branch Plant Notification - Odessa Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9753</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Ottawa1</fullName>
        <actions>
            <name>Branch Plant Notification - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>501</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Pacific1</fullName>
        <actions>
            <name>Branch Plant Notification - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>61</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Penhold</fullName>
        <actions>
            <name>Branch Plant Notification - Penhold</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9729</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Rochelle CSS1</fullName>
        <actions>
            <name>Branch Plant Notification - Rochelle CSS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9769</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Rochelle1</fullName>
        <actions>
            <name>Branch Plant Notification - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>702</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Rockdale</fullName>
        <actions>
            <name>Branch Plant Notification - Rockdale</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>ROCKDALE - TIDEWATER</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Rockwood1</fullName>
        <actions>
            <name>Branch Plant Notification - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>511</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Rook</fullName>
        <actions>
            <name>Inquiry Approval Notification - Rook</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>ROOK - MODERN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - San Antonio RLI1</fullName>
        <actions>
            <name>Branch Plant Notification - San Antonio RLI</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - San Antonio RLI1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9766</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - San Antonio Titan1</fullName>
        <actions>
            <name>Branch Plant Notification - San Antonio Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9701</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - San Augustine</fullName>
        <actions>
            <name>Branch Plant Notification - San Augustine Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9734</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Shattuck</fullName>
        <actions>
            <name>Branch Plant Notification - Shattuck WB Johnston</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9792</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Sparta1</fullName>
        <actions>
            <name>Branch Plant Notification - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Sparta1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>571</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Three Rivers</fullName>
        <actions>
            <name>Branch Plant Notification - Three Rivers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>THREE RIVERS - TIDEWATER LOGISTICS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Victoria1</fullName>
        <actions>
            <name>Branch Plant Notification - Victoria Equalizer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Victoria Equalizer1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9711</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Weatherford1</fullName>
        <actions>
            <name>Branch Plant Notification - Weatherford Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9731</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Williston Rockwater 1</fullName>
        <actions>
            <name>Grouping 2 Notification - Williston Rockwater</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9722</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Wyalusing</fullName>
        <actions>
            <name>Branch Plant Notification - Wyalusing</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9756</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - Wysox1</fullName>
        <actions>
            <name>Branch Plant Notification - Wysox Shale Rail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9779</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - fairmont 1</fullName>
        <actions>
            <name>Branch Plant Notification - Fairmont Transflo</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Fairmont Transflo1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9717</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Approval Notification - laredo wildcat 1</fullName>
        <actions>
            <name>Branch Plant Notification - Laredo Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Laredo Wildcat1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Grouping 1 Transload has been notified</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant_Address_Number__c</field>
            <operation>equals</operation>
            <value>9724</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inquiry Increase Approval Notification - Avis</fullName>
        <actions>
            <name>Order Increase Approval Email Sent</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order Increase Approval Sent to Customer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 and (4 or 5 or 6 or 7 or 8)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.New_or_Increase__c</field>
            <operation>equals</operation>
            <value>Order Increase,New Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>AVIS - MODERN MATERIAL SERVICES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant1__c</field>
            <operation>equals</operation>
            <value>AVIS - MODERN MATERIAL SERVICES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant2__c</field>
            <operation>equals</operation>
            <value>AVIS - MODERN MATERIAL SERVICES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant3__c</field>
            <operation>equals</operation>
            <value>AVIS - MODERN MATERIAL SERVICES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant4__c</field>
            <operation>equals</operation>
            <value>AVIS - MODERN MATERIAL SERVICES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Avis</fullName>
        <actions>
            <name>Branch Plant Notification - Avis</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>AVIS - MODERN MATERIAL SERVICES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Barnhart</fullName>
        <actions>
            <name>Branch Plant Notification - Barnhart</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>inside sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BARNHART - TSS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Berkeley</fullName>
        <actions>
            <name>Branch Plant Notification - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BERKELEY SPRINGS DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Bossier</fullName>
        <actions>
            <name>Branch Plant Notification - Bossier Arrow</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BOSSIER CITY - ARROW MATERIAL SERVICES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Bridgeton</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BRIDGETON - LAFARGE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Brush</fullName>
        <actions>
            <name>Branch Plant Notification - Brush Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BRUSH - WILDCAT MINERALS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Bryan</fullName>
        <actions>
            <name>Branch Plant Notification - Bryan Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BRYAN - MAALT TRANSPORT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Butler</fullName>
        <actions>
            <name>Branch Plant Notification - Butler Transflo</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>BUTLER - TRANSFLO</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Carlsbad</fullName>
        <actions>
            <name>Branch Plant Notification - Carlsbad Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Carlsbad Titan1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>CARLSBAD - TITAN TRANSLOADING</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Casper</fullName>
        <actions>
            <name>Branch Plant Notification - Casper C%26C</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>&quot;CASPER - C&amp;C TRANSLOAD, LLC&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Columbia</fullName>
        <actions>
            <name>Branch Plant Notification - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>COLUMBIA DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Douglas</fullName>
        <actions>
            <name>Branch Plant Notification - Douglas</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>DOUGLAS - TWIN EAGLE SAND LOGISTICS LLC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Dubberly</fullName>
        <actions>
            <name>Branch Plant Notification - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>DUBBERLY DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - East Fairview Wildcat</fullName>
        <actions>
            <name>Branch Plant Notification - East Fairview Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>EAST FAIRVIEW - WILDCAT MINERALS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - East Liverpool</fullName>
        <actions>
            <name>Branch Plant Notification - East Liverpool SH Bell</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>EAST LIVERPOOL - S.H. BELL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Enid</fullName>
        <actions>
            <name>Branch Plant Notification - Enid Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>ENID - MAALT TRANSPORT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Fairfield</fullName>
        <actions>
            <name>Branch Plant Notification - Fairmont Transflo</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>FAIRMONT - TRANSFLO</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Fairmont</fullName>
        <actions>
            <name>Branch Plant Notification - Fairmont Transflo</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Fairmont Transflo1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>FAIRMONT - TRANSFLO</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Ft Worth North</fullName>
        <actions>
            <name>Branch Plant Notification - Ft Worth Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>FT WORTH NORTH - MAALT TRANSPORT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Ft Worth South</fullName>
        <actions>
            <name>Branch Plant Notification - Ft Worth Maalt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>FT WORTH SOUTH - MAALT TRANSPORT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Gonzales</fullName>
        <actions>
            <name>Branch Plant Notification - Gonzales Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>GONZALES - WILDCAT MINERALS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Hannibal</fullName>
        <actions>
            <name>Branch Plant Notification - Hannibal MRIE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>HANNIBAL - MRIE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Hughes Spring</fullName>
        <actions>
            <name>Branch Plant Notification - Hughes Spring</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>HUGHES SPRINGS - ETPS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Hurtsboro</fullName>
        <actions>
            <name>Branch Plant Notification - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>HURTSBORO DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Jackson</fullName>
        <actions>
            <name>Branch Plant Notification - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>JACKSON DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Kelim</fullName>
        <actions>
            <name>Branch Plant Notification - Kelim Omnitrax</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>KELIM - OMNITRAX LOGISTICS SERVICES</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Kosse</fullName>
        <actions>
            <name>Branch Plant Notification - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>KOSSE DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Laredo</fullName>
        <actions>
            <name>Branch Plant Notification - Laredo Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>LAREDO - WILDCAT MINERALS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Levelland</fullName>
        <actions>
            <name>Branch Plant Notification - Levelland Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Branch Plant Notification - Levelland Titan1</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>LEVELLAND - TITAN TRANSLOADING</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Lubbock</fullName>
        <actions>
            <name>Branch Plant Notification - Lubbock Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>LUBBOCK - TITAN TRANSLOADING</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Mapleton</fullName>
        <actions>
            <name>Branch Plant Notification - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>MAPLETON DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Mauricetown</fullName>
        <actions>
            <name>Branch Plant Notification - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>MAURICETOWN DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - McKees</fullName>
        <actions>
            <name>Branch Plant Notification - Mckees Rock MRIE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>MCKEES ROCKS - MRIE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Mill Creek</fullName>
        <actions>
            <name>Branch Plant Notification - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>MILL CREEK DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Montpelier</fullName>
        <actions>
            <name>Branch Plant Notification - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>MONTPELIER DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - New Town</fullName>
        <actions>
            <name>Branch Plant Notification - New Town</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>NEW TOWN - WILDCAT MINERALS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Odessa USS</fullName>
        <actions>
            <name>Branch Plant Notification - Odessa USS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>ODESSA RAILPORT - UPDS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Ottawa</fullName>
        <actions>
            <name>Branch Plant Notification - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>OTTAWA DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Pacific</fullName>
        <actions>
            <name>Branch Plant Notification - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>PACIFIC DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Rochelle</fullName>
        <actions>
            <name>Branch Plant Notification - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>ROCHELLE DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Rochelle CSS</fullName>
        <actions>
            <name>Branch Plant Notification - Rochelle CSS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>ROCHELLE - CSS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Rockwood</fullName>
        <actions>
            <name>Branch Plant Notification - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>ROCKWOOD DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - San Antonio RLI</fullName>
        <actions>
            <name>Branch Plant Notification - San Antonio RLI</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>SAN ANTONIO - RLI</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - San Antonio Titan</fullName>
        <actions>
            <name>Branch Plant Notification - San Antonio Titan</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>SAN ANTONIO - TITAN TRANSLOADING</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Shattuck</fullName>
        <actions>
            <name>Branch Plant Notification - Shattuck WB Johnston</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>SHATTUCK - WB JOHNSTON</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Sparta</fullName>
        <actions>
            <name>Branch Plant Notification - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Plant</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>SPARTA DIRECT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Victoria</fullName>
        <actions>
            <name>Branch Plant Notification - Victoria Equalizer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>VICTORIA - EQUALIZER INC</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Weatherford</fullName>
        <actions>
            <name>Branch Plant Notification - Weatherford Wildcat</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>WEATHERFORD - WILDCAT MINERALS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inside Sales Transload Notification - Williston</fullName>
        <actions>
            <name>Branch Plant Notification - Williston Rockwater</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Approval Email Sent to Transload</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inside Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_to_Transload1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Branch_Plant__c</field>
            <operation>equals</operation>
            <value>WILLISTON - ROCKWATER ENERGY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>International Sales Order Received</fullName>
        <actions>
            <name>ISP International Sales Order Received Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order Received Notification Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP International Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Order Receipt Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification when PO is attached to CS case</fullName>
        <actions>
            <name>Sales and Pricing notified of PO attached to case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PO attached notification sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry,ISP Customer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.PO_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Account Owner of Closed Case</fullName>
        <actions>
            <name>Email Account Owner closed case status</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Closed Email sent to Account Owner</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 or 3 or 4 or 5)</booleanFilter>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Quality Issue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Issue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Past Due Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <description>Notify Account Owner of closed case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Account Owner of New Cases</fullName>
        <actions>
            <name>Email Account Owner - New Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account owner notified of new case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Issue,Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Account Owner of new case</fullName>
        <actions>
            <name>Email Account Owner - New Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email sent to Account Owner</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Notify account owner if case is created or account changes.</description>
        <formula>OR(  AND(ISNEW(),
 (RecordType.Id = &apos;012a0000001Nd49&apos;),
OR ( RecordType.Id = &apos;012a0000001Nd4D&apos;),
OR ( RecordType.Id = &apos;012a0000001Nd4C&apos;),
OR ( RecordType.Id = &apos;012a0000001Nd4E&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of Closed Case - BAKKEN</fullName>
        <actions>
            <name>Notify Regional Manager Closed - BAKKEN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Closed Email sent to RSM - Bakken</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>BAKKEN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Regional Manager when a case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of Closed Case - CANADA</fullName>
        <actions>
            <name>Notify Regional Manager Closed - CANADA</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Closed Email sent to RSM - Canada</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>CANADA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Regional Manager when a case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of Closed Case - MID-CONTINENT</fullName>
        <actions>
            <name>Notify Regional Manager Closed - MID-CONTINENT</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Closed Email sent to RSM - MidCon</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>MID-CONTINENT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Regional Manager when a case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of Closed Case - NORTHEAST</fullName>
        <actions>
            <name>Notify Regional Manager Closed - NORTHEAST</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Closed Email sent to RSM - Northeast</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>NORTHEAST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Regional Manager when a case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of Closed Case - PERMIAN</fullName>
        <actions>
            <name>Notify Regional Manager Closed - PERMIAN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Closed Email sent to RSM - Permian</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>PERMIAN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Regional Manager when a case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of Closed Case - ROCKIES%2FWEST</fullName>
        <actions>
            <name>Notify Regional Manager Closed - ROCKIES%2FWEST</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Closed Email sent to RSM - Rockies</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>ROCKIES/WEST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Regional Manager when a case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of Closed Case - SOUTH</fullName>
        <actions>
            <name>Notify Regional Manager Closed - SOUTH</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email sent to RSM - South</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>SOUTH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Regional Manager when a case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of New Case - BAKKEN</fullName>
        <actions>
            <name>Notify Regional Manager - BAKKEN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New case received notification sent to RSM - Bakken</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>BAKKEN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request,Order Issue,Inquiry,Delays,Transload Stockout</value>
        </criteriaItems>
        <description>Notify Regional Manager when a new case has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of New Case - CANADA</fullName>
        <actions>
            <name>Notify Regional Manager - CANADA</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New case received notification sent to RSM - Canada</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>CANADA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request,Order Issue,Inquiry,Delays,Transload Stockout</value>
        </criteriaItems>
        <description>Notify Regional Manager when a new case has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of New Case - MID-CONTINENT</fullName>
        <actions>
            <name>Notify Regional Manager - MID-CONTINENT</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New case received notification sent to RSM - MidCon</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>MID-CONTINENT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request,Order Issue,Inquiry,Delays,Transload Stockout</value>
        </criteriaItems>
        <description>Notify Regional Manager when a new case has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of New Case - NORTHEAST</fullName>
        <actions>
            <name>Notify Regional Manager - NORTHEAST</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New case received notification sent to RSM - Northeast</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>NORTHEAST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request,Order Issue,Inquiry,Delays,Transload Stockout</value>
        </criteriaItems>
        <description>Notify Regional Manager when a new case has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of New Case - PERMIAN</fullName>
        <actions>
            <name>Notify Regional Manager - PERMIAN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New case received notification sent to RSM - Permian</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>PERMIAN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request,Order Issue,Inquiry,Delays,Transload Stockout</value>
        </criteriaItems>
        <description>Notify Regional Manager when a new case has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of New Case - ROCKIES%2FWEST</fullName>
        <actions>
            <name>Notify Regional Manager - ROCKIES%2FWEST</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New case received notification sent to RSM - Rockies</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>ROCKIES/WEST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request,Order Issue,Inquiry,Delays,Transload Stockout</value>
        </criteriaItems>
        <description>Notify Regional Manager when a new case has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Regional Manager of New Case - South</fullName>
        <actions>
            <name>Email Account Owner - New Case - South</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New case received notification sent to RSM - South</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Region__c</field>
            <operation>equals</operation>
            <value>SOUTH</value>
        </criteriaItems>
        <description>Notify Regional Manager when a new case has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Requester of Pricing Error Case Closed</fullName>
        <actions>
            <name>Notify Requester of Pricing Error Case Closed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Requester notified of case closed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Error</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Requestor when case closed</fullName>
        <actions>
            <name>Notify Requestor of Closed Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Pricing Request,Oil &amp; Gas Pricing Request,Credit / Rebill</value>
        </criteriaItems>
        <description>Notify Requestor of closed case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Transload Operations on Stockout</fullName>
        <actions>
            <name>Transload Operations notified of new stockout case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Transload Operations have been notified of stockout</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transload Stockout</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Aplite Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Aplite - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley Springs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Aplite Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Aplite - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Aplite Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Aplite - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Kaolin Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Kaolin - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 -  Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley Springs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Dubberly</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Dubberly</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Hurtsboro</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Hurtsboro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Jackson</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Mapleton</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mapleton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Mauricetown</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mauricetown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Mill Creek</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mill Creek</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Pacific</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Pacific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Rockwood</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rockwood</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Sparta</fullName>
        <actions>
            <name>MSDS Documentation - sample 1 - Silica - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Sparta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Utica</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Utica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Silica Case Received - Voca</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Voca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 1 - Snowtex Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Snowtex - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>contains</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Aplite Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Aplite - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Aplite Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Aplite - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Aplite Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Aplite - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Kaolin Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Kaolin - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Kaolin Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Kaolin - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Silica Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 -  Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Silica Case Received - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G Sample 2 - Silica Case Received - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Oil %26 Gas Sample 1 - Kaolin Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Kaolin - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pricing entered into JDE notification</fullName>
        <actions>
            <name>Pricing entered into JDE notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Oil &amp; Gas SPR Approval Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pricing_Entered_in_JDE__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RCS Related</fullName>
        <actions>
            <name>RCS Case Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email sent to Ernie Christian</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Credit / Rebill,Oil &amp; Gas Customer Issue,Oil &amp; Gas Customer Order</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RCS__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Case is RCS related</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Record Time of Last Status Change</fullName>
        <actions>
            <name>Update Last Status Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Records the date and time the last time the status has changed</description>
        <formula>ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Record time of first status change</fullName>
        <actions>
            <name>Set Time of First Status Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Records time of the first status change</description>
        <formula>AND( ISCHANGED( Status ),   ISBLANK( First_Status_Change__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sample - Auto notification when sample is received</fullName>
        <actions>
            <name>Sample Request Notification of Receipt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Received Notification sent to Customer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <description>This is to be used when a sample is received which notifies customer that the request has been received and is being processed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Aplite Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Aplite - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley Springs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Aplite Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Aplite - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Aplite Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Aplite - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Kaolin Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Kaolin - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Kaolin Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Kaolin - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 -  Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley Springs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Dubberly</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Dubberly</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Hurtsboro</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Hurtsboro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Jackson</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Mapleton</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mapleton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Mauricetown</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mauricetown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Mill Creek</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mill Creek</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Pacific</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Pacific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Rochelle</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rochelle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Rockwood</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rockwood</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Sparta</fullName>
        <actions>
            <name>MSDS Documentation - sample 1 - Silica - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Sparta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Utica</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Utica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Silica Case Received - Voca</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Silica - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Voca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Snowtex Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Snowtex - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 1 - Snowtex Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 1 - Snowtex - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 1 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_1_MSDS__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Aplite Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Aplite - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Aplite Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Aplite - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Aplite Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Aplite - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Kaolin Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Kaolin - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Kaolin Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Kaolin - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 -  Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Dubberly</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Dubberly</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Hurtsboro</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Hurtsboro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Jackson</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Mapleton</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mapleton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Mauricetown</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mauricetown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Mill Creek</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mill Creek</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Pacific</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Pacific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Rochelle</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rochelle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Rockwood</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rockwood</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Sparta</fullName>
        <actions>
            <name>MSDS Documentation - sample 2 - Silica - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Sparta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Utica</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Utica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Silica Case Received - Voca</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Silica - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Voca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Snowtex Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Snowtex - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 2 - Snowtex Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 2 - Snowtex - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 2 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_2_MSDS__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Aplite Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Aplite - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Aplite Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Aplite - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Aplite Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Aplite - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Kaolin Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Kaolin - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Kaolin Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Kaolin - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 -  Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Dubberly</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Dubberly</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Hurtsboro</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Hurtsboro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Jackson</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Mapleton</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mapleton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Mauricetown</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mauricetown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Mill Creek</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mill Creek</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Pacific</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Pacific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Rochelle</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rochelle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Rockwood</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rockwood</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Sparta</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Sparta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Utica</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Utica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Silica Case Received - Voca</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Silica - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Voca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 3 - Snowtex Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 3 - Snowtex - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 3 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_3_MSDS__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Aplite Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Aplite - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Aplite Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Aplite - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Aplite Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Aplite - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Kaolin Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Kaolin - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Kaolin Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Kaolin - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 -  Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Dubberly</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Dubberly</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Hurtsboro</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Hurtsboro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Jackson</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Mapleton</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mapleton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Mauricetown</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mauricetown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Mill Creek</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mill Creek</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Pacific</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Pacific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Rochelle</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rochelle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Rockwood</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rockwood</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Sparta</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Sparta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Utica</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Utica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 4 - Silica Case Received - Voca</fullName>
        <actions>
            <name>MSDS Documentation - Sample 4 - Silica - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 4 - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Voca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_4_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Aplite Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Aplite - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Aplite Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Aplite - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Aplite Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Aplite - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Kaolin Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Kaolin - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Kaolin Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Kaolin - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Dubberly</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Dubberly</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Hurtsboro</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Hurtsboro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Jackson</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Mapleton</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mapleton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Mauricetown</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mauricetown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Mill Creek</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mill Creek</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Pacific</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Pacific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Rochelle</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rochelle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Rockwood</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rockwood</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Sparta</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Sparta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Utica</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Utica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample 5 - Silica Case Received - Voca</fullName>
        <actions>
            <name>MSDS Documentation - Sample 5 - Silica - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case 5 - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Voca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_5_MSDS__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Internal_Resource__c</field>
            <operation>equals</operation>
            <value>Sample Resource Location</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Aplite - Berkeley</fullName>
        <actions>
            <name>New Sample Case - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests,Oil &amp; Gas Sample Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley Springs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Aplite - Corporate Lab</fullName>
        <actions>
            <name>New Sample Case - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Aplite - Montpelier</fullName>
        <actions>
            <name>New Sample Case - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Aplite</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Kaolin - Corporate Lab</fullName>
        <actions>
            <name>New Sample Case - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Kaolin - Kosse</fullName>
        <actions>
            <name>New Sample Case - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Kaolin</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Berkeley</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Berkeley</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Berkeley Springs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Columbia</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Columbia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Columbia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Dubberly</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Dubberly</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Dubberly</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Hurtsboro</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Hurtsboro</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Hurtsboro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Jackson</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Jackson</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Jackson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Mapleton</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Mapleton</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mapleton</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Mauricetown</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Mauricetown</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mauricetown</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Mill Creek</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Mill Creek</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Mill Creek</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Montpelier</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Montpelier</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Montpelier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Ottawa</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Ottawa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Ottawa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Pacific</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Pacific</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Pacific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Rochelle</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Rochelle</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rochelle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Rockwood</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Rockwood</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Rockwood</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Sparta</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Sparta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Sparta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Utica</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Utica</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Utica</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Silica - Voca</fullName>
        <actions>
            <name>MSDS Documentation - Silica - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Voca</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Voca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Silica</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Snowtex - Corporate Lab</fullName>
        <actions>
            <name>MSDS Documentation - Snowtex - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Corporate Lab</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>ISP Corporate Lab</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request - Snowtex - Kosse</fullName>
        <actions>
            <name>MSDS Documentation - Snowtex - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Sample Case - Kosse</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>MSDS documentation sent to lab</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Sample Request Submitted to Sample Resource Location</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sample_Resource_Location__c</field>
            <operation>equals</operation>
            <value>Kosse</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.MSDS_Field__c</field>
            <operation>equals</operation>
            <value>Snowtex</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request Processed and Sent</fullName>
        <actions>
            <name>Sample Request Notification of Completion</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Complete Notification sent to Customer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request Received Notification to Customer</fullName>
        <actions>
            <name>Sample Request Notification of Receipt</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Received Notification sent to Customer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request Shipped and Customer Notified - Domestic</fullName>
        <actions>
            <name>Sample Request Notification of Shipment - Domestic</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Shipped Notification sent to Customer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Domestic_or_Export__c</field>
            <operation>equals</operation>
            <value>Domestic</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample Request Shipped and Customer Notified - Export</fullName>
        <actions>
            <name>Sample Request Notification of Shipment - Export</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sample Request Shipped Notification sent to Customer</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Sample Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Domestic_or_Export__c</field>
            <operation>equals</operation>
            <value>Export</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sample or Quality Issue Case</fullName>
        <actions>
            <name>Oil %26 Gas Technical Services Contacted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Technical Services contacted</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quality Issue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting on Internal Resource</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Urgent email Notification to CAM</fullName>
        <actions>
            <name>Urgent Email Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>set Urgent Flag to False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Is_Urgent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>3 days past due email sent to customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>5 days past due email sent to customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>7 days past due email sent to customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Account owner notified of new case</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Approval Email Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Approval Email Sent to Plant</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Approval Email Sent to Transload</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Approval Email Sent to Transload - RLI</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Cancelled Past Due Order</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Closed Email sent to Account Owner</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Closed Email sent to RSM - Bakken</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Closed Email sent to RSM - Canada</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Closed Email sent to RSM - MidCon</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Closed Email sent to RSM - Northeast</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Closed Email sent to RSM - Permian</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Closed Email sent to RSM - Rockies</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>CoA Request Sent to Lab</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Credit Team notified to activate account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Credit team notified to remove credit hold</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Customer Master notified to set up account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Customer Service notified of completed JDE setup</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Declined Email Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Order approved email has been sent.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Email notification sent to branch plant to advise of PO on quote</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Email sent to Account Owner</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Email sent to Ernie Christian</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Email sent to RSM - South</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Email sent to sales noting missing company financials</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Email sent to sales noting missing signature</fullName>
        <assignedToType>creator</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Email sent to sales noting missing tax exempt form</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Executive Level Approved</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 1 Transload has been notified</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 1 customer initially notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 1 customer notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 1 customer notified of decline</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 2 Customer initially notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 2 Customer notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 2 Transload has been notified</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 2 customer notified of decline</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 3 Customer initially notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 3 Customer notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 3 Transload has been notified</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 3 customer notified of decline</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 4 Customer initially notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 4 Customer notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 4 Transload has been notified</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 4 customer notified of decline</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 5 Transload has been notified</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 5 customer initially notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 5 customer notified of approval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Grouping 5 customer notified of decline</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Initial Approval Email Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Logistics notified of new rate request</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>MSDS documentation sent to lab</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New Certificate of Analysis request sent to branch plant</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New case received notification sent to RSM - Bakken</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New case received notification sent to RSM - Canada</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New case received notification sent to RSM - MidCon</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New case received notification sent to RSM - Northeast</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New case received notification sent to RSM - Permian</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New case received notification sent to RSM - Rockies</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>New case received notification sent to RSM - South</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Order Confirmation Email Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Order Increase Approval Sent to Customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Order Received Notification Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Order declined email sent to customer for grouping 1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>PO attached notification sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Requester notified of case closed</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Requester notified of rate request complete</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Sales Rep and Customer Service notified of new account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Sales notified of case closed</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Sample Request Complete Notification sent to Customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Sample Request Received Notification sent to Customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Sample Request Shipped Notification sent to Customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Sample Request Submitted to Sample Resource Location</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Technical Services contacted</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Transload Operations have been notified of stockout</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Transportation notified of freight rate acceptance</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Transportation notified of freight rate request received</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 1</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 1 - CAM</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 10</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 11</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 12</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 13</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 14</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 15</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 16</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 17</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 18</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 19</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 2</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 20</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 3</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 4</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 5</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 6</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 7</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 8</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>failed at step 9</fullName>
        <assignedTo>ferdinand@ussilica.com.qa</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
</Workflow>
