<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval Notification of ISP Price Quote</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>Quote_Templates/ISP_Price_Quote_Approval</template>
    </alerts>
    <alerts>
        <fullName>Customer Master Notified of New Account</fullName>
        <ccEmails>customermaster@ussilica.com</ccEmails>
        <protected>false</protected>
        <template>Quote_Templates/ISP_Price_Quote_Approval</template>
    </alerts>
    <alerts>
        <fullName>Email to pricing%40ussilica%2Ecom with accepted quote information</fullName>
        <ccEmails>pricing@ussilica.com</ccEmails>
        <ccEmails>furst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Quote_Templates/ISP_Price_Quote_Approval</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate accepted by customer</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>klinck@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/Freight_Rate_New_Rate_Request_Accepted</template>
    </alerts>
    <alerts>
        <fullName>Freight Rate accepted by customer - not rail</fullName>
        <ccEmails>furst@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennings@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>klinck@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stadnick@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/Freight_Rate_New_Rate_Request_Accepted</template>
    </alerts>
    <alerts>
        <fullName>ISP Quote Accepted - Delivered Freight</fullName>
        <ccEmails>pricing@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Price_Quote_Approval_Delivered</template>
    </alerts>
    <alerts>
        <fullName>ISP Quote Accepted - Prepaid Freight</fullName>
        <ccEmails>pricing@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/ISP_Price_Quote_Approval_Payable</template>
    </alerts>
    <alerts>
        <fullName>Notification for expiring quote</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <template>unfiled$public/Notification_for_expiring_quote</template>
    </alerts>
    <alerts>
        <fullName>Oil %26 Gas SPR Rejected</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>RSM_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/SPR_Rejected_notification</template>
    </alerts>
    <alerts>
        <fullName>Oil %26 Gas SPR approved and ready for pricing</fullName>
        <ccEmails>pricing@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>RSM_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <template>Case_Workflow_Templates/O_G_SPR_has_been_approved</template>
    </alerts>
    <alerts>
        <fullName>Oil %26 Gas SPR approved by Bryan Shinn</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/O_G_Approver_5_Approved_SPR</template>
    </alerts>
    <alerts>
        <fullName>Oil %26 Gas SPR approved by Don Merril</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/O_G_Approver_4_Approved_SPR</template>
    </alerts>
    <alerts>
        <fullName>Oil %26 Gas SPR approved by Don Weinheimer</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/O_G_Approver_3_Approved_SPR</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change Quote Expiration Date to Year End</fullName>
        <field>ExpirationDate</field>
        <formula>DATE(2015,12,31)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Status to In Approval Process</fullName>
        <field>Quote_Accepted_or_Declined__c</field>
        <literalValue>Pending</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change business secured to yes</fullName>
        <field>Business_Secured__c</field>
        <literalValue>Yes</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change status to extension request</fullName>
        <field>Status</field>
        <literalValue>Extension Request</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Freight Expiration Date set to Year End</fullName>
        <field>Freight_Effective_To__c</field>
        <formula>DATE(2015,12,31)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Quote Status field to Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Add Railcar charges to quote template</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Price Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Car_Charge_Quote_Options__c</field>
            <operation>equals</operation>
            <value>Separate out onto quote</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Expiration Date to End of Year</fullName>
        <actions>
            <name>Change Quote Expiration Date to Year End</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>US Silica ISP Sales Opportunity,ISP Sales Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Default Freight Expiration Date to End of Year</fullName>
        <actions>
            <name>Freight Expiration Date set to Year End</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>US Silica ISP Sales Opportunity,ISP Sales Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Pricing Notified on Quote Acceptance</fullName>
        <actions>
            <name>Email to pricing%40ussilica%2Ecom with accepted quote information</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Price Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP - Quote Accepted - Business Secured Yes</fullName>
        <actions>
            <name>Change business secured to yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Price Quote</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISP Quote - Notification to Credit%2FCustomer Master for new account setup</fullName>
        <actions>
            <name>Customer Master Notified of New Account</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Credit%2FCustomer Master Notified</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.New_Customer_Set_Up_Needed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Price Quote</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If extension is checked%2C change status to extension request</fullName>
        <actions>
            <name>Change status to extension request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>ISP Price Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Is_this_an_extension__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification for expiring quote</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Quote.ExpirationDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>O%26G SPR - Send to pricing</fullName>
        <actions>
            <name>Quote information sent to pricing team</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Send_to_Pricing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QUOTE - Pallet Charge Added if Yes Selected</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Quote.Pallet_Chargeforcolumbia__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Credit%2FCustomer Master Notified</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Quote information sent to pricing team</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
</Workflow>
