<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email Customer Master of New Contact</fullName>
        <ccEmails>customermaster@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Sys_Admin_Templates/SUPPORTNewcontactnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Email System Admin of New Contact</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Sys_Admin_Templates/SUPPORTNewcontactnotificationSAMPLE</template>
    </alerts>
    <rules>
        <fullName>Notify Customer Master of Updated Contacts</fullName>
        <actions>
            <name>Email Customer Master of New Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 or 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AccountName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AccountName</field>
            <operation>notEqual</operation>
            <value>US SILICA COMPANY PARENT</value>
        </criteriaItems>
        <description>Anytime a new contact is updated, customer master is notified</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify System Admin of New Contacts</fullName>
        <actions>
            <name>Email System Admin of New Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Anytime a new contact is created email System Admin.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
