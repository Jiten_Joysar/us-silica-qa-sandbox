<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email Carrie with bad survey results</fullName>
        <ccEmails>ferdinand@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>jenks@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>Case_Workflow_Templates/Bad_Survey_Results_Received</template>
    </alerts>
    <rules>
        <fullName>Survey Results contain 1 or 2</fullName>
        <actions>
            <name>Email Carrie with bad survey results</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2 or 3</booleanFilter>
        <criteriaItems>
            <field>Survey__c.Satisfaction__c</field>
            <operation>lessThan</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.LikelytoRecommend__c</field>
            <operation>lessThan</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey__c.OverallRating__c</field>
            <operation>lessThan</operation>
            <value>3</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
