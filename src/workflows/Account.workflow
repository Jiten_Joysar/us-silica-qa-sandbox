<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email Error</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>bfeather@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/Billto_Error</template>
    </alerts>
    <alerts>
        <fullName>New Transload Account Notification to Customer Service</fullName>
        <ccEmails>fracsand@ussilica.com</ccEmails>
        <ccEmails>flaherty@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>barbosa@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>buehl@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>carusona@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>chandler@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christiepayne@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>copley@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>daigle@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>harveyt@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jenks@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krubright@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>randall.larr@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>scotte@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sheldon@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smithmw@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thorp@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thumm@ussilica.com1.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/New_Transload_Account_for_Customer_Service</template>
    </alerts>
    <alerts>
        <fullName>New Transload Account Notification to O%26G Finance</fullName>
        <ccEmails>toure@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>barbosa@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krubright@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/New_Transload_Account_for_O_G_Finance</template>
    </alerts>
    <alerts>
        <fullName>New Transload Account Notification to Purchasing</fullName>
        <ccEmails>flauta@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krubright@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/New_Transload_Account_for_Purchasing</template>
    </alerts>
    <alerts>
        <fullName>New Transload Account Notification to Supply Chain</fullName>
        <ccEmails>ussilica-supplychain@ussilica.com</ccEmails>
        <ccEmails>banks@ussilica.com</ccEmails>
        <ccEmails>oumar.toure@ussilica.com</ccEmails>
        <ccEmails>barbosa@ussilica.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krubright@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/New_Transload_Account_for_Supply_Chain</template>
    </alerts>
    <alerts>
        <fullName>New Transload Account Notification to Transportation</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ferdinand@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennings@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>klinck@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krubright@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stadnick@ussilica.com.qa</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/New_Transload_Account_for_Transportation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign Value to LOB</fullName>
        <field>Line_of_Business__c</field>
        <literalValue>Industry and Specialty Products (ISP)</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flip to Active</fullName>
        <field>Status__c</field>
        <literalValue>Active</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Prospect Status</fullName>
        <field>Status__c</field>
        <literalValue>Prospect</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Active Accounts</fullName>
        <actions>
            <name>Flip to Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>BillTo,Parent,ShipTo (Active)</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JDE Billto Error</fullName>
        <actions>
            <name>Email Error</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This should email the admin team when a record is entered as a billto that does not have a JDE ID</description>
        <formula>AND(RecordTypeId  = &apos;01230000000t5J1&apos;,  ISBLANK(JDE_Address_Book_Number__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Transload Account Notification</fullName>
        <actions>
            <name>New Transload Account Notification to Customer Service</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Transload Account Notification to O%26G Finance</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Transload Account Notification to Purchasing</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Transload Account Notification to Supply Chain</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New Transload Account Notification to Transportation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Corporate Communications Notified of new transload account</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Customer Service notified of new transload account</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Finance notified of new transload account</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Supply Chain notified of new transload account</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Transportation notified of new transload account</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Transload Operations Vendor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Send_new_account_notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prospects Accounts</fullName>
        <actions>
            <name>Update Prospect Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>ShipTo</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Value for LOB</fullName>
        <actions>
            <name>Assign Value to LOB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Line_of_Business__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Corporate Communications Notified of new transload account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Customer Service notified of new transload account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Finance notified of new transload account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Supply Chain notified of new transload account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
    <tasks>
        <fullName>Transportation notified of new transload account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
</Workflow>
