<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Advance status to pending JDE setup</fullName>
        <description>Advance status to pending JDE setup when business secured</description>
        <field>Status</field>
        <literalValue>Pending JDE Setup</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Contact to Me</fullName>
        <field>ToAddress</field>
        <formula>&quot;Erica Ferdinand&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Owner to CS Queue Evenings</fullName>
        <field>OwnerId</field>
        <lookupValue>Oil &amp; Gas Customer Service</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Owner to Customer Service Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Oil &amp; Gas Customer Service</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email TO equals blank</fullName>
        <field>ToAddress</field>
        <formula>&quot;Erica Ferdinand&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set New Activity on Case to True</fullName>
        <field>New_Activity__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Case Status to In Progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Status to Waiting on Customer</fullName>
        <field>Status</field>
        <literalValue>Waiting on Customer</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Advance Case Status on Email Reply</fullName>
        <actions>
            <name>Update Case Status to In Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 or 5) AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pricing Request,General Case Layout,Transload BOLs,Oil &amp; Gas Sample Request,ISP Sample Requests,ISP Communications,ISP Certificate of Analysis,SOA Notifications,Annual Price Increase Letter,Internal Communications,Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.TextBody</field>
            <operation>contains</operation>
            <value>:ref</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quality Issue,Customer Report Request,Order Issue,Inquiry,Delays,Transload Stockout,Transload Information,ISP International Customer Service,Customer Master Request,ISP Customer Order,ISP Customer Issue</value>
        </criteriaItems>
        <description>Advance a Case Status from New to In Progress when a CAM email replies a case within Salesforce.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email TO equals blank</fullName>
        <active>false</active>
        <formula>AND (
CONTAINS(ToAddress,&apos;@&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Freight Rate - Advance Case Status on Business Secured</fullName>
        <actions>
            <name>Advance status to pending JDE setup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Freight Rate Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Business_Secured1__c</field>
            <operation>contains</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Initial Rate Request Completed</value>
        </criteriaItems>
        <description>Advance a Case Status to Pending JDE Setup when an associated quote has secured business</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Night Time Reply Reassign Case</fullName>
        <actions>
            <name>Change Owner to Customer Service Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
NOT( Parent.IsClosed ), 
NOT(AND(VALUE( MID(TEXT( MessageDate ), 12,2))&gt;=10, 
VALUE( MID(TEXT( MessageDate ), 12,2))&lt;=22)), 
Parent.Owner:Queue.Id &lt;&gt; &quot;00G19000000MXE8&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set New Activity on Case for Email</fullName>
        <actions>
            <name>Set New Activity on Case to True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Status on Email Reply from Customer</fullName>
        <actions>
            <name>Update Case Status to In Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 or 5 or 6) and 2 and 3 and 4</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Other Customer Information,Pricing Request,General Case Layout,Transload BOLs,Oil &amp; Gas Sample Request,ISP Sample Requests,Invoices,ISP Communications,ISP Certificate of Analysis,Annual Price Increase Letter,Internal Communications</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>In Progress,New</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Override_Validations__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quality Issue,Customer Report Request,Order Issue,Inquiry,Delays,Transload Stockout,Transload Information,Credit / Rebill,ISP International Customer Service,Customer Master Request,ISP Customer Order,ISP Customer Issue</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.TextBody</field>
            <operation>notEqual</operation>
            <value>Thank you</value>
        </criteriaItems>
        <description>Update a Case Status to In progress when a customer replies to an email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
