/*
    Developed on : 22-March-2013
    Developed By : Brandon Feather
    Description  : This trigger looks for a closed won opportunity, if the account is not Activ this will 
    mark the account avtive, designmating it to be pickup by JDE
 */

trigger OpportunityClosedWon on Opportunity (after insert, after update) {
ID ShiptoActive;
Set<id> Accountids =  new Set<id>();

For(Opportunity Opp: trigger.new){
    if(Opp.IsWon == true){
        Accountids.add(Opp.Accountid);
    }
}

List<Account> Accountupdates = new List<Account>();
Accountupdates = [Select id, recordtypeid from Account where id in:Accountids and status__c != 'Active'];

If(Accountupdates.size()>0){
    ShiptoActive = Schema.SObjectType.account.getRecordTypeInfosByName().get('ShipTo (Active)').getRecordTypeId();
    For(Account a:Accountupdates){
        a.recordtypeid = ShiptoActive;
    }
    
    Update Accountupdates;
    
}


}