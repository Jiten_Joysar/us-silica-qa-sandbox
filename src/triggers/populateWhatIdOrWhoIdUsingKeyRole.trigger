trigger populateWhatIdOrWhoIdUsingKeyRole on Task (before insert,before update) {
        
    String keyRolePrefixId;
    String contactPrefixId;
    String taskRelatedToId;
    String taskContactId;
    Set<Id> keyRoleIdSet = new Set<Id>();
    Set<Id> contactIdSet = new Set<Id>();
    Map<Id,Id> keyRoleIdAndContactIdMap = new Map<Id,Id>();
    Map<Id,Id> contactIdAccountIdMap = new Map<Id,Id>();
    Map<Id,Id> contactIdAndKeyRoleIdMap = new Map<Id,Id>();
    
    Schema.DescribeSObjectResult kr = Key_Role__c.SObjectType.getDescribe();
    keyRolePrefixId = kr.getKeyPrefix();
    
    Schema.DescribeSObjectResult cont = Contact.SObjectType.getDescribe();
    contactPrefixId = cont.getKeyPrefix();
       
    for( Task taskRec : trigger.new ) {
    
        if( taskRec.WhatId != null )
            taskRelatedToId = taskRec.WhatId;
            
        if( taskRec.WhoId != null )
            taskContactId = taskRec.WhoId;
            
        if( ( taskRelatedToId != null && taskRelatedToId.startsWith(keyRolePrefixId) ) || ( taskContactId != null && taskContactId.startsWith(contactPrefixId) ) ) {
         
            if( Trigger.isInsert ) {
                 
                if( taskRec.WhatId != null ) {
                    keyRoleIdSet.add(taskRec.WhatId);
                } else if( taskRec.WhoId != null ) {
                    contactIdSet.add(taskRec.WhoId);
                }
            
            }else if( Trigger.isUpdate && ( taskRec.WhatId != null && taskRec.WhatId != Trigger.oldMap.get(taskRec.id).WhatId ) || ( taskRec.WhoId != null && taskRec.WhoId != Trigger.oldMap.get(taskRec.id).WhoId ) ) {
                
                if( taskRec.WhatId != Trigger.oldMap.get(taskRec.id).WhatId ) {
                    keyRoleIdSet.add(taskRec.WhatId);
                } else if( taskRec.WhoId != Trigger.oldMap.get(taskRec.id).WhoId ) {
                    contactIdSet.add(taskRec.WhoId);
                }
                
            }
        
        }
    }

    if( keyRoleIdSet.size() > 0 ) {
    
        for( Key_Role__c keyRoleRec : [ SELECT Account__c,Contact__c,id 
                                        FROM   Key_Role__c 
                                        WHERE  Id IN : keyRoleIdSet 
                                      ] ) {
        
                if( !keyRoleIdAndContactIdMap.containsKey(keyRoleRec.Id) ) {
                    keyRoleIdAndContactIdMap.put(keyRoleRec.Id,keyRoleRec.Contact__c);
                }  
                
        }
        
    }
    
    if( contactIdSet.size() > 0 ) {       
        
        for( Contact con : [SELECT AccountId,Name FROM Contact WHERE Id IN : contactIdSet ]) {            
            if( !contactIdAccountIdMap.containsKey(con.Id) ) {
                contactIdAccountIdMap.put(con.id,con.AccountId);
            }
        }

    }
        
    if( contactIdAccountIdMap.size() > 0 ) {
    
        for( Key_Role__c keyRoleRec : [ SELECT Account__c,Contact__c 
                                        FROM   Key_Role__c 
                                        WHERE  Contact__c IN : contactIdAccountIdMap.keySet() 
                                      ] ) {
                
                if( keyRoleRec.Account__c == contactIdAccountIdMap.get(keyRoleRec.Contact__c) ) {                    
                    
                    if( !contactIdAndKeyRoleIdMap.containsKey(keyRoleRec.Contact__c) ) {
                        contactIdAndKeyRoleIdMap.put(keyRoleRec.Contact__c,keyRoleRec.id);
                    }    
                    
                }            

        }
        
    }
    
    for( Task tsk : trigger.new ) {
    
        if( tsk.WhatId != null )
            taskRelatedToId = tsk.WhatId;
        if( tsk.WhoId != null )
            taskContactId = tsk.WhoId;
        
        if( ( taskRelatedToId != null && taskRelatedToId.startsWith(keyRolePrefixId) ) || ( taskContactId != null && taskContactId.startsWith(contactPrefixId) ) ) {
        
            if( Trigger.isInsert ) { 
                
                if( tsk.WhatId != null ) {
                
                    if( keyRoleIdAndContactIdMap.containsKey(tsk.WhatId) && keyRoleIdAndContactIdMap.get(tsk.WhatId) != null )                    
                        tsk.WhoId = keyRoleIdAndContactIdMap.get(tsk.WhatId);
                        
                }else if( tsk.WhoId != null ) {
                
                    if( contactIdAndKeyRoleIdMap.containsKey(tsk.WhoId) && contactIdAndKeyRoleIdMap.get(tsk.WhoId) != null )
                        tsk.WhatId = contactIdAndKeyRoleIdMap.get(tsk.WhoId);
                    
                }
                
            }else if( Trigger.isUpdate && ( ( tsk.WhatId != null && tsk.WhatId != Trigger.oldMap.get(tsk.id).WhatId ) || ( tsk.WhoId != null && tsk.WhoId != Trigger.oldMap.get(tsk.id).WhoId ) ) ) {
            
                if( tsk.WhatId != Trigger.oldMap.get(tsk.id).WhatId ) {
                
                    if( keyRoleIdAndContactIdMap.containsKey(tsk.WhatId) && keyRoleIdAndContactIdMap.get(tsk.WhatId) != null )                    
                        tsk.WhoId = keyRoleIdAndContactIdMap.get(tsk.WhatId);
                
                } else if( tsk.WhoId != Trigger.oldMap.get(tsk.id).WhoId ) {
                
                    if( contactIdAndKeyRoleIdMap.containsKey(tsk.WhoId) && contactIdAndKeyRoleIdMap.get(tsk.WhoId) != null )
                        tsk.WhatId = contactIdAndKeyRoleIdMap.get(tsk.WhoId);
                
                }
            
            }
            
        }
        
    }
            
}