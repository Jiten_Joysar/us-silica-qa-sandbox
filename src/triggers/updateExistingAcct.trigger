trigger updateExistingAcct on Territory_Owner_Mapping__c ( after update ) {
    
    Set<Id> tomIdSet = new Set<Id>();
    Map<Id, Territory_Owner_Mapping__c> tomIdAndTomMap = new Map<Id, Territory_Owner_Mapping__c>();
    
    for ( Territory_Owner_Mapping__c tom : Trigger.New ) {
    
        if ( tom.Inside_Sales_Rep__c != Trigger.oldMap.get(tom.Id).Inside_Sales_Rep__c || 
                tom.Regional_Sales_Manager__c != Trigger.oldMap.get(tom.Id).Regional_Sales_Manager__c ||
                tom.Threshold__c != Trigger.oldMap.get(tom.Id).Threshold__c ) {
                
            tomIdSet.add(tom.Id); 
            tomIdAndTomMap.put(tom.Id, tom);   
        }
    }
    
    List<Account> accountList = [ SELECT Id, OwnerId, AnnualRevenue, Territory_Owner_Mapping__c FROM Account 
                                  WHERE Territory_Owner_Mapping__c IN: tomIdSet
                                      AND  Account_Ownership_Override__c = FALSE ];
                                  
    for (Account acc: accountList) {        
        
        
        Decimal revenue = 0;
        if (acc.AnnualRevenue != null) revenue  = acc.AnnualRevenue;
        
        Decimal TOMT = 0;
        if (tomIdAndTomMap.get(acc.Territory_Owner_Mapping__c).Threshold__c != null) TOMT = tomIdAndTomMap.get(acc.Territory_Owner_Mapping__c).Threshold__c;
       
        if (revenue >= TOMT ) {
            
            if (tomIdAndTomMap.get(acc.Territory_Owner_Mapping__c).Regional_Sales_Manager__c!= null)
                acc.ownerId = tomIdAndTomMap.get(acc.Territory_Owner_Mapping__c).Regional_Sales_Manager__c;
        } else {
            if (tomIdAndTomMap.get(acc.Territory_Owner_Mapping__c).Inside_Sales_Rep__c != null)
                acc.ownerId = tomIdAndTomMap.get(acc.Territory_Owner_Mapping__c).Inside_Sales_Rep__c ;
        }
    
    }
    
    if (accountList.size() > 0)  update accountList;
   
    
}