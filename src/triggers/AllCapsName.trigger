/*
    Developed on : 22-March-2013
    Developed By : Brandon Feather
    Description  : This trigger formats the account name to be all caps, this is necessary because JDE is case sensative, and if written in all caps
    The trigger also syncs the owner and a custom owner field to make any related fields avaiable in formulas
    
    Addition: 3-May 2013 add the address fields to the caps format
 */

trigger AllCapsName on Account (before insert, before update) {

For(Account A: trigger.new){
    A.Owner_Clone__c =  a.ownerid;
    
    if(A.name != null){
    A.name = a.name.toUpperCase();
    }
    
    if(A.Street_Address_1__c != null){
    A.Street_Address_1__c = a.Street_Address_1__c.toUpperCase();
    }
    
    if(A.Street_Address_2__c != null){
    A.Street_Address_2__c = a.Street_Address_2__c.toUpperCase();   	
    }

    if(A.City__c != null){
    A.City__c = A.City__c.toUpperCase();   	
    }
    
    if(A.State__c != null){
    A.State__c = A.State__c.toUpperCase();   	
    }
    
    if(A.Country__c != null){
    A.Country__c = A.Country__c.toUpperCase();   	
    }
    
}


}