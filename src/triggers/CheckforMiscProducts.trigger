/*
    Developed on : 22-March-2013
    Developed By : Brandon Feather
    Description  : Part of the JDE integration if the product is not found, on the salesorder line item, the product is defaulted to misc/freight.
 */

trigger CheckforMiscProducts on Sales_Order_Line_Item__c (before insert, before update) {
ID Prodid;
List<Product2> MiscProd = new List<Product2>();
MiscProd = [Select id from Product2 where X2nd_Item_Number__c = 'MISC0001'];

if(MiscProd.size()>0){
    Prodid = MiscProd[0].id;
} else {
    Product2 NewProd = new Product2 (name = 'Miscellaneous', X2nd_Item_Number__c = 'MISC0001');
    insert NewProd;
    Prodid = NewProd.id;    
}

for(Sales_Order_Line_Item__c SOLI: trigger.new){
    if(SOLI.Product__c == null){
        SOLI.Product__c = Prodid;
    }
}


}