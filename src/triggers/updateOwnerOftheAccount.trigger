trigger updateOwnerOftheAccount on Territory_Mapping__c (after insert, after update) {
    
    Set<Id> ISPterritoryMappingIdSet = new Set<Id>();
    Set<Id> OandGterritoryMappingIdSet = new Set<Id>();
    Map<Id, Id> territoryIdAndTOMIdMap = new  Map<Id, Id>();
    Set<Id> territoryIdSet = new Set<Id>();
    Set<Id> tomIdSet = new Set<Id>();
    Map<Id, Territory_Owner_Mapping__c> tomIdAndTomMap = new  Map<Id, Territory_Owner_Mapping__c>();
    Map<String, Territory_Mapping__c> countryStateAndTerritoryMap = new Map<String, Territory_Mapping__c>();
    
    
    if (Trigger.isUpdate) { 
       
        for (Territory_Mapping__c territory : Trigger.new) {
        
            if (Trigger.newMap.get(territory.Id).ISP_Territory_Name__c != Trigger.oldMap.get(territory.Id).ISP_Territory_Name__c) {
            
                ISPterritoryMappingIdSet.add(territory.Id);        
             
                tomIdSet.add(territory.ISP_Territory_Name__c);
                territoryIdAndTOMIdMap.put(territory.Id, territory.ISP_Territory_Name__c);    
            }    
            
            if ( Trigger.newMap.get(territory.Id).O_G_Territory__c != Trigger.oldMap.get(territory.Id).O_G_Territory__c) {
                  OandGterritoryMappingIdSet.add(territory.Id);  
                
                  tomIdSet.add(territory.O_G_Territory__c ); 
                  territoryIdAndTOMIdMap.put(territory.Id, territory.O_G_Territory__c ); 
            }
        }
    }
    
    
    if (Trigger.isInsert) { 
       
        for (Territory_Mapping__c territory : Trigger.new) {
        
            if (territory.Country__c ==  'US'|| territory.Country__c == 'USA' || territory.Country__c == 'United States') {
            
                if (territory.State__c != null && territory.State__c != '') {
                
                    countryStateAndTerritoryMap.put(territory.CountryState__c, territory); 
                    tomIdSet.add(territory.ISP_Territory_Name__c);
                    tomIdSet.add(territory.O_G_Territory__c );              
                }
            }  else if ( territory.Country__c != null && territory.Country__c != '') {
            
                countryStateAndTerritoryMap.put(territory.CountryState__c, territory);
                tomIdSet.add(territory.ISP_Territory_Name__c);
                tomIdSet.add(territory.O_G_Territory__c );              
            }                    
        }       
    } 
    
     System.debug(':::::countryStateAndTerritoryMap::'+countryStateAndTerritoryMap);
    if (tomIdSet.size() > 0) {
        
        List<Account> accountList = new List<Account>();
        List<Territory_Owner_Mapping__c> tomList = [ SELECT Id, Inside_Sales_Rep__c, Regional_Sales_Manager__c , Threshold__c 
            FROM Territory_Owner_Mapping__c WHERE Id IN: tomIdSet ];
        
        for (Territory_Owner_Mapping__c tom : tomList ) {
            tomIdAndTomMap.put(tom.Id, tom);
        }
        
        if (ISPterritoryMappingIdSet.size() > 0 || OandGterritoryMappingIdSet.size() > 0) {
            
            accountList = [ SELECT Id, AnnualRevenue, Territory_Owner_Mapping__c,
                Line_of_Business__c, OwnerId, Account_Ownership_Override__c, Territory_Mapping__c 
                FROM Account 
                WHERE Account_Ownership_Override__c = FALSE  AND ( 
                 
                    ( Territory_Mapping__c IN: ISPterritoryMappingIdSet AND Line_of_Business__c = 'Industry and Specialty Products (ISP)') 
                        OR 
                   ( Territory_Mapping__c IN: OandGterritoryMappingIdSet AND Line_of_Business__c = 'Oil And Gas') 
                )
            ];
            
            for (Account acc: AccountList) {
            
                Decimal revenue = acc.AnnualRevenue;
                if (acc.AnnualRevenue == null) revenue  = 0;
                
                if ( tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)) != null) {
                    
                    Decimal threshold = tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)).Threshold__c;
                    
                    if (tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)).Threshold__c == null) threshold = 0;
                    
                    if (revenue >= threshold  ) {
                    
                        if (tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)).Regional_Sales_Manager__c != null )
                            acc.ownerId =  tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)).Regional_Sales_Manager__c;
                    } else {
                        if (tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)).Inside_Sales_Rep__c != null)
                            acc.ownerId =  tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)).Inside_Sales_Rep__c;
                    }
                    acc.Territory_Owner_Mapping__c  = tomIdAndTomMap.get(territoryIdAndTOMIdMap.get(acc.Territory_Mapping__c)).Id;
                }
            }
        } else if (countryStateAndTerritoryMap.size() > 0) {
        
            accountList = [ SELECT Id, AnnualRevenue, Territory_Owner_Mapping__c, CountryState__c,
                Line_of_Business__c, OwnerId, Account_Ownership_Override__c, Territory_Mapping__c 
                FROM Account 
                WHERE Account_Ownership_Override__c = FALSE 
                    AND CountryState__c IN: countryStateAndTerritoryMap.keySet() AND 
                    CountryState__c != NULL
                
            ];
             System.debug(':::::AccountList::'+AccountList);
            for (Account acc: accountList) {
            
                Decimal revenue = acc.AnnualRevenue;
                if (acc.AnnualRevenue == null) revenue  = 0;
                
                if (countryStateAndTerritoryMap.get(acc.CountryState__c) != null) {
                    
                    acc.Territory_Mapping__c = countryStateAndTerritoryMap.get(acc.CountryState__c).Id;
                    if (acc.Line_of_Business__c == 'Industry and Specialty Products (ISP)') {
                        
                        if (countryStateAndTerritoryMap.get(acc.CountryState__c).ISP_Territory_Name__c != null) {
                            
                            String ispTerritory = countryStateAndTerritoryMap.get(acc.CountryState__c).ISP_Territory_Name__c;
                            Decimal threshold = tomIdAndTomMap.get(ispTerritory).Threshold__c;
                    
                            if (tomIdAndTomMap.get(ispTerritory).Threshold__c == null) threshold = 0;
                            
                            if (revenue >= threshold  ) {
                            
                                if (tomIdAndTomMap.get(ispTerritory).Regional_Sales_Manager__c != null )
                                    acc.ownerId =  tomIdAndTomMap.get(ispTerritory).Regional_Sales_Manager__c;
                            } else {
                                if (tomIdAndTomMap.get(ispTerritory).Inside_Sales_Rep__c != null)
                                    acc.ownerId =  tomIdAndTomMap.get(ispTerritory).Inside_Sales_Rep__c;
                            }
                            acc.Territory_Owner_Mapping__c  = ispTerritory;
                        }
                        
                    } else if (acc.Line_of_Business__c == 'Oil And Gas') {
                    
                        if (countryStateAndTerritoryMap.get(acc.CountryState__c).O_G_Territory__c != null) {
                            
                            String OandG = countryStateAndTerritoryMap.get(acc.CountryState__c).O_G_Territory__c;
                            Decimal threshold = tomIdAndTomMap.get(OandG).Threshold__c;
                    
                            if (tomIdAndTomMap.get(OandG).Threshold__c == null) threshold = 0;
                            
                            if (revenue >= threshold  ) {
                            
                                if (tomIdAndTomMap.get(OandG).Regional_Sales_Manager__c != null )
                                    acc.ownerId =  tomIdAndTomMap.get(OandG).Regional_Sales_Manager__c;
                            } else {
                                if (tomIdAndTomMap.get(OandG).Inside_Sales_Rep__c != null)
                                    acc.ownerId =  tomIdAndTomMap.get(OandG).Inside_Sales_Rep__c;
                            }
                            acc.Territory_Owner_Mapping__c  = OandG;
                        }
                    }
                }
            }
        }
        
        
        System.debug(':::::AccountList::'+AccountList);
        if (AccountList.size() > 0) update AccountList;
    }
    
}