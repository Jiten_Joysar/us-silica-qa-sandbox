/*
    Developed on : 07-March-2012
    Developed By : Alex
    Description  : Assign the Account Owner based on Territory.
 */
trigger ownerAssignmentBasedTerritory on Account ( before insert, before update ) {

    String billToRT = '';
    String shipToRT = '';
    Map<String, List<Account>> ISPCountryStateAcctMap   = new Map<String, List<Account>>();
    
    Map<String, Territory_Mapping__c> territoryOwnerMap = new Map<String, Territory_Mapping__c>();
    
    for( RecordType RT : [ SELECT Id, Name FROM RecordType WHERE sObjectType = 'Account' AND ( Name = 'BillTo' OR Name = 'ShipTo' ) ] ){
    
        if( RT.Name == 'BillTo' ){
        
            billToRT = RT.Id;
        }
        if( RT.Name == 'ShipTo' ){
        
            shipToRT = RT.Id;
        }
    }
    
    for( Account acct : Trigger.New ) {
        
        if( acct.Line_of_Business__c != NULL ){
        
            //Do not consider National Accounts and Ownership override Account records
            if( !( acct.National_Account__c || acct.Account_Ownership_Override__c ) && ( acct.RecordTypeId == billToRT || acct.RecordTypeId == shipToRT )) {
            
                if( Trigger.isUpdate ) {
                    
                    System.debug(LoggingLevel.INFO,':::Inside Update Level I :::');
                    Account oldAcct = Trigger.oldMap.get(acct.Id);
                    
                    System.debug(LoggingLevel.INFO,' acct.RecordType.Name ::: ' + acct.RecordType.Name );
                    System.debug(LoggingLevel.INFO,'acct.Billing_State_Province__c ::: ' + acct.Billing_State_Province__c );
                    System.debug(LoggingLevel.INFO,'oldAcct.Billing_State_Province__c ::: ' + oldAcct.Billing_State_Province__c );
                    
                    if( acct.AnnualRevenue !=  oldAcct.AnnualRevenue || acct.Line_of_Business__c != oldAcct.Line_of_Business__c || acct.National_Account__c != oldAcct.National_Account__c || acct.Account_Ownership_Override__c != oldAcct.Account_Ownership_Override__c || acct.RecordTypeId != oldAcct.RecordTypeId || ( acct.RecordTypeId == billToRT && ( acct.Billing_State_Province__c != oldAcct.Billing_State_Province__c || acct.Billing_Country__c != oldAcct.Billing_Country__c ) ) || ( acct.RecordTypeId == shipToRT && ( acct.Shipping_State_Province__c != oldAcct.Shipping_State_Province__c || acct.Shipping_Country__c != oldAcct.Shipping_Country__c ) ) ){
                    
                        System.debug(LoggingLevel.INFO,':::Inside Update Level II :::');
                        System.debug(LoggingLevel.INFO,':::acct.RecordTypeId ::: ' + acct.RecordTypeId );
                        System.debug(LoggingLevel.INFO,':::oldAcct.RecordTypeId ::: ' + oldAcct.RecordTypeId );
                        
                        String Country='';
                        String State='';
                        
                        if( acct.RecordTypeId == billToRT ){
                            
                            Country = acct.Billing_Country__c!= NULL ? acct.Billing_Country__c : '';
                            State   = acct.Billing_State_Province__c != NULL ? acct.Billing_State_Province__c : '';
                        }
                        if( acct.RecordTypeId == shipToRT ){
                            
                            Country = acct.Shipping_Country__c != NULL ? acct.Shipping_Country__c : '';
                            State   = acct.Shipping_State_Province__c != NULL ? acct.Shipping_State_Province__c : '';
                        }
                        
                        if( acct.Line_of_Business__c == 'Industry and Specialty Products (ISP)' || acct.Line_of_Business__c == 'Oil and Gas' ){
                             
                            String KeyVal = Country == 'US'||Country == 'USA'||Country == 'United States' ? 'US'+State.toUpperCase() : Country.toUpperCase();
                            
                            If( !ISPCountryStateAcctMap.containsKey( KeyVal ) )
                                    ISPCountryStateAcctMap.put( KeyVal, new List<Account>() );
                                        
                            ISPCountryStateAcctMap.get( KeyVal ).add(acct);
                        }//End of LOB check
                        
                        if( acct.Line_of_Business__c.toUpperCase() == 'PARENT' ) {
                        
                            String KeyVal = 'PARENT';
                            
                            If( !ISPCountryStateAcctMap.containsKey( KeyVal ) )
                                    ISPCountryStateAcctMap.put( KeyVal, new List<Account>() );
                                        
                            ISPCountryStateAcctMap.get( KeyVal ).add(acct);
                        } 
                    }
                }//End of isUpdate
                
                System.debug(LoggingLevel.INFO,'acct.Billing_State_Province__c ::: ' + acct.Billing_State_Province__c );
                System.debug(LoggingLevel.INFO,'acct.Billing_Country__c ::: ' + acct.Billing_Country__c );
                System.debug(LoggingLevel.INFO,'acct.Line_of_Business__c ::: ' + acct.Line_of_Business__c );
                
                if( Trigger.isInsert && ( ( acct.RecordTypeId == billToRT && ( acct.Billing_State_Province__c != NULL || acct.Billing_Country__c != NULL ) ) || ( acct.RecordTypeId == shipToRT && ( acct.Shipping_State_Province__c != NULL || acct.Shipping_Country__c != NULL ) ) )  ) {
                    
                    String Country;
                    String State;
                    
                    if( acct.RecordTypeId == billToRT ){
                        
                        Country = acct.Billing_Country__c != NULL ? acct.Billing_Country__c : '';
                        State   = acct.Billing_State_Province__c != NULL ? acct.Billing_State_Province__c : '';
                    }
                    if( acct.RecordTypeId == shipToRT ){
                        
                        Country = acct.Shipping_Country__c != NULL ? acct.Shipping_Country__c : '';
                        State   = acct.Shipping_State_Province__c != NULL ? acct.Shipping_State_Province__c : '';
                    }
                    
                    if( acct.Line_of_Business__c == 'Industry and Specialty Products (ISP)' || acct.Line_of_Business__c == 'Oil and Gas' ){
                         
                        String KeyVal = Country == 'US'||Country == 'USA'||Country == 'United States' ? 'US'+State.toUpperCase() : Country.toUpperCase();
                        
                        If( !ISPCountryStateAcctMap.containsKey( KeyVal ) )
                                ISPCountryStateAcctMap.put( KeyVal, new List<Account>() );
                                    
                        ISPCountryStateAcctMap.get( KeyVal ).add(acct);
                    }//End of LOB check                
                    
                    if( acct.Line_of_Business__c.toUpperCase() == 'PARENT' ) {
                        
                        String KeyVal = 'PARENT';
                        
                        If( !ISPCountryStateAcctMap.containsKey( KeyVal ) )
                                ISPCountryStateAcctMap.put( KeyVal, new List<Account>() );
                                    
                        ISPCountryStateAcctMap.get( KeyVal ).add(acct);
                    } 
                }//Endof isInsert
            }//End of National Accounts and Ownership override check
            
            if( acct.National_Account__c && ( Trigger.isInsert || ( Trigger.isUpdate &&(( acct.National_Account__c != Trigger.oldMap.get(acct.Id).National_Account__c ) || ( acct.Account_Ownership_Override__c != Trigger.oldMap.get(acct.Id).Account_Ownership_Override__c) ) ) )&& !acct.Account_Ownership_Override__c ) {
            
                String KeyVal = 'NATIONAL ACCOUNTS';
                        
                If( !ISPCountryStateAcctMap.containsKey( KeyVal ) )
                        ISPCountryStateAcctMap.put( KeyVal, new List<Account>() );
                            
                ISPCountryStateAcctMap.get( KeyVal ).add(acct);
            }//End of acct.National_Account__c
        }//End of acct.Line_of_Business__c  NULL check
    }//End of Trigger.New
    
    System.debug(LoggingLevel.INFO,'ISPCountryStateAcctMap :::' + ISPCountryStateAcctMap);
    //Query the Territory Mapping object
    for( Territory_Mapping__c TM : [ SELECT Id, CountryState__c, ISP_Territory_Name__r.Id, ISP_Territory_Name__r.Regional_Sales_Manager__c, ISP_Territory_Name__r.Inside_Sales_Rep__c, ISP_Territory_Name__r.Threshold__c, O_G_Territory__r.Id, O_G_Territory__r.Regional_Sales_Manager__c FROM Territory_Mapping__c WHERE CountryState__c IN :ISPCountryStateAcctMap.keySet() ] ){
    
        territoryOwnerMap.put( TM.CountryState__c, TM );
    }
    System.debug( LoggingLevel.INFO,'territoryOwnerMap ::: ' + territoryOwnerMap );
    //Assign Owner if Country (and State) is available in Territories Mapping 
    for( String CS : territoryOwnerMap.keySet() ){
    
        System.debug( LoggingLevel.INFO,'CS ::: ' + CS );
        System.debug(LoggingLevel.INFO,'ISPCountryStateAcctMap :::' + ISPCountryStateAcctMap);
        for( Account acc : ISPCountryStateAcctMap.get( CS ) ){
           
           if( acc.Line_of_Business__c == 'Industry and Specialty Products (ISP)' ){ 
           
                if( territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Threshold__c != NULL && ( acc.AnnualRevenue == NULL || (acc.AnnualRevenue != NULL && (territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Threshold__c > acc.AnnualRevenue &&  acc.AnnualRevenue >= 0 ))) ){
                
                    acc.OwnerId = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Inside_Sales_Rep__c;
                    acc.Territory_Mapping__c       = territoryOwnerMap.get( CS ).Id;
                    acc.Territory_Owner_Mapping__c = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Id;
                    
                }else {
                
                    acc.OwnerId = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Regional_Sales_Manager__c;
                    acc.Territory_Mapping__c       = territoryOwnerMap.get( CS ).Id;
                    acc.Territory_Owner_Mapping__c = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Id;
                }//Threshold NULL check
            }//End of ISP ceck
            
            if( acc.Line_of_Business__c == 'Oil and Gas' ){
            
                acc.OwnerId = territoryOwnerMap.get( CS ).O_G_Territory__r.Regional_Sales_Manager__c;
                    acc.Territory_Mapping__c       = territoryOwnerMap.get( CS ).Id;
                    acc.Territory_Owner_Mapping__c = territoryOwnerMap.get( CS ).O_G_Territory__r.Id;
            }
            
            if( acc.Line_of_Business__c == 'Parent' ){
            
                acc.OwnerId = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Regional_Sales_Manager__c;
                    acc.Territory_Mapping__c       = territoryOwnerMap.get( CS ).Id;
                    acc.Territory_Owner_Mapping__c = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Id;
            }
            
            if( acc.National_Account__c ){
            
                acc.OwnerId = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Regional_Sales_Manager__c;
                    acc.Territory_Mapping__c       = territoryOwnerMap.get( CS ).Id;
                    acc.Territory_Owner_Mapping__c = territoryOwnerMap.get( CS ).ISP_Territory_Name__r.Id;
            }
        }//End of ISPCountryStateAcctMap.get( CS ) 
    }//End of territoryOwnerMap.keySet()
}