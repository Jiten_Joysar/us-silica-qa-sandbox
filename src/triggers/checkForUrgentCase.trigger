trigger checkForUrgentCase on Case (before insert , before Update,after Update) {
	public checkForUrgentCaseTriggerHandler checkForUrgentCaseTriggerHandlerObj = new checkForUrgentCaseTriggerHandler(Trigger.new,Trigger.old,Trigger.newMap,Trigger.oldMap);
	if(Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
	    for(Case caseRec:Trigger.New)
	    {
	        if(caseRec.is_urgent__c)
	            caseRec.cam_email__C=caseRec.CAM_email_formula__c;
	    }
	}
    if(Trigger.isUpdate && Trigger.isAfter)
    {
    	checkForUrgentCaseTriggerHandlerObj.afterUpdateBusinessLogic();
    }
}