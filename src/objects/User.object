<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>DB_Region__c</fullName>
        <description>This is a field that is installed by and used with the Adoption Dashboard AppExchange package. If your org already has a similar field, you can change the reports that are part of the Adoption Dashboard package to use your custom field and then delete this field.</description>
        <label>DB Region</label>
        <picklist>
            <picklistValues>
                <fullName>NA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>EMEA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>APAC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LA</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Full_Name__c</fullName>
        <formula>FirstName &amp; &quot; &quot; &amp; LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Full Name</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>JDE_Rep_Code__c</fullName>
        <externalId>true</externalId>
        <label>JDE Rep Code</label>
        <length>5</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>NVMContactWorld__MostRecentCallEventTimestamp__c</fullName>
        <description>The timestamp of the most recent call event (deliver, transfer...) that this user has handled</description>
        <label>Most Recent Call Event Timestamp</label>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>NVMContactWorld__MostRecentCallIsActive__c</fullName>
        <defaultValue>false</defaultValue>
        <label>Most Recent Call Is Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>NVMContactWorld__MostRecentCall__c</fullName>
        <label>Most Recent Call</label>
        <length>128</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>NVMContactWorld__NVM_Agent_Id__c</fullName>
        <description>NVM Agent Id for Contact World for Salesforce</description>
        <externalId>true</externalId>
        <inlineHelpText>NVM Agent Id for Contact World for Salesforce. This should be set to the correct NVM Agent Id to make sure that call tasks are assigned to the correct Salesforce user.</inlineHelpText>
        <label>NVM Agent Id</label>
        <length>11</length>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>NVMContactWorld__NVM_Cell__c</fullName>
        <description>NewVoiceMedia click to dial Cell Phone</description>
        <formula>IF( 
        LEN( MobilePhone  )&gt;0 , 
        HYPERLINK( &quot;/apex/NVMContactWorld__NVM_AgentUI_ClickToDialInitiator?number=&quot; &amp; 
                   SUBSTITUTE(MobilePhone , &quot;+&quot;, &quot;%2B&quot;) &amp; 
                   &quot;&amp;oid=&quot; &amp; Id &amp; 
                   &quot;&amp;name=&quot; &amp; FirstName &amp; &quot;%20&quot; &amp;  LastName &amp; 
                   &quot;&amp;type=User&quot;, 
                   MobilePhone  , &quot;nvm-clicktodial&quot;) 
                   &amp; &quot; &quot; &amp; IMAGE( &quot;../servlet/servlet.FileDownload?file=015A0000000FU0f&quot;, &quot;Dial&quot;), &quot;&quot;)</formula>
        <inlineHelpText>Click to dial Cell Phone</inlineHelpText>
        <label>NVM Cell</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>NVMContactWorld__NVM_Extension__c</fullName>
        <description>NewVoiceMedia click to dial Extension Phone</description>
        <formula>IF( 
        LEN( Extension  )&gt;0 , 
        HYPERLINK( &quot;/apex/NVMContactWorld__NVM_AgentUI_ClickToDialInitiator?number=&quot; &amp; 
                   SUBSTITUTE(Extension , &quot;+&quot;, &quot;%2B&quot;) &amp; 
                   &quot;&amp;oid=&quot; &amp; Id &amp; 
                   &quot;&amp;name=&quot; &amp; FirstName &amp; &quot;%20&quot; &amp;  LastName &amp; 
                   &quot;&amp;type=User&quot;, 
                   Extension  , &quot;nvm-clicktodial&quot;) 
                   &amp; &quot; &quot; &amp; IMAGE( &quot;../servlet/servlet.FileDownload?file=015A0000000FU0f&quot;, &quot;Dial&quot;), &quot;&quot;)</formula>
        <inlineHelpText>Click to dial Extension Phone</inlineHelpText>
        <label>NVM Extension</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>NVMContactWorld__NVM_Phone__c</fullName>
        <description>NewVoiceMedia click to dial Phone</description>
        <formula>IF( 
        LEN( Phone )&gt;0 , 
        HYPERLINK( &quot;/apex/NVMContactWorld__NVM_AgentUI_ClickToDialInitiator?number=&quot; &amp; 
                   SUBSTITUTE(Phone, &quot;+&quot;, &quot;%2B&quot;) &amp; 
                   &quot;&amp;oid=&quot; &amp; Id &amp; 
                   &quot;&amp;name=&quot; &amp; FirstName &amp; &quot;%20&quot; &amp;  LastName &amp; 
                   &quot;&amp;type=User&quot;, 
                   Phone, &quot;nvm-clicktodial&quot;) 
                   &amp; &quot; &quot; &amp; IMAGE( &quot;../servlet/servlet.FileDownload?file=015A0000000FU0f&quot;, &quot;Dial&quot;), &quot;&quot;)</formula>
        <inlineHelpText>Click to dial Phone</inlineHelpText>
        <label>NVM Phone</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>NVM_User_ID__c</fullName>
        <label>NVM User ID</label>
        <length>25</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>User_ID__c</fullName>
        <formula>Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>User ID</label>
        <type>Text</type>
    </fields>
</CustomObject>
