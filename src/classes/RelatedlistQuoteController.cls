public class RelatedlistQuoteController {
    List<quote> quotes{get;set;}
    //List<quote> quotes_f{get;set;}
   
  // instantiate the StandardSetController from a query locator
  public ApexPages.StandardSetController con{
    get {
        if(con == null) {
            con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, QuoteNumber, Name, Status, GrandTotal, Discount, ExpirationDate, opportunity.Name, Account.Owner.Name, Account.Name FROM quote ORDER BY QuoteNumber DESC]));// where lastmodifieddate >=: system.today()-365]));
            // sets the number of records in each page set
            con.setPageSize(20);
            
        }
        return con;
    }
    set;
   }

 // returns a list of wrapper objects for the sObjects in the current page set
 public List<quote> getQuotes() {
    quotes= (List<quote>) con.getRecords();

    return quotes;
 }

 // indicates whether there are more records after the current page set.
 public Boolean hasNext {
    get {
        return con.getHasNext();
    }
    set;
 }

 // indicates whether there are more records before the current page set.
 public Boolean hasPrevious {
    get {
        return con.getHasPrevious();
    }
    set;
 }

 // returns the page number of the current page set
 public Integer pageNumber {
    get {
        return con.getPageNumber();
    }
    set;
 }

 // returns the first page of records
 public void first() {
    con.first();
 }

 // returns the last page of records
 public void last() {
    con.last();
 }

 // returns the previous page of records
 public void previous() {
    con.previous();
 }

 // returns the next page of records
 public void next() {
    con.next();
 }

 // returns the PageReference of the original page, if known, or the home page.
 public void cancel() {
    con.cancel();
}
 }