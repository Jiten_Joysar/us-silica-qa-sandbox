@isTest(seeAllData = true)
public class TestPopulateWhatIdOrWhoIdUsingKeyRole {
    
    static testmethod void testTeskInsertUpdateTrigger() {
    
        List<User> uList = [ SELECT id,Name FROM User Where (Profile.Name = 'System Administrator' OR Profile.Name = 'U.S. Silica System Administrator' ) AND IsActive = true LIMIT 1 ];
        
        if( uList != null && uList.size() > 0 ) {
        
            System.runAs(uList[0]) {
    
                List<RecordType> recordType = [ SELECT id,Name FROM RecordType Where SObjectType = 'Account' LIMIT 1 ];
                
                if( recordType != null && recordType.size() > 0 ) {
                
                    Account acct = new Account(Name = 'Universal Solutions',RecordTypeId = recordType[0].id);
                    insert acct;
                    
                    Contact[] con = new Contact[] {
                        new Contact(AccountId = acct.id,LastName = 'John Doe'),
                        new Contact(AccountId = acct.id,LastName = 'Albert Kingston')
                    };
                    insert con;
                                        
                    Key_Role__c[] keyRoleRec = new Key_Role__c[] {
                        new Key_Role__c(Account__c = acct.id,Contact__c = con[0].id),
                        new Key_Role__c(Account__c = acct.id,Contact__c = con[0].id),
                        new Key_Role__c(Account__c = acct.id,Contact__c = con[1].id)
                    };
                    insert keyRoleRec;
                                    
                    Task[] tsk = new Task[] {
                        new Task(WhatId=keyRoleRec[0].id,OwnerId=uList[0].id,Subject='Call',Status='Not Started',Priority='Low'),
                        new Task(WhatId=keyRoleRec[2].id,OwnerId=uList[0].id,Subject='Send Quote',Status='In Progress',Priority='Normal')
                    };
                    insert tsk;
                                        
                    tsk[1].WhatId = keyRoleRec[0].id;
                    update tsk[1];
                    
                    Task[] tskRec = new Task[] {
                        new Task(WhoId=con[0].id,OwnerId=uList[0].id,Subject='Call',Status='Not Started',Priority='Low'),
                        new Task(WhoId=con[1].id,OwnerId=uList[0].id,Subject='Send Quote',Status='In Progress',Priority='Normal')
                    };
                    insert tskRec;
                    
                    tskRec[0].WhoId = con[1].id;
                    update tskRec[0];
                    
                }
            }
        }
    }
    
}