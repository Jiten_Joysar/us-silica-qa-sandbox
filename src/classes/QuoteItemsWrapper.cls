public class QuoteItemsWrapper {
    
    public Boolean checked{ get; set; }
    public PricebookEntry cat { get; set;}

    public QuoteItemsWrapper(){
        cat = new PricebookEntry();
        checked = false;
    }

    public QuoteItemsWrapper(PricebookEntry c){
        cat = c;
        checked = false;
    }

    /*public static testMethod void testMe() {

        QuoteItemsWrapper cw = new QuoteItemsWrapper();
        System.assertEquals(cw.checked,false);      

        QuoteItemsWrapper cw2 = new QuoteItemsWrapper(new Product2(name='Test1'));
        System.assertEquals(cw2.cat.name,'Test1');
        System.assertEquals(cw2.checked,false);       

    }*/


}