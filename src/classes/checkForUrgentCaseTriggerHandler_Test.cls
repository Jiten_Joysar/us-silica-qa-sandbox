/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = false)
private class checkForUrgentCaseTriggerHandler_Test {

	/* class variable */
	static case caseObj ;
	private static void setUpData(){
		Account accountobj = new Account(Name = 'Main Account');
		insert accountobj;
		Contact contactObj = new Contact(
					  LastName = 'abc',	
					  Phone = '(301) 682-0680',                                  
					  MobilePhone = '(301) 852-1976',                             
					  AccountId = accountobj.id,                                	     
					  Title = 'Inside Sales Representative',                      
					  Email = 'hoffman@ussilica.com',                            
					  HasOptedOutOfEmail = false
		);
		insert contactObj;
		Account carrierAccount = new Account(Name = 'Carrier Account');
		insert carrierAccount;
		Contact carrierContact = new Contact(
	  				Email = 'ferdinand@ussilica.com',
	  				LastName = 'abc',
	  				AccountId = carrierAccount.id
		);
		Contact contactObjNew = new Contact(
					  LastName = 'xyz',	
					  Phone = '(301) 682-0680',                                  
					  MobilePhone = '(301) 852-1976',                             
					  AccountId = carrierAccount.id,                                	     
					  Title = 'Inside Sales Representative',                      
					  Email = 'hoffman@ussilica.com',                            
					  HasOptedOutOfEmail = false
		);
		insert carrierContact;
		caseObj = new Case(
					  Status = 'Pending Shipment',                           
					  AccountId = accountobj.id,                           	     
					  Carrier_Name__c = carrierAccount.id,					
					  Origin = 'Email - ISP Customer Care',                  
					  ContactID = contactObj.id,                                  
					  Subject = 'test',                                      
					  Description = 'test',                                  
					  Priority = 'Medium',
					  RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('ISP Customer Order').getRecordTypeId(),
					  is_urgent__c = true                                    
		);
		insert caseObj;
	}
    public static testMethod void myUnitTest() {
    	setUpData();
    	Test.startTest();
    	update caseObj;
    	caseObj.First_Acknowledged__c = Date.Today()+2 ;
    	update caseObj;
    	caseObj.Last_Acknowledged_Date_1__c = Date.Today()+2 ;
    	update caseObj;
    	caseObj.Last_Acknowledged_Date_2__c = Date.Today()+2 ;
    	update caseObj;
    	caseObj.Status = 'Closed - Final' ;
    	update caseObj;
    	Test.stopTest();
    }
}