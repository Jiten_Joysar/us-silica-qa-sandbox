@IsTest
public class TestUpdateExistingAcctTrigger {

   public static testMethod void TestUpdateExistingAcctTrigger () {
  
        List<Account> updateAccountList = new List<Account>();
        List<Territory_Owner_Mapping__c> tomList = new List<Territory_Owner_Mapping__c>();
        List<Territory_Mapping__c> tmList = new List<Territory_Mapping__c>();
        Id rtSId = [ SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'ShipTo'].Id;
        Id rtBId = [ SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'BillTo'].Id;        
       
        
        List<User> userList = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
        
        Territory_Owner_Mapping__c tom = new Territory_Owner_Mapping__c(
            Name = 'Northeast', Inside_Sales_Rep__c = userList[0].Id, Regional_Sales_Manager__c = userList[1].Id);
        tom.Threshold__c = 6000;
        tom.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
        insert tom;   
        
        Account testAccount = new Account(Name = 'Test Account1');
        testAccount.AnnualRevenue = 1000;
        testAccount.RecordTypeId = rtSId;
        testAccount.ShippingState = 'Alabama';
        testAccount.ShippingCountry = 'USA';
        testAccount.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
        testAccount.National_Account__c = false;
        testAccount.Account_Ownership_Override__c = false;
        testAccount.Territory_Owner_Mapping__c = tom.Id;
      
        Insert testAccount;
   
        tom.Threshold__c = 3000;
        update tom;
        
        tom.Threshold__c = 1000;
        update tom;
      
       
    }
}