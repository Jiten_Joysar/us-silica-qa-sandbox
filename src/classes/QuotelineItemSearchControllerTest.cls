/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
public class QuotelineItemSearchControllerTest {

    public static testMethod void setUpAllDataForPositiveScenario(){
        
        QuotelineItemSearchController quoteLineItemObj = new QuotelineItemSearchController();
        QuoteItemsWrapper wrapperObj = new QuoteItemsWrapper();
        List<QuoteItemsWrapper> wrapperList = new List<QuoteItemsWrapper>();
        List<String> product = new List<String>();
        product.add('a');
        product.add('b');
        List<String> regions = new List<String>();
        regions.add('a');
        regions.add('b');
        List<String> location = new List<String>();
        location.add('a');
        location.add('b');
        quoteLineItemObj.products = product;
        quoteLineItemObj.regions = regions;
        quoteLineItemObj.location = location;
        wrapperObj.checked = true;
        PricebookEntry priceBookObj = new PricebookEntry();
        Product2 pr2 = new Product2(); 
        //priceBookObj.name = 'abc';
        priceBookObj.UnitPrice = 30;
        pr2.Unit_Cost__c = 20;
        priceBookObj.Product2 = pr2;
        wrapperObj.cat = priceBookObj;
        wrapperList.add(wrapperObj);
        quoteLineItemObj.selectedPriceBook = 'priceBookObj';
        quoteLineItemObj.resultsToShow = wrapperList;
        test.startTest();
        quoteLineItemObj.getlItems();
        quoteLineItemObj.getrItems();
        quoteLineItemObj.getpItems();
        quoteLineItemObj.back();
        quoteLineItemObj.backtostd();
        quoteLineItemObj.test();
        quoteLineItemObj.Custom_First();
        quoteLineItemObj.Custom_Next();
        quoteLineItemObj.Custom_Last();
        quoteLineItemObj.Custom_Save();
        quoteLineItemObj.resultsToShow = wrapperList;
        quoteLineItemObj.next();
        quoteLineItemObj.search();
        quoteLineItemObj.searchText = 'abc';
        test.stopTest();

    }
}