/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCreditRebillController {

    static testMethod void testCreditRebill() {
		Account accObj=Utility_TestClass.prepareAccount('testaccount','Parent',null,true);
      	Contact contactTest =Utility_TestClass.prepareContact(accObj.Id,'contactlast','test');
      	Case csObj=Utility_TestClass.prepareCase(accObj.Id,contactTest.Id,'testregion','testcaseorigin','teststatus','test.xxx@astadia.com','last, Test','test',true);
    	
    	csObj.Customer_Preference__c = 'Credit Rebill Each Line';
    	csObj.Credit_Reason_Code__c = 'Quality Issue';
    	csObj.Rebill_Comments__c = 'Comments';
    	update csObj;
    	
		Credit_Rebill__c crbill = new Credit_Rebill__c();
		crbill.Case__c = csObj.Id;
		
		insert crbill;
		
    	test.startTest();
    	ApexPages.StandardController stdControl = new ApexPages.StandardController(csObj);
        CreditRebillController cr = new CreditRebillController(stdControl);
        cr.tabId = '1';
        cr.plus = '1';
        cr.Minus = '1';
        cr.meetingId = '1';
        cr.aId = '1';
        
        cr.createNewAction();
        cr.Save();
        cr.SaveActionChanges();
        cr.SaveCloseActionChanges();
        cr.cancelChanges();
        cr.deleteAction();
        test.StopTest();
    }
}