public class QuotelineItemSearchController {
    
    Public String VarQuoteId{get;set;}
    Public String oppId{get;set;}
    public List<String> products{get;set;} 
    Public Quote varQuote{get;set;}
    Public String selectedPriceBook{get;set;}
    public List<String> regions {get;set;}
    Public List<Quotelineitem> varQLIList {get;set;}
    public  List<String> location {get;set;}
    public List<pricebookentry> pbeList ;
    // the results from the search. do not init the results or a blank rows show up initially on page load
    public List<QuoteItemsWrapper> searchResults {get;set;}
    //Pagination variable  
    Public Map<integer,List<QuoteItemsWrapper>>  pageNumberwithList{get;set;}
    Integer pageSize=20;
    Integer totalPages;
    Integer currentPage;
    public List<QuoteItemsWrapper> resultsToShow {get;set;}
    public boolean disablePrevious{get;set;}
    public boolean disableNext{get;set;}
    
    // the categories that were checked/selected.
    public List<QuoteItemsWrapper> selectedCategories {
        get {
            if (selectedCategories == null) selectedCategories = new List<QuoteItemsWrapper>();
            return selectedCategories;
        }
        set;
    }  
    // the text in the search box
    public string searchText {
        get {
            if (searchText == null) searchText = ''; // prefill the serach box for ease of use
            return searchText;
        }
        set;
    } 
    // constructor
    public QuotelineItemSearchController() {
	    resultsToShow  = null;
	    pageNumberwithList=new Map<integer,List<QuoteItemsWrapper>>();
	    disablePrevious=true;
	    disableNext=true;
	    varQuote=new Quote();
	    if(ApexPages.currentPage().getParameters().get('id')!=null)
	    {
	    VarQuoteId=ApexPages.currentPage().getParameters().get('id');
	    
	    varQuote=[select id,name,Opportunity.id,Opportunity.pricebook2Id,pricebook2Id from Quote where id =:VarQuoteId];
	    if(!String.IsBlank(varQuote.pricebook2Id))
	    {
	    selectedPriceBook=varQuote.pricebook2Id ;
	   
	    }
	    else
	    {
	    selectedPriceBook=[select id,name from pricebook2 where name='Oil & Gas Price Book' limit 1].id;
	    }
	        system.debug('selectedPriceBook'+selectedPriceBook);
	    }
	    pbeList = new List<pricebookentry>();
	    pbeList = [Select Name,id,Product2.Region__c,Product2.Branch_Plantfromaccount__r.Name From PricebookEntry Where isactive=true and Pricebook2Id= :selectedPriceBook limit 50000];
	   
    }
    
    // fired when the search button is clicked
    public PageReference search() {
    	
        if(!String.isBlank(selectedPriceBook))
        {
                if (searchResults == null) {
                    searchResults = new List<QuoteItemsWrapper>(); // init the list if it is null
                } else {
                    searchResults.clear(); // clear out the current results if they exist
                }   
                
                // dynamic soql
                String qry;
                system.debug('products  '+products);
                system.debug('Loc  '+location);
                system.debug('Region  '+regions);
                
                if(products.size()>0){
                     
                    qry = 'Select Name,id,UnitPrice,Product2Id,Product2.Branch_Plantfromaccount__r.Name,Pricebook2.name,Product2.ProductCode,Pricebook2Id,Product2.Branch_Plantfromaccount__c,Product2.Region__c,Product2.Unit_of_Measure__c,Product2.Unit_Cost__c,Product2.Product_Group__c From PricebookEntry Where isactive=true and Pricebook2Id=\''+selectedPriceBook+'\' and Name IN: products';
                    
                } 
                if(regions.size()>0){
                                      
                    if(qry!=null && qry!=''){
                        qry+=' AND Product2.Region__c IN :regions'; 
                    }else{
                        qry = 'Select Name,id,UnitPrice,Product2Id,Product2.Branch_Plantfromaccount__r.Name,Pricebook2.name,Product2.ProductCode,Pricebook2Id,Product2.Branch_Plantfromaccount__c,Product2.Region__c,Product2.Unit_of_Measure__c,Product2.Unit_Cost__c,Product2.Product_Group__c From PricebookEntry Where isactive=true and Pricebook2Id=\''+selectedPriceBook+'\' and Product2.Region__c IN: regions';
                    }
                }
                if(location.size()>0){
                    
                         if(qry!=null && qry!=''){
                         qry+=' AND Product2.Branch_Plantfromaccount__c IN :location';
                        }else{
                            qry = 'Select Name,id,UnitPrice,Product2Id,Product2.Branch_Plantfromaccount__r.Name,Pricebook2.name,Product2.ProductCode,Pricebook2Id,Product2.Branch_Plantfromaccount__c,Product2.Region__c,Product2.Unit_of_Measure__c,Product2.Unit_Cost__c,Product2.Product_Group__c From PricebookEntry Where isactive=true and Pricebook2Id=\''+selectedPriceBook+'\' and Product2.Branch_Plantfromaccount__r.Name IN: location';
                        }
                }     
                system.debug('query '+qry);
                if(qry!=null){
                    List<PricebookEntry> priceBookList = new List<PricebookEntry>();
                    try{
                    priceBookList = Database.query(qry);
                    }
                    catch(Exception ex){
                        System.debug('Exception : '+ex);
                    }
                    for(PricebookEntry  c : priceBookList) {
                        // create a new wrapper by passing it the category in the constructor
                        QuoteItemsWrapper cw = new QuoteItemsWrapper(c);
                        
                            // add the wrapper to the results
                        searchResults.add(cw);
                    }
                     totalPages=searchResults.size()/pageSize+1;
                     
                     resultsToShow =new  List<QuoteItemsWrapper>();
                      integer tempVar=0;
              integer pageNumberVar=1;
              List<QuoteItemsWrapper>  tempList=new List<QuoteItemsWrapper>();
              for(integer i=0;i<searchResults.size();i++)
              {
              tempList.add(searchResults[i]);
              tempVar++;
                  if(tempVar==pageSize)
                  {
                      tempVar=0;
                      pageNumberwithList.put(pageNumberVar,tempList);
                      pageNumberVar++;
                      tempList=new List<QuoteItemsWrapper>();
                  }
              }
              if(tempList!=null)
              {
              pageNumberwithList.put(pageNumberVar,tempList);
              }
              if(pageNumberVar>1)
              {
              disableNext=false;
              }
              else 
              disableNext =true;
              }
              Custom_First();
        }
        
        return null;
    }   
    
    
     Public PageReference Custom_First()
	    {
	    resultsToShow=pageNumberwithList.get(1);
	    currentPage=1;
	    disablePrevious=true;
	    disableNext=false;
	    return null;
	    } 
    Public PageReference Custom_Next()
	    {
	    if(currentPage!=totalPages)
	    {
		    currentPage=currentPage+1;
		    disablePrevious=false;
	    }
	    else
	    {
	    	disableNext=true;
	    }
	    resultsToShow=pageNumberwithList.get(currentPage);
	    return null;
	    }
    Public PageReference Custom_Last()
	    {
	     resultsToShow=pageNumberwithList.get(totalPages);
	     currentPage=totalPages;
	     disablePrevious=false;
	     disableNext=true;
	     return null;
	    }
    Public PageReference Custom_Previous()
	    {
	     if(currentPage==1)
	     {
		  	disableNext=false;
		    disablePrevious=True;
	     }
	    else
	     {
		    currentPage=currentPage-1;
		    disableNext=false;
	    
	     }
	     resultsToShow=pageNumberwithList.get(currentPage);
	    return null;
	    }
    
    Public PageReference Custom_Save()
    {
        try
        {
        Quote q=new Quote();
        q.id=varQLIList[0].QuoteId;
        system.debug('****selectedPriceBook****'+selectedPriceBook);
        system.debug('****varQLIList[0].QuoteId****'+varQLIList[0].QuoteId);
        system.debug('****varQLIList[0]****'+varQLIList[0]);
        q.Pricebook2Id=selectedPriceBook;  
        update q;         
        insert varQLIList;
        system.debug('****varQLIList****'+varQLIList);
        }
        Catch(Exception e)
        {
           system.debug('****exception msg'+e.getMessage());
        }   
        PageReference pageRef = new PageReference('/'+VarQuoteId);
        return pageRef;
    }
    
    public PageReference next() {
        varQLIList =new List<Quotelineitem>();
        Quotelineitem tempQuote;
        // clear out the currently selected categories
        selectedCategories.clear();
        System.debug('Check2');
        // add the selected categories to a new List
        for (QuoteItemsWrapper cw : resultsToShow) {
            if (cw.checked)
            {
            tempQuote=new Quotelineitem();
            tempQuote.PricebookEntryId=cw.cat.id;
            // tempQuote.Pricebook2Id=cw.cat.id;
            tempQuote.Product2Id=cw.cat.Product2Id;
            tempQuote.Product_Name__c=cw.cat.name;
            tempQuote.QuoteId=VarQuoteId;
            tempQuote.UnitPrice= cw.cat.UnitPrice;
            tempQuote.Ucost__c= cw.cat.Product2.Unit_Cost__c.setScale(2, RoundingMode.HALF_UP);
            tempQuote.Quantity = 1; // G10
            tempQuote.Branch_Plantlookup__c = cw.cat.Product2.Branch_Plantfromaccount__c ; //G10
            System.debug('value of Product2.Branch_Plantfromaccount__c '+cw.cat.Product2.Branch_Plantfromaccount__c);
            System.debug('value of Product2.Branch_Plantfromaccount__r.Name '+cw.cat.Product2.Branch_Plantfromaccount__r.Name);
            //tempQuote.Branch_Plantlookup__r.Name = cw.cat.Product2.Branch_Plantfromaccount__r.Name ; //G10
            varQLIList.add(tempQuote);
            System.debug('============='+tempQuote);
            tempQuote=null;
            }
        }
        // ensure they selected at least one category or show an error message.
        if (varQLIList.size() > 0) {
            return Page.Quoteitems_Results;
            /*PageReference ReturnPage = new PageReference('/quoteitm/multilineitem.jsp'); 
            ReturnPage.setRedirect(true);
            return ReturnPage;*/
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one Category.'));
            return null;
        } 
    }       
    
    // fired when the back button is clicked
    public PageReference back() {
        return Page.Quoteitems_Search;
    }   
    
    public PageReference backtostd() {
        Pagereference pg =  new Pagereference('/'+VarQuoteId); 
        pg.setRedirect(true);
        return pg;
    } 
    public PageReference reset() {
        PageReference newpage = new PageReference(System.currentPageReference().getURL());
        newpage.setRedirect(true);
        return newpage;
    }
    public PageReference test() {
        return null;
    }             
    
    public List<SelectOption> getpItems() {
        
        List<SelectOption> prdopts = new List<SelectOption>();
        Set<String> pbeNameSet = new Set<String>();
        List<String> pbeNameList = new List<String>();
        for(PricebookEntry pbe : pbeList){
        	pbeNameSet.add(pbe.Name);
        }
        for(String name : pbeNameSet)
        	pbeNameList.add(name);
        pbeNameList.sort();	
        for(String name : pbeNameList)
        	prdopts.add(new SelectOption(name,name));	
        return prdopts;
        
    }       
    
    public List<SelectOption> getrItems() {
        
        List<SelectOption> regopts = new List<SelectOption>();
        Set<String> pricebookRegionSet = new Set<String>();
        for(PricebookEntry pbe : pbeList){
        	if(pbe.Product2.Region__c != null)
        		pricebookRegionSet.add(pbe.Product2.Region__c);
        }
        if(pricebookRegionSet.size() > 0)
        for(String name : pricebookRegionSet)
        	regopts.add(new SelectOption(name,name));
        return regopts;
    }
    
    public List<SelectOption> getlItems() {
        
        List<SelectOption> locopts = new List<SelectOption>();
        Set<String> locationSet = new Set<String>();
        for(PricebookEntry pbe : pbeList){
        	if(pbe.Product2.Branch_Plantfromaccount__r.Name != null)
        		locationSet.add(pbe.Product2.Branch_Plantfromaccount__r.Name);
        }
        if(locationSet.size() > 0){
        	List<String> stringList = new List<String>();
	        for(String name : locationSet)
	        	stringList.add(name);
	        stringList.sort();
	        for(String name : stringList)
        		locopts.add(new SelectOption(name,name));
	        }
        return locopts;
    }
}