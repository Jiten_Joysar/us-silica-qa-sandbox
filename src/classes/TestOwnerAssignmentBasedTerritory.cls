@IsTest
public class TestOwnerAssignmentBasedTerritory {
  public static testMethod void createTestAccounts() {
    List<Account> updateAccountList = new List<Account>();
    List<Territory_Owner_Mapping__c> tomList = new List<Territory_Owner_Mapping__c>();
    List<Territory_Mapping__c> tmList = new List<Territory_Mapping__c>();
    Id rtSId = [ SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'ShipTo'].Id;
    Id rtBId = [ SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'BillTo'].Id;
    
    List<User> userList = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
    
    Territory_Owner_Mapping__c tom = new Territory_Owner_Mapping__c(Name = 'Northeast', Inside_Sales_Rep__c = userList[0].Id, Regional_Sales_Manager__c = userList[1].Id);
    tom.Threshold__c = 6000;
    tom.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
    
    Territory_Owner_Mapping__c tom1 = new Territory_Owner_Mapping__c(Name = 'Southeast', Inside_Sales_Rep__c = userList[1].Id, Regional_Sales_Manager__c = userList[0].Id);
    tom1.Threshold__c = 7000;
    tom1.Line_of_Business__c = 'Oil and Gas';
        
    tomList.add(tom1);

    Territory_Owner_Mapping__c tom2 = new Territory_Owner_Mapping__c(Name = 'Southeast', Inside_Sales_Rep__c = userList[1].Id, Regional_Sales_Manager__c = userList[0].Id);
    tom2.Threshold__c = 5000;
    tom2.Line_of_Business__c = 'Parent';
        
    tomList.add(tom2);
    
    Insert tomList;
    
    Territory_Mapping__c tm = new Territory_Mapping__c();
    tm.Country__c = 'US';
    tm.ISP_Territory_Name__c = tomList[0].Id;
    tm.O_G_Territory__c = tomList[0].Id;
    tm.State__c = 'Alabama';
    tmList.add(tm);
    
    Insert tmList;
    
    Account acc = new Account(Name = 'Sample Account Name');
    acc.AnnualRevenue = 100;
    acc.RecordTypeId = rtSId;
    acc.ShippingState = 'Alabama';
    acc.ShippingCountry = 'USA';
    acc.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
    acc.National_Account__c = false;
    acc.Account_Ownership_Override__c = false;
    Insert acc;

    Account acc1 = new Account(Name = 'Sample Account Name1');
    acc1.AnnualRevenue = 70000;
    acc1.RecordTypeId = rtBId;
    acc1.Billing_State_Province__c = 'Alabama';
    acc1.Billing_Country__c = 'US';
    //acc1.Line_of_Business__c = 'Oil and Gas';
    acc1.Line_of_Business__c = 'Parent';
    acc1.National_Account__c = false;
    acc1.Account_Ownership_Override__c = false;    
    Insert acc1;

    Account acc2 = new Account(Name = 'Sample Account Name2');
    acc2.AnnualRevenue = 70000;
    acc2.RecordTypeId = rtBId;
    acc2.Billing_State_Province__c = 'Alabama';
    acc2.Billing_Country__c = 'US';
    acc2.Line_of_Business__c = 'Oil and Gas';
    acc2.National_Account__c = false;
    acc2.Account_Ownership_Override__c = false;    
    Insert acc2;    
    
    /*
    Account acc2 = new Account(Name = 'Sample Account Name2');
    acc2.AnnualRevenue = 200;
    acc2.RecordTypeId = rtBId;
    acc2.Billing_State_Province__c = 'New york';
    acc2.Billing_Country__c = 'USA';
    acc2.Line_of_Business__c = 'Oil and Gas';
    //acc1.National_Account__c = false;
    //acc1.Account_Ownership_Override__c = true;    
    Insert acc2; */   

    Account upAcc = new Account(Id = acc.Id, Name = 'Sample Account Name');
    upAcc.AnnualRevenue = 80000;
    upAcc.RecordTypeId = rtBId;
    upAcc.ShippingCountry = 'US';
    upAcc.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
    
    update upAcc;
    //updateAccountList.add(upAcc);
    
    Account upAcc1 = new Account(Id = acc1.Id, Name = 'Sample Account Name1');
    upAcc1.AnnualRevenue = 200;    
    upAcc1.RecordTypeId = rtSId;
    upAcc1.ShippingCountry = 'United States';
    upAcc1.Line_of_Business__c = 'Oil and Gas';
    update upAcc1;

    Account upAcc2 = new Account(Id = acc1.Id, Name = 'Sample Account Name2');
    upAcc2.AnnualRevenue = 12200;    
    upAcc2.RecordTypeId = rtSId;
    upAcc2.ShippingCountry = 'United States';
    upAcc2.Line_of_Business__c = 'Parent';
    update upAcc2;    

    upAcc2 = new Account(Id = acc1.Id, Name = 'Sample Account Name2');
    upAcc2.AnnualRevenue = 200;    
    upAcc2.RecordTypeId = rtSId;
    upAcc2.ShippingCountry = 'United States';
    upAcc2.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
    update upAcc2;

    upAcc2 = new Account(Id = acc1.Id, Name = 'Sample Account Name2');
    upAcc2.AnnualRevenue = 200;    
    upAcc2.RecordTypeId = rtBId;
    upAcc2.ShippingCountry = 'United States';
    upAcc2.Line_of_Business__c = 'Parent';
    update upAcc2; 
    
    //updateAccountList.add(upAcc1);
    
    //Update updateAccountList;
    
  }
}