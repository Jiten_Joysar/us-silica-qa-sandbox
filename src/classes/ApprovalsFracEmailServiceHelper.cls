/*
###########################################################################
# Created by............:Megha Bhardwaj.
# Created Date..........:09/18/2014.
# Last Modified by......:Megha Bhardwaj.
# Last Modified Date....:09/24/2014.
# Description...........:This is a helper class for ApprovalsFracEmailServiceClass.
#
# Change Log:
# S.No.   Date       Author         Description 
#  1   09/18/2014  Megha Bhardwaj  Initial Version
#
###########################################################################
*/ 
public class ApprovalsFracEmailServiceHelper{
    public static final string KEY_EMAIL='EmailAddresses';
    public static final string KEY_ACCOUNT='AccountIds';
    /*
    * @description :This function is populating case details.
    * @return : Void.
    * @param : Param 1 : Case caseObj- An instance of Case.
    *          Param 2 : Messaging.InboundEmail email- email is the complete body of the email which is coming to the Salesforce generated email Id through EmailService.
    */
    public static void PopulateCaseDetails(Case caseObj,Messaging.InboundEmail email,Messaging.InboundEnvelope envelope){
        try{
            
            caseObj.Status=Label.ES_Status;
            caseObj.Origin=Label.ES_Origin;
            caseObj.RecordTypeId=Utility.SelectRecordType('Case',Label.ES_RecordType);
            caseObj.Product_Description__c=email.htmlbody;
            caseObj.Subject=email.subject;
            map<string,id> mapToAssign=new map<string,id>();
            map<string,list<string>> mapEmail=parseEmailBody(mapToAssign,email.plaintextbody);
            if(envelope!=null)
                caseObj.ContactId=SelectContact(envelope.fromAddress);
            list<string> listkeyacc=mapEmail.get(KEY_ACCOUNT);
          
            if(Utility.CheckForNullList(listkeyacc))
            {
                if(listkeyacc.get(0) != null)
                    caseObj.AccountId=Id.Valueof(listkeyacc.get(0));
            }
            else
                caseObj.AccountId = null;
            system.debug('reached'+ caseObj.AccountId);
            AssignApprovers(caseObj,mapEmail,mapToAssign);
         
       }
       catch(Exception e)
       {
            System.debug('Exception occurred'+e);
       }
    }
    /*
    * @description :This function is parsing the complete plain text email.
    * @return : Map of Key and list of email addresses and account id.
    * @param : Param 1 :complete plain text email.
    *          Param 2 : N/A.
    */
    public static map<string,list<string>> parseEmailBody(map<string,id> mapToAssign, string emailplain){
        map<string,list<string>> mapAccountEmail= new map<string,list<string>>();
        try
        {
    
            string emailAddressString=emailplain.toupperCase().substringAfter(Label.ES_ParseFromHere.touppercase());
            emailAddressString=emailAddressString.trim();
            System.Debug('debug'+emailAddressString); 
            string companyName=emailplain.toUpperCase().substringBetween(Label.ES_CompanyName.toUpperCase(),Label.ES_CompanyAddress.toUpperCase());
            companyName=companyName.trim();
            mapAccountEmail.put(KEY_EMAIL,GetEmailAddresses(mapToAssign, emailAddressString));
            mapAccountEmail.put(KEY_ACCOUNT,GetAccountId(companyName));
            System.Debug('##'+mapAccountEmail);         
        }
        catch(Exception e)
        {
            System.debug('Exception occurred'+e);
            
        }
        return mapAccountEmail;
    }
    /*
    * @description :This function is generating the email addresses.
    * @return : list of email addresses.
    * @param : Param 1 : a section of the complete email.
    *          Param 2 : N/A.
    */
    public static list<string> GetEmailAddresses(map<string,id> mapToAssign, string parsedEmail){
        list<string> emailAddressList=new list<string>();
    
        try{
          list<string> emailstringList=parsedEmail.split('\n',0);
          
           list<string> emailstringList2=new list<string>();
      
          for(string emailCheck:emailstringList){
            if(!(string.isBlank(emailCheck)))
              emailstringList2.add(emailCheck.tolowerCase());    
          }
        
          for(string emailString:emailstringList2)
          {      
                string temp='';
                if( emailString.contains('@') )  
                {        
                          temp=emailString.substringbefore(Label.ES_EmailAddressStarting);
                          temp=temp.trim()+Label.ES_SandboxEmail; //as usernames are different in sandbox and production
                          temp=temp.removeEnd('.'); //as usernames are different
                          emailAddressList.add(temp);
                          mapToAssign.put(temp,null); 
                } 
                else if(!emailString.contains('@'))
                    break; 
      
           }
        }
        catch(Exception e)
        {
            System.debug('Exception occurred'+e);
        }
         return emailAddressList;
         
    }
    /*
    * @description :This function is generating the related account id.
    * @return : list of account ids.
    * @param : Param 1 : company name from email.
    *          Param 2 : N/A.
    */
     public static list<string> GetAccountId(string companyName){
        try{
         list<Account> accountNameList=[Select Id,Name  from Account where Name =:companyName limit 1];
         if(Utility.CheckForNullList(accountNameList)){
            return new list<string>{string.valueOf(accountNameList[0].Id)};
         }
        
        }
       catch(Exception e)
        {
            System.debug('Exception occurred'+e);
        }  
        return null; 
     }
    /*
    * @description :This function is assigning the user ids to user looks.
    * @return : Void.
    * @param : Param 1 : an instance of Case.
    *          Param 2 : map of keys and email addresses.
    */
     public static void AssignApprovers(Case caseObject,map<string,list<string>> mapAccEmail,map<string,id> mapToAssign){
        try{
            list<string> listKeyEmail=mapAccEmail.get(KEY_EMAIL);
            system.debug('#####key'+listKeyEmail);
            if(Utility.CheckForNullList(listKeyEmail)){
                list<User> listUser=new list<User>();
                listUser=[SELECT Id, Name, Email,Username FROM User WHERE Username IN :listKeyEmail];
                for(User userFor:listUser)
                {
                    mapToAssign.put(userFor.Username,userFor.Id);
                }
                Integer counter=0;
                system.debug('user map'+mapToAssign);
                Integer listSize=listKeyEmail.size();
                if(counter<listSize && listKeyEmail.get(counter)!=null)
                    caseObject.Oil_Gas_Pricing_Approver_1__c=mapToAssign.get(listKeyEmail.get(counter));
                    
                counter=counter+1;
                if( counter<listSize && listKeyEmail.get(counter)!=null) 
                   caseObject.Oil_Gas_Pricing_Approver_2__c=mapToAssign.get(listKeyEmail.get(counter));
              
                counter=counter+1;
                if(counter<listSize && listKeyEmail.get(counter)!=null)
                   caseObject.Oil_Gas_Pricing_Approver_3__c=mapToAssign.get(listKeyEmail.get(counter));
                  
                counter=counter+1;
                if(counter<listSize && listKeyEmail.get(counter)!=null)
                  caseObject.Oil_Gas_Pricing_Approver_4__c=mapToAssign.get(listKeyEmail.get(counter));
                
                counter=counter+1;
                if(counter<listSize && listKeyEmail.get(counter)!=null)
                  caseObject.Oil_Gas_Pricing_Approver_5__c=mapToAssign.get(listKeyEmail.get(counter));
            }  
        }       
        catch(Exception e)
        {
            System.debug('Exception occurred'+e);
        }
     }
     /*
    * @description : Function to fetch Contact Id
    * @return : Id    
    * @param  : email : The From Address from email  
    */     
    public static Id SelectContact(string email)
    {
                list<Contact> conObj = [SELECT Id,Email FROM Contact WHERE Email = :email and Contact.Account.Name=:label.ES_AccountName LIMIT 1];
                System.debug('##'+conObj);
                System.debug('##Email'+email);
                if(Utility.CheckForNullList(conObj))
                    return conObj.get(0).id;
                else 
                    return null;
    }   
      

}