public class QuoteCloneWithItemsController {
    
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    // add the instance for the variables being passed by id on the url
    public Quote qte {get;set;}
    
    public PricebookEntry pb;
    public Product2  p2;
    
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}
    
    // initialize the controller
    public QuoteCloneWithItemsController(ApexPages.StandardController controller) {
        
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        qte = (Quote)controller.getRecord();
        
    }
    
    // method called from the VF's action attribute to clone the po
    public PageReference cloneWithItems() {
        
        // setup the save point for rollback
        Savepoint sp = Database.setSavepoint();
        Quote newQte;
        
        try {
            
            //copy the Quote - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
            qte = [select Id, Pricebook2Id, Account.Name, Opportunity.Name, Name, Quote_Type__c, Quote_Rejected_Reason_Code__c, Associated_Rate_Request_Case__c,Quote_Reason__c, Pricing_Type_CC13__c, Effective_Date__c, ExpirationDate,Branch_Plant__c,Quantity__c,Lead_Time__c,Minimum_Quantity__c,Price_Variance__c,Total_Quote_Quantity__c,Payable_Rate_per_Ton__c,Average_Billable_Rate_per_Ton__c,Billable_Freight_Amount__c,Car_Charges__c,Car_Charge_Quote_Options__c,Shipping_Handling_Fee1__c,Mode_of_Transportation__c,Business_Secured__c,Bill_To_Name__c,Bill_To_Address__c,Ship_To_Name__c,Ship_To_Address__c,Alternate_Ship_To_Address__c from Quote where id =:qte.id];            
            system.debug('***qte***'+qte);
            newQte = qte.clone(false);            
            insert newQte;
            
            List<QuoteLineItem > items = new List<QuoteLineItem >();
            for (QuoteLineItem  ql : [Select Bags_per_Pallet__c,Branch_Plantlookup__c,Branch_Plant_from_Quote__c,Branch_Plant__c,Car_Charges__c,Contribution_Margin__c,CreatedById,
                                      CreatedDate,Delivered_Price_per_TonOG__c,Description,Discount,Id,IsDeleted,Item_Number__c,LineNumber,ListPrice,LTL_Upcharge__c,Margin__c,Material_Price__c,MISC_Charge__c,Overtime__c,Package_Type__c,Pallet_Charge__c,Premium_Discount__c,
                                      PricebookEntryId,Price_Per_Unit__c,Price_Variance__c,Product2Id,Product_Group__c,Product_Mark_Up__c,Product_Price__c,Quantity,QuoteId,
                                      Quote_Type__c,ServiceDate,Shrink_Wrap__c,Shroud__c,SortOrder,Subtotal,Subtotal__c,SystemModstamp,TotalPrice,Total_Delivered_Price__c,
                                      Total_Freight_per_Unit__c,Total_List_Price__c,Total_PriceOG__c,Total_Price__c,Transload_Premium__c,UnitPrice,Unit_Cost__c,Unit_of_Measure__c From QuoteLineItem  where QuoteId = :qte.id]) 
            {
                QuoteLineItem  newQL    = ql.clone();
                newQL.QuoteId           = newQte.id;
                
                //newQL.PricebookEntryId = ql.PricebookEntryId;
                //newQL.Product2Id  = ql.Product2Id;
                items.add(newQL);
            }
            
            
            insert items;            
            
            
        } catch (Exception e){
            //roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }
        
        return new PageReference('/'+newQte.id+'/e?retURL=%2F'+newQte.id);
    }
    
}