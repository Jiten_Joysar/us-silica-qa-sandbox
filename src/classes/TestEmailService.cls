/*
###########################################################################
# Created by............:Megha Bhardwaj.
# Created Date..........:09/25/2014.
# Last Modified by......:Aditi Garg.
# Last Modified Date....:11/25/2014.
# Description...........:This is a test class of ApprovalsFracEmailServiceClass.
#
# Change Log:
# S.No.   Date       Author         Description 
#  1   09/26/2014  Megha Bhardwaj  Initial Version
#
###########################################################################
*/ 
@isTest
private class TestEmailService
{
   /*
    * @description :This is a test method.
    * @return : Void.
    * @param :  N/A.
    *      
    */
  static testMethod void TestMailPositive()
  {
    //create a new email and envelope object
    Messaging.InboundEmail email = new Messaging.InboundEmail() ;
    Messaging.InboundEnvelope envelopForEmail = new Messaging.InboundEnvelope();
    Profile pf = [Select Id from Profile where Name = 'Standard User' limit 1];
    list<user> listUser=new list<user>();
    listUser.add(Utility_TestClass.createUser('user1','USS',pf.Id,false));
    listUser.add(Utility_TestClass.createUser('user2','USS',pf.Id,false));
    listUser.add(Utility_TestClass.createUser('user3','USS',pf.Id,false));
    listUser.add(Utility_TestClass.createUser('user4','USS',pf.Id,false));
    listUser.add(Utility_TestClass.createUser('user5','USS',pf.Id,false));
    insert listUser;
    
    email.subject = 'Test USS Email Service';
    email.plaintextbody=Utility_TestClass.ReadStaticResource('PlainTextEmailForTestClassEmailService');
    email.htmlbody=Utility_TestClass.ReadStaticResource('HTMLEmailForEmailServiceTestClass');
    
    Territory_Owner_Mapping__c tomObj=Utility_TestClass.prepareTerritoryOwnerMapping('TerritoryTestTest');
     
    List<Account> ListAccount=new List<Account>();
    ListAccount.add(Utility_TestClass.prepareAccount('testaccount','Parent',tomObj.Id,false));
    ListAccount.add(Utility_TestClass.prepareAccount(Label.ES_AccountName,'Parent',tomObj.Id,false));
    insert ListAccount;
    
    Contact contactTest =Utility_TestClass.prepareContact(ListAccount.get(1).Id,'contactlast','test');
    
    envelopForEmail.fromAddress='testcontactlast@test.com';
    
    test.starttest();
    ApprovalsFracEmailServiceClass emailTestObj = new ApprovalsFracEmailServiceClass();
    // call the method of email service class and test it with the data in the testMethod
    emailTestObj.handleInboundEmail(email, envelopForEmail);
    test.stoptest();
   
  }
  /*
  * @description :This is a test method for negative scenarios.
  * @return : Void.
  * @param :  N/A.
  *      
  */
  static testMethod void TestMailNegative()
  {
    //create a new email and envelope object
    Messaging.InboundEmail email = new Messaging.InboundEmail() ;
    Messaging.InboundEnvelope envelopForEmail = new Messaging.InboundEnvelope();
    email.plaintextbody=Utility_TestClass.ReadStaticResource('PlainEmailForNegativetest');
    test.starttest();
    envelopForEmail.fromAddress='rttrtrt';
    
    Territory_Owner_Mapping__c tomObj=Utility_TestClass.prepareTerritoryOwnerMapping('TerritoryTestTest'); 
    List<Account> ListAccount=new List<Account>();
    ListAccount.add(Utility_TestClass.prepareAccount('testaccount','Parent',null,true));
    ListAccount.add(Utility_TestClass.prepareAccount(null,null,tomObj.Id,true));
    Profile pf = [Select Id from Profile where Name = 'Standard User' limit 1];
    user u=Utility_TestClass.createUser('user7','USS',pf.Id,true);
    Contact contactTest =Utility_TestClass.prepareContact(null,null,null);
     Territory_Owner_Mapping__c tomObj2=Utility_TestClass.prepareTerritoryOwnerMapping(null); 
    ApprovalsFracEmailServiceClass emailTestObj = new ApprovalsFracEmailServiceClass();
    // call the method of email service class and test it with the data in the testMethod
    emailTestObj.handleInboundEmail(email, envelopForEmail);
    test.stoptest();
  }
  
  /*
  * @description :This is a test method for checkingTriggerForUrgent.
  * @return : Void.
  * @param :  N/A.
  *      
  */
  
  static testMethod void testCheckforurgentTrigger()
  {
  	Account aObj = new Account();
  	aObj.name='test';
  	insert aObj;
  	Contact cObj = new Contact();
  	cObj.lastname='testit';
  	cObj.accountid=aObj.id;
  	insert cObj;
  	Case caseObj = new Case();
  	caseObj.AccountId=aObj.id;
  	caseObj.Status='new';
  	caseObj.ContactId=cObj.id;
  	caseObj.Region__c='CANADA';
  	caseObj.Origin='Phone';
  	caseObj.RecordTypeId= Utility.SelectRecordType('Case',label.ES_RecordType);
  	caseObj.Is_Urgent__c=true;
  	insert caseObj;  
  	
  	/*Account aobj1 = new Account();
  	aobj1.name=Label.ES_AccountName;
  	insert aobj1;
  	Case case1obj= new Case();
  	case1obj.SuppliedEmail='aditi.garg@nov.com';
  	case1obj.SuppliedName='Garg, Aditi';
  	case1obj.RecordTypeId=Utility.SelectRecordType('Case',label.ETC_RecordType);
  	insert case1obj;
  	*/
   }
   
  
 
}