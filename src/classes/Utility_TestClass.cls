/*
###########################################################################
# Created by............:Megha Bhardwaj.
# Created Date..........:09/25/2014.
# Last Modified by......:Megha Bhardwaj.
# Last Modified Date....:02/12/2014.
# Description...........:This is a utility class for test class of ApprovalsFracEmailServiceClass.
#
# Change Log:
# S.No.   Date       Author         Description 
#  1   09/25/2014  Megha Bhardwaj  Initial Version
#
###########################################################################
*/ 
public class Utility_TestClass
{
    /*
    * @description :This function is creating a new test account.
    * @return : An instance of Account.
    * @param : Param 1 : Name of the account which is going to be created.
    *          Param 2 : Line of Business of the account which is going to be created.
               Param 3:  Id of the Territory Owner Mapping of the account which is going to be created.
               Param 4:  true/false.
    */
       public static Account prepareAccount(String accName,String LineofBusiness,Id tomId,Boolean isInsert){   
          if(accName==null)
              accName='TestAccount';
          if(LineofBusiness==null)
              LineofBusiness='TestLineOfBusiness';
          Account account = new Account(Name=accName,Line_of_Business__c=LineofBusiness,Territory_Owner_Mapping__c=tomId);
          if(isInsert)
              insert account;
          return account;
       }
       /*
       * @description :This function is creating a new test contact.
       * @return : An instance of contact.
       * @param : Param 1 : Id of the account related to the contact which is going to be created.
       *          Param 2 : last name of the contact which is going to be created.
                  Param 3:  first name of the contact which is going to be created.
       */
       public static Contact prepareContact(Id accountId,String lastName,String firstname){
           Id accId;
           if(accountId != null)
               accId = accountId;
           else
            {
                accId = prepareAccount('Test Account','TestLineofBusiness',prepareTerritoryOwnerMapping('TestTerritoryOwner').Id,true).Id;
            }
            if(lastName == null)
                lastName = 'Test Name';
            if(firstname == null)
                lastName = 'User';    
            Contact contact= new Contact(Accountid=accId,LastName = lastName,Firstname= firstname,Email=firstname+lastName+'@test.com');
            insert contact;
            system.debug('### contact : ' + contact);
            return contact;
    } 
    /*
    * @description :This function is creating a new test user.
    * @return : An instance of user.
    * @param : Param 1 : last name of the user which is going to be created.
    *          Param 2 : first name of the user which is going to be created.
               Param 3 : Id of the profile which is going to be assigned to the user.
               Param 4 :  true/false.
    */
    public static User createUser(string lastName, string firstName, id ProfileId,Boolean isInsert)
    {  
        User u = new User();
        u.LastName=lastName;
        u.FirstName=firstName;
        u.Alias = firstName.substring(0,1) + lastName.substring(0,3);
        u.Email = firstName + lastName + '@test.com';
        u.CommunityNickname = lastName;
        u.ProfileId = ProfileId;
        u.EmailEncodingKey='UTF-8';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US';
        u.TimeZoneSidKey='America/Los_Angeles';
        u.Username = firstName+lastName+'@test.com'+Label.ES_SandboxEmail;
        u.Username=u.Username.removeend('.');
        if(isInsert)
            insert u;
        return u;       
    }
    /*
    * @description :This function is creating a new test TerritoryOwnerMapping.
    * @return : An instance of TerritoryOwnerMapping.
    * @param : Param 1 : territory owner mapping name of the TerritoryOwnerMapping which is going to be created.
    *      
    */
     public static Territory_Owner_Mapping__c prepareTerritoryOwnerMapping(String territoryName){   
          if(territoryName==null)
              territoryName='TestTerritory';     
          Territory_Owner_Mapping__c t = new Territory_Owner_Mapping__c(Name=territoryName);
              insert t;
          return t;
     }
      /*
    * @description :This function is reading a static resource.
    * @return : A string containing the static resource content.
    * @param : Param 1 : name of the static resouce name.
    *      
    */
    public static string ReadStaticResource(String staticResourceName)
    {
        StaticResource sr = [Select  s.Name, s.Id, s.Body From StaticResource s  where name =:staticResourceName];
        blob tempBlob = sr <> null?sr.Body:null;
        String tempString = tempBlob <> null?tempBlob.toString().trim():null;
        system.debug('### tempString : ' + tempString);
        return tempString;   
    }
     /*
    * @description :This function is creating a new test case.
    * @return : An instance of case.
    * @param : Param 1 : Account's id of the case which is going to be created.
    *          Param 2 : Contact's Id of the case which is going to be created.
               Param 3 : Region  of the case which is going to be created.
               Param 4 : Case origin of the case which is going to be created.
               Param 5 : Status of the case which is going to be created.
               Param 6 :  true/false.
    */
    public static Case prepareCase(Id accId, Id conId, string region, string case_origin,string Status,String supldEmail,String SuppliedName, string subj, Boolean isInsert){   
          Case caseObj = new Case(AccountId=accId, ContactId=conId, Origin=case_origin,Region__c=region,Status=Status,SuppliedEmail=supldEmail,subject=subj);
          caseObj.RecordTypeId= Utility.SelectRecordType('Case',label.ES_RecordType);
          if(isInsert)
              insert caseObj;
          return caseObj;
    }

}