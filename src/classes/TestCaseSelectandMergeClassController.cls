/*
###########################################################################
# Created by............:Megha Bhardwaj.
# Created Date..........:12/02/2014.
# Last Modified by......:Aditi Garg/Megha Bhardwaj.
# Last Modified Date....:12/12/2014
# Description...........:This is a test class of CaseSelectandMergeClassController.
#
# Change Log:
# S.No.   Date       Author         Description 
#  1   12/02/2014   Megha Bhardwaj Initial Version
#
###########################################################################
*/ 
@isTest
private class TestCaseSelectandMergeClassController
{
   /*
    * @description :This is a test method.
    * @return : Void.
    * @param :  N/A.
    *      
    */
  static testMethod void TestCaseMergeMethod()
  {    
      Territory_Owner_Mapping__c tomObj=Utility_TestClass.prepareTerritoryOwnerMapping('TerritoryTestTest'); 
      Account accObj=Utility_TestClass.prepareAccount('testaccount','Parent',null,true);
      Contact contactTest =Utility_TestClass.prepareContact(accObj.Id,'contactlast','test');
      Case csObj=Utility_TestClass.prepareCase(accObj.Id,contactTest.Id,'testregion','testcaseorigin','teststatus','test.xxx@astadia.com','last, Test','test',true);
    
      list<case> lstCase=new list<case>();
      lstCase.add(Utility_TestClass.prepareCase(accObj.Id,contactTest.Id,'testregion','testcaseorigin','teststatus','test.xxx@astadia.com','last, Test','test', false));
      lstCase.add(Utility_TestClass.prepareCase(accObj.Id,contactTest.Id,'testregion','testcaseorigin','teststatus','test.xxx@astadia.com','last, Test','test', false));
      insert lstCase;
      
      CaseSelectandMergeClassController cObj=new CaseSelectandMergeClassController();
      cObj.SearchMethod();
      
      CaseSelectandMergeClassController.wrapCase cobjWrapperObj=new CaseSelectandMergeClassController.wrapCase(csObj);
      
      cObj.selectedSubject='';
      cObj.selectedAccName='';
      cObj.SearchMethod();
      
      cObj.selectedAccName=accObj.name;
      cObj.selectedSubject='test';
      cObj.SearchMethod();
      
      cObj.masterRecordId=csObj.id;
      cobjWrapperObj.selected=true;
      cObj.wrapCaseList.add(cobjWrapperObj);
      cObj.wrapCaseList.get(0).selected = true;
      cObj.selectedCases = new list<Case>();
      cObj.selectedCases.add(lstCase.get(0));
      cObj.processSelected();
      
      
      cObj.MergeCases(csObj.id,lstCase);
      cObj.refreshPage();
      cObj.setMasterRecord();
      
      cObj.MergeCases(lstCase.get(0).id,lstCase);
      cObj.wrapCaseList.get(0).selected = true;
      cObj.wrapCaseList.get(1).selected = true;
      cObj.processSelected();
  }
}