public with sharing class CreditRebillController {

	public List<Credit_Rebill__c> Tasks;
    public Case csRecord {get;set;}
    public List<String> CRIds;
    public List<Schema.FieldSetMember> crFields{get;set;}
    public List<WrapperClassEx> WrapperList{get;set;}
    public List<Credit_Rebill__c> lstCRBill { get; set; }
    
    
	public CreditRebillController(ApexPages.StandardController stdController){
        this.csRecord = [Select id, AccountId, ContactId, Customer_Preference__c, Credit_Reason_Code__c,Credit_Reason_Summary_Code__c, Rebill_Comments__c from Case where Id =: stdController.getId()];
        //system.debug('constructor --->');
       	//objActn = new Action__c();
       	loadData(); 
    }
    
    public String tabId{get; set{
            tabId = value;
            System.debug('value 1: '+value);
    }}
    public String plus{get; set{
            plus = value;
            System.debug('value 2: '+value);
    }}
    public String minus{get; set{
            minus = value;
            System.debug('value 3: '+value);
    }}
    
    public String meetingId{get; set{
            meetingId = value;
            System.debug('value 4: '+value);
    }}
    
    public String aId{get; set{
            aId = value;
            System.debug('value 5: '+value);
    }}
    
    
    private void loadData()
    {
    	this.lstCRBill = getCreditRebills();
        //this.entryFields = getTaskFields();
        this.crFields = getCRFields();
        
        WrapperList = New List<WrapperClassEx>();
        CRIds = new List<String>();
        for(Credit_Rebill__c cr : this.lstCRBill ){
            CRIds.add(cr.Id);
        }
        
        system.debug('lstCRBill--->' + this.lstCRBill);
        Integer a = 1;
        for(Credit_Rebill__c cpmA : this.lstCRBill ){
            String tabId = String.valueof(a);
            System.debug('tabId ---> ' + tabId);
            
            WrapperList.add(New WrapperClassEx(cpmA, tabId));
            a++;
        }
    }
    
    public List<Schema.FieldSetMember> getCRFields() {
        return SObjectType.Credit_Rebill__c.FieldSets.MassCreate.getFields();
    }
     
    private List<Credit_Rebill__c> getCreditRebills() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getCRFields()) {
            query += f.getFieldPath() + ', ' ;
        }
        query += 'Id FROM Credit_Rebill__c Where Case__c = \'' + this.csRecord.Id + '\'';
			        
        system.debug('Query' + query);
        return Database.query(query);
    }
    
    
    public class WrapperClassEx{
        public Credit_Rebill__c CRfieldObj{get;set;}
        public String tabId{get;set;}
        public Boolean flag{get;set;}
        public Integer originalAmount {get;set;}
    
        public WrapperClassEx(Credit_Rebill__c CRrecord, String tab){
            CRfieldObj = CRrecord;
            tabId = tab;
            flag = true;
        }
    }
    
    public void createNewAction(){
    	system.debug('WrapperList ---> ' + WrapperList);
        Integer size = WrapperList.size() + 1;
        String tabId = string.valueof(size);
        System.debug('tabId Create--->' + tabId);
        WrapperList.add(New WrapperClassEx(New Credit_Rebill__c(Case__c = this.csRecord.Id), tabId));
    }
    
    public PageReference Save(){
    	update csRecord;
        PageReference projectPage = new PageReference('/apex/Credit_Rebill?id=' + this.csRecord.Id);
        projectPage.setRedirect(true);
        return projectPage;
    }
    
    public PageReference SaveActionChanges(){
        saveAction();
        PageReference projectPage = new PageReference('/apex/Credit_Rebill?id=' + this.csRecord.Id);
        projectPage.setRedirect(true);
        return projectPage;
    }
    
     public PageReference SaveCloseActionChanges(){
        saveAction();
        PageReference projectPage = new ApexPages.StandardController(this.csRecord).view();
        projectPage.setRedirect(true);
        return projectPage;
    }
    
    private void saveAction()
    {
    	List<Credit_Rebill__c> cpmUpdatedList = new List<Credit_Rebill__c>();
        List<Credit_Rebill__c> actionToDelete = new List<Credit_Rebill__c>();
        Set<ID> actionsDeleted = new Set<ID>();
        
        for(WrapperClassEx wc: WrapperList){
        	
        	system.debug('Flag ---> ' + wc.flag);
            if(wc.flag == false){
                if(wc.CRfieldObj.Id != null){
                    actionToDelete.add(wc.CRfieldObj);
                    actionsDeleted.add(wc.CRfieldObj.Id);
                    system.debug('actionToDelete ---> ' + wc.CRfieldObj);
                }
            }else{
                cpmUpdatedList.add(wc.CRfieldObj);
            }
        }
        system.debug('cpmUpdatedList   ----> ' + cpmUpdatedList);
        upsert cpmUpdatedList;
        system.debug('actionToDelete   ----> ' + actionToDelete);
        database.delete(actionToDelete);
    }
    
    public PageReference cancelChanges(){
        PageReference projectPage = new ApexPages.StandardController(this.csRecord).view();
        projectPage.setRedirect(true);
        return projectPage;
    }

    public void deleteAction(){
        String actionId = aId;
        for(WrapperClassEx wc: WrapperList){
            if(actionId == wc.tabId){
                wc.flag = false;
            }
        }
    }
}