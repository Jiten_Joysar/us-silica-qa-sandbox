@isTest(seeAllData=true)
public class Test_QuoteCloneWithItemsController {
    
    static testMethod void testQuoteCloneController() {

       // setup a reference to the page the controller is expecting with the parameters
        PageReference pref = Page.clonebutforQuote;
        Test.setCurrentPage(pref);
        
        Profile P = [Select Id from Profile where Name = 'System Administrator'];
        User us = new User();
        us.Alias = 'standt1';
        us.Email= 'bharaj@yahoo.com';
        us.EmailEncodingKey='UTF-8';
        us.FirstName = 'Bharaj';
        us.LastName = 'Raj';
        us.LanguageLocaleKey= 'en_US';
        us.LocaleSidKey='en_US';
        us.TimeZoneSidKey='Asia/Kolkata';
        us.UserName='bharaj@gmail.com';
        us.ProfileId = P.Id;
        
        Insert us;

        
        Account act1 = new Account();
        act1.Name = 'testAcc1';
        act1.City__c = 'bengalore';
        act1.Shipping_Street_1__c = '#2, 6th cross';
        act1.Shipping_Street_2__c = 'Dayanand Layout';
        act1.Country__c = 'US';
        act1.State__c = 'OH';
        act1.Zip_Postal_Code__c = '44136';
        
       
        insert act1;
        
        Account act = new Account();
        act.Name = 'testAcc';
        act.Pricing_Type_CC13__c = 'LST - List Pricing';
        act.Shipping_Street_1__c = '#2, 6th cross';
        act.Shipping_Street_2__c = 'Dayanand Layout';
        act.City__c = 'STRONGSVILLE';
        act.State__c = 'OH';
        act.Zip_Postal_Code__c = '44136';
        act.Country__c = 'US';
        act.Bill_To__c = act1.id;
        insert act;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'testopp';
        opp.AccountId = act.id;
        opp.Alternate_Mailing_Address_1__c = 'wwqeqeqwe';
        opp.Alternate_Mailing_Address_2__c = 'sadadddd';
        opp.Alternate_City__c = 'Silica';
        opp.Alternate_State__c = 'Iowa';
        opp.Alternate_Zip_Code__c = '123456';
        opp.StageName = 'Open';
        opp.CloseDate = system.today()+5;
        Insert opp;
        
        Pricebook2 standard = [Select Id, Name, isActive From Pricebook2 where IsStandard = true] ;
        if (!standard.isActive) {
            standard.isActive = true;
            update standard;
        }
                
        Pricebook2 pb = new Pricebook2();
        pb.IsActive=true;
        pb.Name = 'testBook';
        insert pb;
        
        case cs = new case();
        cs.Payable_Rate_per_Ton__c = 12345.67;
        cs.Average_Billable_Rate__c = 2345.23;
        insert cs;
        
        Product2 pd = new Product2();
        pd.Name = 'testProduct';
        pd.Bags_per_Pallet__c = '1 BAGS';
        pd.Branch_Plantfromaccount__c = act.id;
        pd.Unit_Cost__c = 123;
        pd.ProductCode = '123';
        pd.Package_Type__c = 'Anti-Static';
        pd.Product_Group__c = '#17';
        pd.Transload_Premium__c = 1234567891012345.10;
        pd.Unit_of_Measure__c = 'BG';
        insert pd;  
        
        PricebookEntry pbe1 = new PricebookEntry();
        pbe1.Pricebook2Id = standard.id;
        pbe1.Product2Id = pd.id;
        pbe1.UnitPrice = 122345.23;
        pbe1.IsActive= True;
        insert pbe1;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = pb.id;
        pbe.Product2Id = pd.id;
        pbe.UnitPrice = 122345.23;
        pbe.IsActive= True;
        
        insert pbe;
        
       
        
        Quote qc = new Quote();
        qc.Name = 'testQuote';
        qc.Pricebook2Id = pb.id;
        //qc.AccountId = act.id;
        qc.OpportunityId = opp.Id;
        qc.Quote_Type__c = 'Delivered';
        qc.Quote_Rejected_Reason_Code__c = 'Failed Testing';
        qc.Associated_Rate_Request_Case__c = cs.Id;
        qc.Quote_Reason__c = 'Extension';
        qc.Effective_Date__c = date.today();
        qc.ExpirationDate = date.today().adddays(5);
        //qc.Branch_Plant__c =act.id;
        qc.Quantity__c = '15';
        qc.Lead_Time__c = 'tetstete';
        qc.Minimum_Quantity__c = '12';
        qc.Billable_Freight_Amount__c = 32141.12;
        qc.Car_Charges__c =  32141.12;
        qc.Car_Charge_Quote_Options__c = 'Roll up into price';
        qc.Mode_of_Transportation__c = 'CNT - Container';
        qc.Business_Secured__c = 'Yes';
        qc.Freight_Mark_Up__c = 12345.23;
        qc.Fuel_Surcharge_Mark_Up__c = 12345.23;
        insert qc;
        
        
        QuoteLineItem ql = new QuoteLineItem();
        ql.Branch_Plantlookup__c = act.id;
        //ql.CreatedById = us.id;
       // ql.CreatedDate = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        ql.Delivered_Price_per_TonOG__c = 1234567891012345.10;
        ql.Description = 'qsdweeewe';
        ql.Discount = 12.20;
        //ql.IsDeleted = False;
        //ql.LineNumber = '1234';
        //ql.ListPrice = 1234567891012345.12;
        ql.LTL_Upcharge__c = 12.20;
        ql.Material_Price__c = 1234567891012345.12;
        ql.MISC_Charge__c = 12345.23;
        ql.Overtime__c = 12345.23;
        ql.Pallet_Charge__c = 12345.23;
        ql.PricebookEntryId = pbe.id;
        ql.Product2Id = pd.id;
        ql.Product_Mark_Up__c = 12345.23;
        ql.Quantity = 12;
        ql.QuoteId = qc.id;
        ql.Quote_Type__c = 'FOB';
        ql.ServiceDate = date.today();
        ql.Shrink_Wrap__c = 12345.23;
        ql.Shroud__c = 12345.23;
        //ql.SortOrder = 1;
        //ql.SystemModstamp = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        ql.UnitPrice = 123.12;
        insert ql;     
        
        // Construct the standard controller
        ApexPages.StandardController con = new ApexPages.StandardController(qc);
        
        // create the controller
        QuoteCloneWithItemsController ext = new QuoteCloneWithItemsController(con);
        
        Test.startTest();
        PageReference ref = ext.cloneWithItems();
       
		
        List<QuoteLineItem> newItems = [Select p.Id From QuoteLineItem p where Quoteid = :qc.id];
             
        
        Test.stopTest();

    }    

}