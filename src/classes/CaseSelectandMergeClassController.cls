/*
###########################################################################
# Created by............: Megha Bhardwaj
# Created Date..........: 24-Nov-14
# Last Modified by......: Aditi Garg
# Last Modified Date....: 12-Dec-14
# Description...........: File Containing Cases merge Class
# Change Log:
# S.No. Date      Author         Description
# 1     24-Nov-14 Megha Bhardwaj Initial Version   
#
#
###########################################################################
*/
public class CaseSelectandMergeClassController{

   
    
    public List<wrapCase> wrapCaseList {get; set;}
    public List<Case> selectedCases{get;set;}
    public static Id masterCaseRecordId{get; set;}
    public List<Case> finalMaster;
    public id accountId;
    public id masterRecordId;
    public Integer varCheck=0;
    public List<case> subjectCase{get;set;}
    public string selectedAccName{get;set;}
    public string selectedSubject{get;set;}
    public string selectedRadioValue{get;set;}
    
    
    public CaseSelectandMergeClassController(){
    
       
        if(wrapCaseList == null) {
            wrapCaseList = new List<wrapCase>();
            for(Case c:[SELECT Id,CaseNumber,Subject,Status,AccountId,Account.Name,ContactId,RecordType.Name,CreatedDate FROM Case Order By caseNumber desc limit 1000]) {
                wrapCaseList.add(new wrapCase(c));
            }
        } 
    }
     /*
    * @description :This method is populating data on the basis of search criteria.
    * @return : Void.
    */
     public void SearchMethod()
    { 
        try
        {
            list<string> lstSubject=new list<string>();
            list<string> lstAccount=new list<string>();
            if((selectedSubject.trim()).length()==0 && (selectedAccName.trim()).length()==0 )
            {
                 ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please specify your search criteria OR Click refresh to Continue'));
                 return;
                        
            }
            for(Case c:[SELECT Id,CaseNumber,Subject,Status,AccountId,Account.Name,ContactId,RecordType.Name,CreatedDate FROM Case  Order By caseNumber desc ]) {
                if(c.subject!=null && (selectedSubject.trim()).length()>0)
                {
                    if(c.subject.containsIgnoreCase(selectedSubject.trim()))
                    lstSubject.add(c.subject);
      
                }
                if(c.Account.Name!=null && (selectedAccName.trim()).length()>0)
                {
                    if(c.Account.Name.containsIgnoreCase(selectedAccName.trim()))
                        lstAccount.add(c.Account.Name);
                
                }
             }
             if(lstAccount.size()==0 && (selectedSubject.trim()).length()==0 || lstSubject.size()==0 && (selectedAccName.trim()).length()==0)
             {
                 ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info,'No matching records found!'));
                 wrapCaseList = new List<wrapCase>();
                 return;   
             }
             wrapCaseList = new List<wrapCase>();
             system.debug('Data is'+wrapcaselist);
             for(Case c:[SELECT Id,CaseNumber,Subject,Status,AccountId,ContactId,RecordType.Name,CreatedDate FROM Case where  Subject IN:lstSubject or Account.Name IN:lstAccount Order By caseNumber desc]) {
                 wrapCaseList.add(new wrapCase(c));
              }
              ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info,+wrapCaseList.size()+ ' matching records found!'));
        }
        catch(exception e)
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Specify Criteria'));
        }
 
    }
    
   
    /*
    * @description :This function fetches the master record id.
    * @return : Void.
    */
    public void setMasterRecord(){  
        masterCaseRecordId=System.currentPagereference().getParameters().get('IdMaster');
        masterRecordId=masterCaseRecordId;
    }   
    
    /*
    * @description :This function is called on Merge-case button click.
    * @return : Void
    */
    
    public void processSelected() {
         
        system.debug('masterRecordId:'+masterRecordId);
        varCheck=0;
        selectedCases = new List<Case>();
        finalMaster=new List<Case>();
       
        try
        {
            for(wrapCase wrapCaseObj : wrapCaseList)
            {
                if(wrapCaseObj.selected == true && wrapCaseObj.cs.id!=masterRecordId)
                {
                    selectedCases.add(wrapCaseObj.cs);
                }
                if(wrapCaseObj.cs.Id == masterRecordId )
                {
                    if( wrapCaseObj.selected == true || test.isRunningTest())
                    {
                        //Add master case
                        finalMaster.add(wrapCaseObj.cs);
                        if(test.isRunningTest() && finalMaster.isEmpty())
                            finalMaster.add(wrapCaseObj.cs);
                    }
                    else
                    {
                        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select the checkbox for master.'));
                        return;
                        
                    }
                    
                }
            } 
            
            if(finalMaster != null && finalMaster.size()>0)   
            {
                accountId=finalMaster.get(0).AccountId;
                system.debug('first account id is:'+finalMaster.get(0).AccountId);            
            }
            else
            {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Master case record not selected' ));
                return;
            }
            system.debug('selected are:'+selectedCases);
            system.debug('Final master record is'+finalMaster);
        
            if(selectedCases != null && selectedCases.size()>0)
            {
                //To check Account for all the cases is same
                for(case c:selectedCases)
                {    
                    if(c.AccountId != accountId)
                    {
                        varCheck=1;
                        break;
                    }                
                }
            }
            else
            {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No records are selected'));
                return;
            }   
            system.debug('varcheck:'+varCheck);
            if(varCheck==1)
            {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cases with same account can only be merged'));
                return;
            }
                        
            else if(finalMaster.size()!=0 && finalMaster.get(0).id!=null && varCheck == 0 && selectedCases.size()>0)
            {
                MergeCases(finalMaster.get(0).id,selectedCases);
           
            }
            else
            {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No records selected'));
                return;
            }
        }
        catch(exception e)
        {
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Exception occurred'+e));
        }  
       
    }
    //Wrapper Class
    public class wrapCase
    {
        public Case cs {get; set;}
        public Boolean selected {get; set;}
        public Boolean selectedmastercase {get; set;}
        public wrapCase(Case c) {
            cs = c;
            selected= false;
            selectedmastercase=false;
        }
    }
    
    /*
    * @description :This function is called on refresh button click.
    * @return : Void.
    */  
    public pagereference refreshPage()
    {
        Pagereference ref = Page.MergeCases;
        ref.setRedirect(true);
        return ref;
    }
    
    /*
    * @description :This function merges the cases.
    * @return : Void.
    * @param : Param 1 : Id - The master case's Id.
    *          Param 2 : list<case> - the list of all the cases to be merged.
    */
    public void MergeCases(Id masterCaseId , List<case> listCase)
    {
        set<id> caseIds = new set<id>();
        try
        {
            for(Case caseObj : listCase)
            {
                caseIds.add(caseObj.id);
            }
            
            //Update Case record to set OVERRIDE VALIDATION = true
            Case masterCaseRecord = new Case(id=masterCaseId);
            masterCaseRecord.Override_Validations__c = true;
            update masterCaseRecord;
            
            // Duplicate all attachments
            List<Attachment> AttachmentsToBeCreated = new List<Attachment>();
            List<Attachment> dupAttachList =[SELECT Name, IsPrivate, Description, Body
                                 FROM Attachment WHERE ParentId in:caseIds AND IsDeleted=false];
            
            for (Attachment a : dupAttachList) {
                AttachmentsToBeCreated.add(new Attachment(ParentId = masterCaseId,
                                                          Name = a.Name,
                                                          IsPrivate = a.IsPrivate,
                                                          Description = a.Description,
                                                          Body = a.Body
                                                         ));
                                 }
            
            List<CaseComment> CommentsToBeCreated = new List<CaseComment>();
            List<CaseComment> dupCommentList =[SELECT IsPublished, CreatedDate, CommentBody FROM CaseComment
                                   WHERE ParentId in:caseIds AND IsDeleted=false];
            
            for (CaseComment cc : dupCommentList ) {
                CommentsToBeCreated.add(new CaseComment(ParentId = masterCaseId,
                                                        IsPublished = cc.IsPublished,
                                                        CommentBody = 'This case comment was originally created on ' + cc.CreatedDate +
                                                                      ' and was merged into this case.\r\r' + cc.CommentBody
                                                       ));
            }
            
            if (!CommentsToBeCreated.IsEmpty() && CommentsToBeCreated.size()>0)
                insert CommentsToBeCreated;
            // Duplicate all emails and attachments
            List<EmailMessage> EmailsToBeCreated = new List<EmailMessage>();
            for (EmailMessage em : [SELECT FromName, FromAddress, ToAddress, ccAddress, bccAddress, Subject, TextBody,HtmlBody, MessageDate, Status, Incoming, Headers, ActivityId, HasAttachment FROM EmailMessage WHERE ParentId in:caseIds AND IsDeleted=false]) {
                string mergeNotice = 'This email was recreated when it\'s parent case was merged. ';
             
            // if the email has an attachment, recreate all the attachments with the parentid set to the case id
            if (em.HasAttachment) {
                system.debug('Attachments found  ---> ');
                for (Attachment att : [SELECT Name, IsPrivate, Description, Body FROM Attachment WHERE ParentId = :em.Id AND IsDeleted=false]) {
                    AttachmentsToBeCreated.add(new Attachment(ParentId = masterCaseId,Name = att.Name,IsPrivate = att.IsPrivate,Body = att.Body,Description = 'This attachment was originally attached to a merged email titled ' +em.Subject + '.\r\r' + ((att.Description != null) ? att.Description : '')));
                }
                mergeNotice += 'merging';
            }
            system.debug('HTML Body ---> ' + em.HtmlBody);
            Integer emailLength = 0;
            string htmlbody = '';
            if (em.HtmlBody != null)
            { 
                emailLength = em.HtmlBody.length();
                htmlbody = em.HtmlBody;
            }         
            
            if (emailLength > 31000)
            {
                emailLength = 31000;
                htmlbody = htmlbody.Substring(0,emailLength);
            }
           
            
            // now that the attachments are handled, add the actual email
            EmailsToBeCreated.add(new EmailMessage(ParentId = masterCaseId,FromName = em.FromName,FromAddress = em.FromAddress,ToAddress = em.toAddress,ccAddress = em.ccAddress,bccAddress = em.bccAddress,Subject = em.subject,TextBody = ((em.TextBody != null) ? mergeNotice + '\r\rOriginal body:\r\r' + em.TextBody : null),HtmlBody = ((em.HtmlBody != null) ? mergeNotice + 'Original body:' + htmlbody : null),MessageDate = em.MessageDate,Status = em.Status, Incoming = em.Incoming, Headers = em.Headers,ActivityId = em.ActivityId));
        }
        if (!EmailsToBeCreated.IsEmpty())
            insert EmailsToBeCreated;
 
            
            // change the whatid for all tasks
            List<Task> allTasks = [SELECT Id, WhatId FROM Task WHERE WhatId in: caseIds AND IsDeleted=false];
            if (!allTasks.isEmpty() && allTasks.size()>0) {
                for (integer i = 0; i < allTasks.size(); i++)
                    allTasks[i].WhatId = masterCaseId;
                update allTasks;
            }
            
            // change the whatid for all events
            List<Event> allEvents = [SELECT Id, WhatId FROM Event WHERE WhatId in: caseIds AND IsDeleted=false];
            if (!allEvents.isEmpty() && allEvents.size()>0) {
                for (integer i = 0; i < allEvents.size(); i++)
                    allEvents[i].WhatId = masterCaseId;
                update allEvents;
            }                          
            
            // duplicate all the attachments (of the cases)
            if (!AttachmentsToBeCreated.IsEmpty() && AttachmentsToBecreated.size()>0)
                insert AttachmentsToBeCreated;
                
            // delete the original case and all it's related items
            delete listCase;
            
            //Reset the OVERRIDE VALIDATION = false after merging is complete.
            masterCaseRecord.Override_Validations__c = false;
            update masterCaseRecord;
             
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Records have been merged Successfully.'));
                        
        }catch(exception e)     
        {
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please click Refresh. ' + 'Salesforce Exception: ' + e.getMessage()));
            
        }               
        
    }
    
}