@IsTest
public class TestMiscTriggerTests {

   public static testMethod void testoppclosed () {
   
   Account a = new account(name='test accout', Status__c = 'prospect');
   
   insert a;
   
   Opportunity Opp = new Opportunity(name= 'testopp', accountid= a.id, stagename = 'Closed Won', closedate=date.today(), Tech_Support_Needed__c='test');
   insert Opp;
   
   }
   
   public static testMethod void testSalesOrderMisc () {
   
   Account a = new account(name='test accout', Status__c = 'prospect');
   insert a;
   
   Sales_Order__c SO = new Sales_Order__c(Ship_to_Account__c = a.id);
   insert SO;
   
   Sales_Order_Line_Item__c SOLI = new Sales_Order_Line_Item__c(Sales_Order__c = SO.id);
   insert SOLI;
   
   }
   
   }