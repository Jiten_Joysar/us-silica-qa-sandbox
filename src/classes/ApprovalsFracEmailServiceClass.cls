/*
###########################################################################
# Created by............: Aditi Garg
# Created Date..........: 18-Sep-14
# Last Modified by......: Aditi Garg
# Last Modified Date....: 24-Sep-14
# Description...........: File Containing Email Service Class
# Change Log:
# S.No. Date      Author     Description
# 1     18-Sep-14 Aditi Garg Initial Version   
#
#
###########################################################################
*/
 global class ApprovalsFracEmailServiceClass implements Messaging.InboundEmailHandler{
    /*
    * @description : Function to send mail and initiate approval process
    * @return : Messaging.InboundEmailResult
    * @param : email : Inbound mail body 
    * envelope : Stores the to and from address of the Inbound mail
    */  
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope){
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Case objEmailCase = new case();
        System.debug('from Address'+email.FromAddress);
         System.debug('from Address'+envelope);       
        //Call to Helper class for creation of Case record         
        Database.DMLOptions objDmlOpts = new Database.DMLOptions();
        
        try{
            ApprovalsFracEmailServiceHelper.PopulateCaseDetails(objEmailCase,email,envelope);     
            //Assignment Rule Call Initiated and send email     
            objDmlOpts.assignmentRuleHeader.useDefaultRule = true;
            objDmlOpts.EmailHeader.triggerAutoResponseEmail = true;
            objDmlOpts.EmailHeader.TriggerUserEmail = true;
            objEmailCase.setOptions(objDmlOpts);
          
            // Insertion of Case Record
            insert objEmailCase;
          
            //Approval process Initiated
            Approval.ProcessSubmitRequest objApprovalRequest= new Approval.ProcessSubmitRequest();
            objApprovalRequest.setComments('Submitted for approval. Please approve.');
            objApprovalRequest.setObjectId(objEmailCase.id);
            System.debug('##approvalRequest'+objApprovalRequest);
            Approval.ProcessResult resultProcess = Approval.process(objApprovalRequest);
            System.debug('##Submitted for approval successfully: '+resultProcess.isSuccess());
            result.success=true;
            return result;
        }
        
        catch(Exception en){
            result.success=false;
            System.debug('##errormessage'+en.getMessage());
            return result;
        }
    }
}