@IsTest (seealldata=TRUE)
public class TestUpdateOwnerOftheAccountTrigger {

   public static testMethod void TestUpdateOwnerOftheAccountTrigger () {
  
        
        Id rtSId = [ SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'ShipTo'].Id;
        Id rtBId = [ SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'BillTo'].Id;        
       
        
        List<User> userList = [SELECT Id FROM User WHERE IsActive = true LIMIT 2];
        
        Territory_Owner_Mapping__c tom = new Territory_Owner_Mapping__c(
            Name = 'Northeast', Inside_Sales_Rep__c = userList[0].Id, Regional_Sales_Manager__c = userList[1].Id);
        tom.Threshold__c = 6000;
        tom.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
        insert tom; 
        
        Territory_Owner_Mapping__c tom1 = new Territory_Owner_Mapping__c(
            Name = 'west', Inside_Sales_Rep__c = userList[0].Id, Regional_Sales_Manager__c = userList[1].Id);
        tom1.Threshold__c = 6000;
        tom1.Line_of_Business__c = 'Oil And Gas';
        insert tom1; 
        
         Territory_Owner_Mapping__c tom2 = new Territory_Owner_Mapping__c(
            Name = 'Northeast1', Inside_Sales_Rep__c = userList[1].Id, Regional_Sales_Manager__c = userList[0].Id);
        tom2.Threshold__c = 6000;
        tom2.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
        insert tom2;  
        
         Territory_Owner_Mapping__c tom3 = new Territory_Owner_Mapping__c(
            Name = 'west3', Inside_Sales_Rep__c = userList[0].Id, Regional_Sales_Manager__c = userList[1].Id);
        tom3.Threshold__c = 1000;
        tom3.Line_of_Business__c = 'Oil And Gas';
        insert tom3;
        
        Account testAccount = new Account(Name = 'Test Account1');
        testAccount.AnnualRevenue = 1000;
        testAccount.RecordTypeId = rtSId;
        testAccount.ShippingState = 'Alabama';
        testAccount.ShippingCountry = 'USA';
        testAccount.Line_of_Business__c = 'Industry and Specialty Products (ISP)';
        testAccount.National_Account__c = false;
        testAccount.Account_Ownership_Override__c = false;
        testAccount.Territory_Owner_Mapping__c = tom.Id;
      
        Insert testAccount;   
        
        Account testAccount1 = new Account(Name = 'Test Account2');
        testAccount1.AnnualRevenue = 1000;
        testAccount1.RecordTypeId = rtSId;
        testAccount1.ShippingState = 'Alabama';
        testAccount1.ShippingCountry = 'USA';
        testAccount1.Line_of_Business__c = 'Oil and Gas';
        testAccount1.National_Account__c = false;
        testAccount1.Account_Ownership_Override__c = false;
        testAccount1.Territory_Owner_Mapping__c = tom.Id;
      
        Insert testAccount1;   
       
        Account[] ac = [ SELECT Id, CountryState__c FROM Account WHERE Id =: testAccount1.Id OR ID =:testAccount.ID ]; 
        System.debug(':::Formula field value:'+ac);
       
        Territory_Mapping__c tm = new Territory_Mapping__c();
        tm.Country__c = 'US';
        tm.ISP_Territory_Name__c = tom.Id;
        tm.O_G_Territory__c = tom1.Id;
        tm.State__c = 'Alabama';
        insert tm;
        
        Territory_Mapping__c tm1 = new Territory_Mapping__c();
        tm1.Country__c = 'Canada';
        tm1.ISP_Territory_Name__c = tom.Id;
        tm1.O_G_Territory__c = tom1.Id;       
        insert tm1;
        
        Account testAccount2 = new Account(Name = 'Test Account2');
        testAccount2.AnnualRevenue = 1000;
        testAccount2.RecordTypeId = rtSId;
        testAccount2.ShippingState = 'Alabama';
        testAccount2.ShippingCountry = 'USA';
        testAccount2.Line_of_Business__c = 'Oil and Gas';
        testAccount2.National_Account__c = false;
        testAccount2.Account_Ownership_Override__c = false;
        testAccount2.Territory_Owner_Mapping__c = tom.Id;
        testAccount2.Territory_Mapping__c = tm1.Id;      
        Insert testAccount2; 
        
        tm1.ISP_Territory_Name__c = tom2.Id;
        update tm1;
        
        tm1.ISP_Territory_Name__c = tom2.Id;
        update tm1;
        
        tm1.O_G_Territory__c = tom3.Id;
        update tm1;
        
       
       
       
    }
}