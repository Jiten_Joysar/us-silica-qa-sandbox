/*
###########################################################################
# Created by............: Aditi Garg
# Created Date..........: 18-Sep-14
# Last Modified by......: Aditi Garg
# Last Modified Date....: 24-Sep-14
# Description...........: File Containing Utility Methods and static Variables
# Changes added on .....: 26-May-16
# Changes done By ......: Jiten Joysar
#
# Change Log:
# S.No. Date      Author     Description
# 1     18-Sep-14 Aditi Garg Initial Version
# 2		26-May-16 Jiten Joysar Added Static Variable to Avoid recursion of trigger
#
###########################################################################
*/
public class Utility
{
	/*Static Variable To avoid recursion of trigger in single Apex transaction.*/
	public static boolean ISP_order_Received_Email = true;
	public static boolean ISP_Pending_Shipment_Carrier_Reminder = true;
	public static boolean ISP_Pending_Shipment_1st_Acknowledgement = true;
    public static boolean ISP_Pending_Shipment_2nd_Acknowledgement = true;
    public static boolean ISP_Pending_Shipment_3rd_Acknowledgement = true;
    /*
    * @description : Function to Select Record Type
    * @return : Id
    * @param : sObj : sObject type
    * recordTypeName : Name of the Record Type
    */  
    public static Id SelectRecordType(string sObj,string recordTypeName)
    {        
         Schema.DescribeSObjectResult objSchemaDesc = Schema.getGlobalDescribe().get(sObj).getDescribe(); 
         Map<string,schema.RecordTypeInfo> mapRecType =objSchemaDesc.getRecordTypeInfosByName();
         System.debug('##'+mapRecType);
         Schema.RecordTypeInfo objRecTypeName = mapRecType.get(recordTypeName);
         System.debug('##'+objRecTypeName.getRecordTypeId());
         return objRecTypeName.getRecordTypeId();   
    }
    public static Boolean CheckForNullList(list<Object> listData)
    {       
         
         return (listData!=null && listData.size()>0)? true:false;
            
    }

}