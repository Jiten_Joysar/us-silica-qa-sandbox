/**
* Handler Class     : checkForUrgentCaseTriggerHandler
* Created by        : ETMarlabs (30-05-2016)
* Version           : 1.0
* Description       : Trigger Handler contains all the Business logic for checkForUrgentCase trigger.
*                   : Inline comments are added to get specific details.
**/
public with sharing class checkForUrgentCaseTriggerHandler {
    
    /*Variable declaration */
    public List<case> newCaseList;
    public List<case> oldCaseList;
    public map<id,case> newCaseMap;
    public map<id,case> oldCaseMap;
    public List<Messaging.SingleEmailMessage> messageList ;
    /*Default Constructor*/
    public checkForUrgentCaseTriggerHandler(){
        //nothing
    }
    /*parameterized constructor*/
    public checkForUrgentCaseTriggerHandler(List<case> newList , List<case> oldList, map<id,case> newMap , map<id,case> oldMap){
        /*
        newCaseList = new List<case>();
        oldCaseList = new List<case>();
        newCaseMap = new map<id,case>();
        oldCaseMap = new map<id,case>();
        messageList = new List<Messaging.SingleEmailMessage>();
        if(newList != null)
            newCaseList = newList;
        if(oldList != null)
            oldCaseList = oldList; 
        if(newMap != null)
            newCaseMap = newMap;
        if(oldMap != null)
            oldCaseMap = oldMap;
        */    
        newCaseList = (newList != null && !newList.isEmpty())? newList : new List<case>();
        oldCaseList = (oldList != null && !oldList.isEmpty())? oldList : new List<case>();
        newCaseMap = (newMap != null && !newMap.isEmpty())? newMap : new map<id,case>();
        oldCaseMap = (oldMap != null && !oldMap.isEmpty())? oldMap : new map<id,case>(); 
        messageList = new List<Messaging.SingleEmailMessage>();  
    }
    /*
    * Method Name   : afterUpdateBusinessLogic
    * Description   : Method contains all the business logic for after update Trigger.     
    * @param        : None
    * @returns      : None
    */
    public void afterUpdateBusinessLogic(){
        List<ID> carrierAccount = new List<ID>();
        List<ID> userIds = new List<ID>();
        List<ID> queueIds = new List<ID>();
        for(Case caseRec : newCaseList)
        {       
            if(caseRec.Carrier_Name__c != null)
                carrierAccount.add(caseRec.Carrier_Name__c);
            if(String.valueOf(caseRec.ownerID).substring(0,3) == '00G') 
                queueIds.add(caseRec.ownerID);
            else
                userIds.add(caseRec.ownerID);
        }
        System.debug('carrierAccount '+carrierAccount);
        map<ID,Account> mapOfIdAndCarrierAccount ;
        map<ID,User> mapOfIdAndUser ;
        map<ID,Group> mapOfIdAndGroup ;
        if(userIds.size()>0)
            mapOfIdAndUser = new map<ID,User>([select id,Email from user where ID IN :userIds]);
        if(queueIds.size()>0)   
            mapOfIdAndGroup = new map<ID,Group>([select Id,Email from Group where Type = 'Queue' and ID IN:queueIds]);
        if(carrierAccount.size()>0){
            mapOfIdAndCarrierAccount = new Map<ID,Account>([Select id,(select id,Email from contacts) from Account where id IN : carrierAccount]);
        }    
        map<case,List<String>> mapOfCaseAndListOfCCAddress = new map<case,List<String>>();
        String allTheRecordType = Label.Record_Type_for_Order_Acknowledgement_Email;
        Map<ID,Schema.RecordTypeInfo> rt_Map = case.sObjectType.getDescribe().getRecordTypeInfosById();
        for(Case c : newCaseList){
            if(allTheRecordType.contains(rt_map.get(c.recordTypeID).getName())){
                List<Contact> conListOfCarrierAccount = new List<Contact>();
                List<String> carrierAddress = new  List<String>();
                
                /* Get contact for Carrier Account*/
                if(mapOfIdAndCarrierAccount != null && mapOfIdAndCarrierAccount.containsKey(c.Carrier_Name__c)){
                    conListOfCarrierAccount = mapOfIdAndCarrierAccount.get(c.Carrier_Name__c).contacts ;
                }
                System.debug('conListOfCarrierAccount '+conListOfCarrierAccount);
                for(contact con : conListOfCarrierAccount){
                    system.debug('con.Email '+con.Email);
                    if(con.Email != null)
                        carrierAddress.add(con.Email);
                }
                System.debug('ccAddress '+carrierAddress);
                mapOfCaseAndListOfCCAddress.put(c,carrierAddress);
            }
           }
        /*Send Email method*/
        sendEmailToCustomers(mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup);   
    }
    /*
    * Method Name   : sendEmailToCustomers
    * Description   : Method used to send Email to related contacts and its owner     
    * @param        : map<case,List<String>> mapOfCaseAndListOfCCAddress --> map of case and its related carrier contacts  
                      map<ID,User> mapOfIdAndUser  --> map of id and owner(user) 
                      map<ID,Group> mapOfIdAndGroup --> map of id and owner(Queue)
    * @returns      : None
    */
    public void sendEmailToCustomers(map<case,List<String>> mapOfCaseAndListOfCCAddress,map<ID,User> mapOfIdAndUser, map<ID,Group> mapOfIdAndGroup){
        final List<String> templateNames = new List<String>{Label.ISP_Order_Received,Label.ISP_Pending_Shipment_Carrier_Reminder,Label.ISP_Pending_Shipment_1st_Acknowledgement,Label.ISP_Pending_Shipment_2nd_Acknowledgement,Label.ISP_Pending_Shipment_3rd_Acknowledgement,Label.ISP_Pending_Shipment_3rd_Acknowledgement_Carrier,Label.ISP_Pending_Shipment_2nd_Acknowledgement_Carrier,Label.ISP_Pending_Shipment_1st_Acknowledgement_Carrier};
        
        List<EmailTemplate> templateIdList = new List<EmailTemplate>();
        Id carrierReminderTemplateID,orderReceivedTemplateID,firstAckTemplateID,secondAckTemplateID,thirdAckTemplateID,firstAckTemplateIDCarrier,secondAckTemplateIDCarrier,thirdAckTemplateIDCarrier;
        try {
            templateIdList = [select id,developerName from EmailTemplate where developerName IN :templateNames];
            if(templateIdList.size()>0){
                
                    for(EmailTemplate et : templateIdList){
                        if(et.developerName == Label.ISP_Order_Received)
                            orderReceivedTemplateID = et.id;
                        else if(et.developerName == Label.ISP_Pending_Shipment_Carrier_Reminder)
                            carrierReminderTemplateID = et.id;
                        else if(et.developerName == Label.ISP_Pending_Shipment_1st_Acknowledgement)
                            firstAckTemplateID = et.id;
                        else if(et.developerName == Label.ISP_Pending_Shipment_2nd_Acknowledgement)
                            secondAckTemplateID = et.id;
                        else if(et.developerName == Label.ISP_Pending_Shipment_3rd_Acknowledgement)
                            thirdAckTemplateID = et.id; 
                        else if(et.developerName == Label.ISP_Pending_Shipment_1st_Acknowledgement_Carrier)
                            firstAckTemplateIDCarrier = et.id;
                        else if(et.developerName == Label.ISP_Pending_Shipment_2nd_Acknowledgement_Carrier)
                            secondAckTemplateIDCarrier = et.id;
                        else if(et.developerName == Label.ISP_Pending_Shipment_3rd_Acknowledgement_Carrier)
                            thirdAckTemplateIDCarrier = et.id;                      
                    }
                    for(case c: newCaseList){
                        String allTheRecordType = Label.Record_Type_for_Order_Acknowledgement_Email;
                        Map<ID,Schema.RecordTypeInfo> rt_Map = case.sObjectType.getDescribe().getRecordTypeInfosById();
                        if(allTheRecordType.contains(rt_map.get(c.recordTypeID).getName())){
                            /*ISP order Received Email
                            if(Utility.ISP_order_Received_Email && orderReceivedTemplateID != null && c.Customer_P_O_accepted__c && oldCaseMap.get(c.id).Customer_P_O_accepted__c == false){
                                if(c.contactID != null){
                                    Messaging.SingleEmailMessage message =  new Messaging.SingleEmailMessage();
                                        message.setTargetObjectId(c.contactID);
                                        message.setTemplateId(orderReceivedTemplateID);
                                        message.setWhatId(c.id);
                                        System.debug('mapOfCaseAndListOfCCAddress.containsKey(c) '+mapOfCaseAndListOfCCAddress.containsKey(c));
                                        if(mapOfCaseAndListOfCCAddress.containsKey(c))
                                            if(mapOfCaseAndListOfCCAddress.get(c).size()>0)
                                                message.setCcAddresses(mapOfCaseAndListOfCCAddress.get(c));
                                        messageList.add(message);   
                                }
                                Utility.ISP_order_Received_Email = false;
                            }
                            */
                            /*ISP Pending Shipment Carrier Reminder*/
                            if(Utility.ISP_Pending_Shipment_Carrier_Reminder && carrierReminderTemplateID != null && c.Carrier_Notification_Date__c != null && Date.today().daysBetween(c.Carrier_Notification_Date__c) <= 3 && Date.today().daysBetween(c.Carrier_Notification_Date__c) >= 0 && c.Status == 'Closed - Final'){
                                
                                assignDetailsForEmail(c,carrierReminderTemplateID,mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup,true,false);   
                                Utility.ISP_Pending_Shipment_Carrier_Reminder = false;      
                            }
                            /*ISP Pending Shipment 1st Acknowledgement*/
                            if(Utility.ISP_Pending_Shipment_1st_Acknowledgement && (firstAckTemplateID != null || firstAckTemplateIDCarrier != null) && c.First_Acknowledged__c != null && c.Last_Acknowledged_Date_1__c == null && c.Status == 'Pending Shipment' && oldCaseMap.get(c.id).First_Acknowledged__c != c.First_Acknowledged__c ){
                                if(firstAckTemplateID != null)  
                                    assignDetailsForEmail(c,firstAckTemplateID,mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup,false,false);
                                if(firstAckTemplateIDCarrier != null)
                                    assignDetailsForEmail(c,firstAckTemplateIDCarrier,mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup,false,true);       
                                Utility.ISP_Pending_Shipment_1st_Acknowledgement = false;
                            }
                            /*ISP Pending Shipment 2nd Acknowledgement*/
                            if(Utility.ISP_Pending_Shipment_2nd_Acknowledgement && (secondAckTemplateID != null || secondAckTemplateIDCarrier != null) && c.First_Acknowledged__c != null && c.Last_Acknowledged_Date_1__c != null && c.Last_Acknowledged_Date_2__c == null && c.Status == 'Pending Shipment' && oldCaseMap.get(c.id).Last_Acknowledged_Date_1__c != c.Last_Acknowledged_Date_1__c){
                                if(secondAckTemplateID != null)
                                    assignDetailsForEmail(c,secondAckTemplateID,mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup,false,false);
                                if(secondAckTemplateIDCarrier != null)
                                    assignDetailsForEmail(c,secondAckTemplateIDCarrier,mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup,false,true);      
                                Utility.ISP_Pending_Shipment_2nd_Acknowledgement = false;
                            }
                            /*ISP Pending Shipment 3rd Acknowledgement*/
                            if(Utility.ISP_Pending_Shipment_3rd_Acknowledgement && (thirdAckTemplateID != null || thirdAckTemplateIDCarrier != null) && c.First_Acknowledged__c != null && c.Last_Acknowledged_Date_1__c != null && c.Last_Acknowledged_Date_2__c != null && c.Status == 'Pending Shipment' && oldCaseMap.get(c.id).Last_Acknowledged_Date_2__c != c.Last_Acknowledged_Date_2__c){
                                if(thirdAckTemplateID != null)
                                    assignDetailsForEmail(c,thirdAckTemplateID,mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup,false,false);
                                if(thirdAckTemplateIDCarrier != null)
                                    assignDetailsForEmail(c,thirdAckTemplateIDCarrier,mapOfCaseAndListOfCCAddress,mapOfIdAndUser,mapOfIdAndGroup,false,true);       
                                Utility.ISP_Pending_Shipment_3rd_Acknowledgement = false;
                            }
                        }
                    }
                    
                    system.debug('messageList '+messageList);
                    if(messageList.size()>0){
                        Messaging.SendEmailResult[] results = Messaging.sendEmail(messageList);
                        if(results[0].success) {
                            System.debug('The email was sent successfully.');
                        } 
                        else {
                            System.debug('The email failed to send: '+ results[0].errors[0].message);
                        }       
                    }
            }   
        }
        catch(Exception e){
            System.debug('Exception '+e);   
        }       
    }
    /*
    * Method Name   : assignDetailsForEmail
    * Description   : method is used to create Messaging.SingleEmailMessage instance with all the necessary Data.     
    * @param        : case caseOb --> case object
                      Id templateID --> tempate Id 
                      map<case,List<String>> mapOfCaseAndListOfCCAddress --> Map of the case and list of related carrier contacts as Cc'd members
                      map<ID,User> mapOfIdAndUser --> map of Id and related owner(User) details
                      map<ID,Group> mapOfIdAndGroup --> map of Id and related owner(Queue) details
                      boolean carrierReminder -- Boolean variable to identify the carrier reminder email.
                      boolean isEmailCarrierContact -- Boolean variable to differentiate the carrier contact email and customer contact email.
    * @returns      : None
    */
    public void assignDetailsForEmail(case caseObj,Id templateID,map<case,List<String>> mapOfCaseAndListOfCarrierAddress,map<ID,User> mapOfIdAndUser, map<ID,Group> mapOfIdAndGroup,boolean carrierReminder,boolean isEmailCarrierContact){
        Messaging.SingleEmailMessage message =  new Messaging.SingleEmailMessage();
            message.setTargetObjectId(caseObj.contactID);
            message.setTemplateId(templateID);
            message.setWhatId(caseObj.id);
            if(carrierReminder && mapOfCaseAndListOfCarrierAddress.containsKey(caseObj)){
                if(mapOfCaseAndListOfCarrierAddress.get(caseObj).size()>0){
                    List<String> toAddress = new List<String>();
                    toAddress = mapOfCaseAndListOfCarrierAddress.get(caseObj);
                    if(caseObj.CAM_Email__c != null)
                        toAddress.add(caseObj.CAM_Email__c);                                            
                    if(caseObj.Email_Address__c != null)
                        toAddress.add(caseObj.Email_Address__c);
                    if(String.valueOf(caseObj.ownerID).substring(0,3) == '00G' && mapOfIdAndGroup != null && mapOfIdAndGroup.size() > 0 && mapOfIdAndGroup.containsKey(caseObj.ownerID) && mapOfIdAndGroup.get(caseObj.ownerID).Email != null)
                        toAddress.add(mapOfIdAndGroup.get(caseObj.ownerId).Email);
                    else if(mapOfIdAndUser != null && mapOfIdAndUser.size() > 0 && mapOfIdAndUser.containsKey(caseObj.ownerID) && mapOfIdAndUser.get(caseObj.ownerID).Email != null)
                        toAddress.add(mapOfIdAndUser.get(caseObj.ownerId).Email);
                    message.setTreatTargetObjectAsRecipient(false);
                    message.setToAddresses(toAddress);
                }   
            messageList.add(message);
            }
            else{   
                 if(isEmailCarrierContact && mapOfCaseAndListOfCarrierAddress.containsKey(caseObj) && mapOfCaseAndListOfCarrierAddress.get(caseObj).size()>0){
                    List<String> toAddress = new List<String>();
                    toAddress = mapOfCaseAndListOfCarrierAddress.get(caseObj);
                    if(caseObj.CAM_Email__c != null)
                        toAddress.add(caseObj.CAM_Email__c);                                            
                    if(caseObj.Email_Address__c != null)
                        toAddress.add(caseObj.Email_Address__c);
                    if(String.valueOf(caseObj.ownerID).substring(0,3) == '00G' && mapOfIdAndGroup != null && mapOfIdAndGroup.size() > 0 && mapOfIdAndGroup.containsKey(caseObj.ownerID) && mapOfIdAndGroup.get(caseObj.ownerID).Email != null)
                        toAddress.add(mapOfIdAndGroup.get(caseObj.ownerId).Email);
                    else if(mapOfIdAndUser != null && mapOfIdAndUser.size() > 0 && mapOfIdAndUser.containsKey(caseObj.ownerID) && mapOfIdAndUser.get(caseObj.ownerID).Email != null)
                        toAddress.add(mapOfIdAndUser.get(caseObj.ownerId).Email);
                    message.setToAddresses(toAddress);
                    message.setTreatTargetObjectAsRecipient(false);
                 }
                 messageList.add(message);  
            }   
    }
}